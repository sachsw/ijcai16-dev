\section{Evaluation}

We focus on the impact on the performance of our approach of two
environmental factors: noise (i.e., extraneous actions) and partial
observability (i.e., missing trigger actions).  Each of these
complicates the task of mapping observations to expectations of
planned actions. Thus, we expect some degradation in performance as
the noise increases and observability decreases; the question is by
how much? We also consider the impact of parameters of our
approach. This leads to a factorial experiment design of 1,296 trials
as shown in Table~\ref{tab:exp} and described in the next two
subsections. In addition, we demonstrate the influence of the
algorithm parameters on observation traces taken from a human subject
study. 

\begin{table}
  \centering
  \begin{tabular}{{|l|p{1.6in}|}}
\hline
    Variable & Settings \\
\hline
Mosaic search (MS) & ITA*, MQA+ts \\
Objective weights (OW) & (0,1), (.5,.5), (1,0) \\
Extraneous actions (EA) & 0, 50, 100 \\
Partial observability (PO) & 25, 50, 100 \\
Planning domains (PD) & {\footnotesize {\tt blocks-words,  grid-navigation,
    ipc-grid+,  logistics, intrusion, PAG}} \\
Planning problems (PP) & 4 for each domain \\
\hline
  \end{tabular}
  \caption{Independent variables for evaluation experiment}
  \label{tab:exp}
\end{table}
\subsection{Independent Variables}

Our experiment includes two types of independent variables: algorithm
parameters and problem factors. 

The two algorithms in our approach are the alternate set generation
planner (Mosaic) and the trigger action ranker.  The certainty metric
is predicated on the sampling of alternative plans.  The sampling is
directed by the search strategy of the Mosaic planner. The two
possible strategies are ITA* and MQA+ts, as described in
section~\ref{sec:mosaic}. Mosiac will be configured to compute up to 100
possible plans with preferred operators disabled. 

The ranking is calculated from a weighting of certainty and
timeliness. We consider two extrema (only certainty  or only
timeliness) and equal weighting of $0.5$ each.

Our problem definition assumes observation traces are imperfect due to
including actions that may not be contributing to the undesirable
state (extraneous actions) and not including actions that do contribute (partial
observability). Each are measured as percentages of the observation
traces. Similar to \cite{ramirez2010probabilistic}, we generate the
noisy traces by starting with an optimal plan and modifying it, such
that for each domain/problem,
\begin{smallenum}
  \item Compute an optimal plan $OP$ using Metric-FF \cite{hoffmann2003metric}
  \item At each level in the plan graph for $OP$, randomly
    select an applicable action that was not in $OP$ and
    add it to the augmented trace $AT$.
\item For each value of EA, randomly remove
  actions from $AT$ until the \% is satisfied to produce an
  $AT_i$.
\item For each value of PO, randomly select an action from $OP$ until
  the \% is satisfied to produce $OP_i$. 
\item Construct traces ($|$PO$|$ x $|$EA$|$) from $OP_i$ and $AT_i$.
\end{smallenum}


% Problem factors: 1) number of undesirable states {1, 2 or domain
%   pre-set}, 2) percent of extraneous actions in observation traces (number
% extra/number in trace) as a proxy for signal-to-noise ratio {0, 25, 50,
%   100}, 3) percent of observable actions in observation traces (number from actual plan that are in the traces/number in actual plan) as a proxy for partial observability {25, 50, 100}

\subsubsection{Benchmark Domains}

The four domains used in \cite{ramirez2009plan} were made publicly
aavailable\footnote{See \smallurl{https://sites.google.com/site/prasplanning}}. {\tt Block-Words}
constructs one of 20 English words using 8 distinct letters. {\tt
  Grid-Navigation} forms paths through a connected graph to specific
locations (goals). {\tt IPC-grid+} is a grid navigation with keys
added at a restricted set of locations. {\tt Logistics} moves packages
between locations.

\input{security}

\subsection{Dependent Variables}

Performance is assessed on two measures: computation time and
accuracy. Computation time (CPU) is the average time required to process each
observation (one cycle of the process depicted in
Figure~\ref{fig:components}) within a trial; this includes both generating the
alternate plans and ranking the trigger actions. Computation time is
important in mixed initiative systems because the user may need to
wait while the proposed action is being vetted. 

Accuracy (ACC) is defined in terms of percentage of observations correctly
identified as either a critical trigger action or not (number correct
divided by total number in the observation trace). Each trial will
produce a sequence of results (either ``critical trigger action'' or nil)
corresponding to each observation. If the observed action corresponded
to the next step in the optimal plan, then the correct result is
``critical trigger action''; otherwise it is nil. 

% For each trial, results file should record: 
% a) independent variable values, b) amount of computation time for the
% trial, and c)  list of critical trigger actions (one for each observation step) or nil
% if no critical trigger action was found for that step.

% From the data, we can derive/compute the following measures:
% 1) computation time
% 2) accuracy: precision and recall for triggers
% 3) criticality? maybe construct lots of plans for domain and assess
% certainty and timeliness from those?

\subsection{Results}

