\section{Undesirable State Anticipation}

Computer users routinely download and start apps with only partial knowledge of what the apps do; for example, a recent study of 110 popular mobile apps showed that 73\% of Android and 47\% of iOS apps shared personal information with third parties \cite{Zang2015}.  Consider that some states that can result from user actions may not be what was intended because the person did not know about those consequences or was not actively aware of them. Such a situation can occur in mixed-initiative contexts where the person is focused on specific goals without regard to possible undesirable consequences. Simple common examples are when an autofill/autocorrect capability fills in the wrong word or address into electronic communication, the spam filter hides important correspondence, clicking on a link sends private information to a third party or a new app overwrites existing files.  Thus, our problem is that of recognizing that an action can directly or indirectly cause  a known undesirable state.

The focus of this paper is on defining the problem and constructing an approach to identify when a user's action(s) can be complicit in causing undesirable consequences.  We call these actions ``triggers'' because they can directly contribute to producing the undesirable state (e.g., as an effect or by satisfying a required precondition) or allow another actor (e.g., software or another person) to produce the undesirable state. 

We describe a general approach for ranking triggers and anticipating which are critical to producing the state. While similar to plan recognition in matching observation traces to possible plans, it differs significantly in that the ``goal(s)'' are known, but it is not known if a current action is critical to producing the goal state sometime in the future.  As in some plan recognition approaches \cite{hong2001goal,sun2007recognizing,ramirez2009plan,ramirez2010probabilistic}, our approach views undesirable state anticipation as a form of reverse planning; it leverages plan graphs from an existing planner to project possible undesirable outcomes that may result from what is known about the current state and then trace back to actions critical to their occurrence. We assume that state is partially observable; current state is estimated from the initial state and an observation trace of actions, which may be incomplete and may include actions that are extraneous (do not contribute to undesirable states). The approach requires as input an initial state, a PDDL model of actions and a set of undesirable states.

% \frommak{The notion of helpful is similar to the \emph{Value of Alert} proposed in Wilkins, Lee, \& Berry, Interactive Execution Monitoring of Agent Teams, JAIR 18 (2003).  Not sure it's worth citing, but it came to mind.}
% AEH: good call. I added it. I had looked at some of Myers papers and decided against them. This one is better.

Our focus is on identifying as early as is {\em helpful} when a user
may be triggering an undesirable state.  The concept of {\em helpful} is similar to the {\em value of alerts} described in \cite{Wilkins2003} which informs the decision of when an automated Execution Assistant should alert a human user to new information regarding the function of a distributed team of human and machine agents; they recognized that it is important to be judicious in interrupting a user: don't ignore but don't annoy. The key issue is when is it
helpful. Recognizing that an undesirable state has occurred is clearly
too late. Sometimes blocking the action with the undesirable effect is
too late because the final action might be done by another actor
(attacker or software without user intervention) or is not observed or
is the coup de grace in a series of actions where an earlier one
(e.g., downloading malware) was the critical trigger action.  As in
plan recognition, just because an action can lead to an undesirable
state does not mean that it is certain to occur. We define a
``critical trigger action'' to be a trigger action that maximizes a
domain/application appropriate combination of two competing
objectives: certainty and timeliness (i.e., early recognition of
potential danger). As actions are observed, their criticality is
ranked. For this paper, we examine the iterated decision form of this
problem: Given a sequence of observations $[ o_1, o_2, .., o_i ]$ is
the latest observed action $o_i$ the critical trigger action?  If
$o_i$ is top ranked, it is deemed a critical trigger action.

We evaluate the accuracy and performance of the approach on a set of
domains and observation traces. Five of the domains are taken from
prior work: \cite{ramirez2009plan,ramirez2010probabilistic}. The last
is a fine grained model of user/attacker actions and logs of actions
collected during a user study of how ``typical'' computer users
(university psychology students) respond to potentially unsafe
security situations. For each domain, the observation traces are
manipulated to vary noise and partial observability in the traces; we
assess their impact on accuracy and computation cost of the
approach. We find ...

