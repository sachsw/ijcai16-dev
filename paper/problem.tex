\section{Problem Statement}
\label{sec:defs}
%relates our problem to plan recognition

Plan recognition is the problem of inferring the goal(s) of an actor by constructing a plan from a sequence of actions or observations \cite{Schmidt1978plan,kautz1986generalized}.  The constructed plan, if followed to completion, is expected to result in states that correspond to goals of the actor, which in turn presupposes that the actor {\em intends} to achieve those goals. Plan recognition is the closest well known problem to our focus on identifying actions that contribute to causing known, undesirable goals. 
Thus, we adapt the plan recognition problem definition from \cite{ramirez2010probabilistic}.
\begin{mydef}
A {\em planning domain} is a tuple $D = \langle F, A \rangle$ where $F$ is a set of fluents and $A$ is a set of actions with preconditions and effects that are fluents.
\end{mydef}
\begin{mydef}
A {\em planning problem} is a tuple $P = \langle D, I, G \rangle$ where $D$ is a planning domain, $I \subseteq F$ and $G \subseteq F$. 
\end{mydef}
$F$ defines the states that may hold, and $A$ is the set of actions that can cause needed effects for the domain. $I$ is the initial state, $G$ is the goal state. So that we can use an existing planner, without loss of generality, we assume that the planning domain and problems are represented in PDDL. 

% \frommak{I changed $T= \langle D,O,C,U,\alpha,\beta \rangle$ to $T= \langle D,O,I,U,\alpha \rangle$ because I didn't find $C$ in the prose and because (at least for this paper) $\beta = 1 - \alpha$. I also slightly modified the prose below.}
\begin{mydef} 
An {\em undesirable state anticipation problem} is a tuple $T= \langle D,O,I,U,\alpha \rangle$ where
$D$ is a planning domain, $O \subseteq A$ is the sequence of observed actions, $I \subseteq F$ is the set of fluents comprising the initial state, $U \subseteq F$ is a set of undesirable states and $\alpha = [0,1]$ is a weight for the objective function. 
\end{mydef}
Each observed action $o_i$ in $O$ is given a subscript from 1 to $n$ to indicate its position in the sequence.  We assume that the set of undesirable states, $U$, is known in advance. The weight $\alpha$ designates the relative importance of the two factors that define criticality: certainty and timeliness.  

The solution to the undesirable state anticipation problem is a sequence of actions or nil that correspond to the sequence of observations (one for each observation) such that each is judged whether it is a critical trigger action given the observations up to that point in the observation sequence. Thus, the problem is an iterated decision problem where for each observation $o_i$. Given each observation, $o_i$, a new set of planning problems are constructed from a new initial state computed from the initial state and the observations so far and the set of undesirable states $U$. An {\em undesirable plan} $\pi^u \in \Pi^u$ is a sequence of actions that maps the estimated current state into an undesirable state $u$, $u \in U$ such that each action in $\pi^u$ is applicable (its preconditions are satisfied). Given $\pi^u$ we distinguish two types of actions leading one or more undesirable states.

\begin{mydef}
A {\em trigger action} is an action $t_i$, $(t \in A) \wedge (t \in {o_1,...,o_i})$, that is found in at least one undesirable plan.  
\end{mydef}
\begin{mydef}
A {\em critical trigger action} $c_i$ is the trigger action at observation $o_i$ that maximizes an objective function, $V(c_i)= \alpha v_1(c_i) + (1-\alpha) v_2(c_i)$, across all possible trigger actions found given observations ${o_1,...o_i}$. 
\end{mydef}
$v_1$ corresponds to the certainty objective, and $v_2$ corresponds to the timeliness objective. 

Given a closed, deterministic, and finite domain, certainty that an action is necessary to causing the undesirable state could be calculated precisely. We assume that we must estimate certainty through sampling. Thus, for  our purposes, certainty is measured by the percent of sampled undesirable plans in which the trigger action occurs: 
\[ \textrm{Certainty}(a) = \frac{| \textrm{undesirable plans in which } a \textrm{ occurs}|}{| \textrm{undesirable plans found}|}\]

Similarly, precise computation of timeliness requires knowing what is yet to be observed and knowing what could be done with the information (e.g., how the action or its effects could be prevented or circumvented). For our purposes, timeliness is measured by the maximum normalized steps remaining in sampled undesirable plans in which the action occurs: 
\[ \textrm{Timeliness}(a) = \max_{\pi \in \Pi} \left(\frac{\max\left(\# \textrm{ of steps in } \pi \textrm{ starting from } a\right)}{\# \textrm{of steps in }\pi} \right) \]
The normalization produces the same range ($0\rightarrow1$) for Certainty and Timeliness. 

To illustrate, we present a small example. Assume at a certain point in the observations where $U={u_1,u_2,u_3}$, we have generated a set of undesirable plans $\Pi=\{(bgn[u_1]), (bgmq[u_1]), (ch[u_2]), (cjp[u_1]), (cjdfl[u_3]),$ $(cjdfkp[u_1]), (fl[u_3]), (fkp[u_1]), (fkdfl[u_3]), (fkdfkp[u_1])\}$ where a plan is a sequence of actions (letters) followed in brackets by the undesirable state that results and each letter $(b,c,d,f,g,h,j,k,l,m,n,p,q)$ denotes a trigger action (an action that appears in at least one undesirable plan). From this we can compute the Certainty and Timeliness of each trigger action as shown in Table~\ref{tab:example}.
\begin{table}
  \centering
  \begin{tabular}{|l|l|l|l|}
\hline
 & Certainty & Timeliness & Obj \\
\hline
b & 2/10=0.2 & max(3/3,4/4)=1.0 & 0.6 \\
c & 4/10=0.4 & max(2/2,3/3,5/5,6/6)=1.0 & 0.7 \\
d & 4/10=0.4 & max(3/5,4/6,3/5,4/6)=0.6 & 0.5 \\
f & 6/10=0.6 & max(2/5,3/6,2/2,2/3,5/5,6/6)=1.0 & 0.8 \\
g & 2/10=0.2 & max(2/3,3/4)=0.75 & 0.475 \\
h & 1/10=0.1 & max(1/2)=0.5 & 0.35 \\
j & 3/10=0.3 & max(2/3,4/5,5/6)=0.83 & 0.565 \\
k & 4/10=0.4 & max(2/6,2/3,4/5,5/6)=0.83 & 0.615 \\
l & 3/10=0.3 & max(1/5,1/2,1/5)=0.5 & 0.4 \\
m & 1/10=0.1 & max(2/4)=0.5 & 0.35 \\
n & 1/10=0.1 & max(1/3)=0.33 & 0.215 \\
p & 4/10=0.4 & max(1/3,1/6,1/3,1/6)=0.33 & 0.53 \\
q & 1/10=0.1 & max(1/4)=0.25 & 0.175 \\
\hline    
  \end{tabular}
  \caption{Certainty and Timeliness computations for each trigger action in a synthetic example. Obj is the objective function assuming $\alpha=0.5$.}
\label{tab:example}
\end{table}
If the two objectives are equally weighted ($alpha=0.5$) or if certainty is the only objective ($alpha=1$), then the highest ranked action is $f$; so if $f$ is the observed action, it will be flagged as a critical action. If timeliness is the objective ($alpha=0$), then we have a tie between $b$, $c$ and $f$; thus, if one of them is the observed action, it will be flagged as critical.  
