(define (domain synu)
  (:requirements :strips :typing :equality)
  (:types value)
  (:predicates (enabled ?val - value))

  (:action b
     :parameters ( )
     :precondition (and (enabled s1))
     :effect (and (enabled sb)) )

  (:action g
     :parameters ( )
     :precondition (and (enabled sb))
     :effect (and (enabled sg)) )

  (:action m
     :parameters ( )
     :precondition (and (enabled sg))
     :effect (and (enabled sm)) )

  (:action q
    :parameters ( )
    :precondition (and (enabled sm))
    :effect (and (enabled u1)) )

  (:action n
     :parameters ( )
     :precondition (and (enabled sg))
     :effect (and (enabled u1)) )


  
  (:action c
     :parameters ( )
     :precondition (and (enabled s2))
     :effect (and (enabled sc)) )

  (:action h
     :parameters ( )
     :precondition (and (enabled sc))
     :effect (and (enabled u2)) )



  (:action f
     :parameters ( )
     :precondition (and (enabled s3))
     :effect (and (enabled sf)) )

  (:action k
     :parameters ( )
     :precondition (and (enabled sf))
     :effect (and (enabled sk)) )

  (:action p
     :parameters ( )
     :precondition (and (enabled sk))
     :effect (and (enabled u1)) )

  (:action d
     :parameters ( )
     :precondition (and (enabled sk))
     :effect (and (enabled s3)) )

  (:action l
     :parameters ( )
     :precondition (and (enabled sf))
     :effect (and (enabled u3)) )

)

