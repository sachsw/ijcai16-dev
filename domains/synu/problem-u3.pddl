
(define (problem synu-u3)
	(:domain synu)
(:objects 
  s1 s2 s3 - value
  sb sc sf sg sh sk sl sm sn sp - value
  u1 u2 u3 - value
)
(:init
  (enabled s1)
  (enabled s2)
  (enabled s3)
)
(:goal (and (enabled u3)))
)
 