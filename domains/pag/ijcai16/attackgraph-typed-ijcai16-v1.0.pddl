(define (domain attackgraph-typed)
  (:requirements :strips :adl :equality :disjunctive-preconditions :typing)
  (:types unknown software misc - object  
          account certificate email-ID file key site user attacker vulnerability-type password direct-message device database feature firmware module parameter permission process username response request function-call - misc
          browser editor key-logger mailer webmailer plug-in site-creating-software server-connecting-software server anti-virus OS desktop-app server-app malicious-software - software
          phishing-site server-site normal-site - site
		  phishing-site-email phishing-site-twitter - phishing-site
		  attacker-remote attacker-local - attacker
          )

(:constants
	;;ACCOUNTS
		account-bank - account
		user1-email-account - account ;;IJCAI16
		user1-twitter-account - account ;;IJCAI16
	;;ANTI-VIRUSES
		pc-doctor - anti-virus
		norton - anti-virus ;IJCAI16
	;;BROWSWERS
		browser-firefox - browser 	
		browser-IE - browser 	
		browser-IE-consumer-preview - browser 	
		browser-mozilla - browser 	
		browser-seaMonkey - browser 	
		browser-thunderbird - browser
	;;CERTIFICATES
        certificate-good - certificate
       	malicious-certificate - certificate
	;;DEVICES
		user-computer - device
	;;DIRECT-MESSAGES
		phishing-direct-message - direct-message  
	;;EDITORS
		word - editor
	;;EMAILS
		bad-email  - email-ID
	    crafted-email - email-ID
		email-with-malicious-attachment - email-ID
		phishing-email - email-ID
	;;FILES
		file-with-trojan - file
		file-without-trojan - file
		zip-file-with-malicious-script - file
		pdf-file-with-malicious-script - file ;;IJCAI16
	;;KEYS
		F1 - key
	;;KEY LOGGERS
		key-logger1 - key-logger
	;;MAILERS
		thunderbird - mailer
	;;WEBMAILER
		squirrelmail - webmailer
		gmail - webmailer ;IJCAI16
	;;Operating Systems
		os-windows - OS
		os-mac-os-x - OS
		os-linux - OS
		os-solaris - OS
	;;PASSWORDS
		user1-twitter-password - password
		bank-account-password - password
		user1-email-password - password
	;;SITES
		bank-site - normal-site
	   	compromised-site - normal-site
		crafted-site - normal-site
		crafted-telnet-site - normal-site
		site-with-e4x-document - normal-site
		twitter-site - normal-site
		vbscript-link - normal-site
		twitter-phishing-site1 - phishing-site-twitter
		twitter-phishing-site2 - phishing-site-twitter ;;IJCAI16
		email-phishing-site1 - phishing-site-email
		email-phishing-site2 - phishing-site-email ;;IJCAI16
		webmail-site - normal-site
		antivirus-software-download-site - normal-site ;IJCAI16
	;;Software
		malicious-script - malicious-software
	;;USERS
		user1 - user
	;;VULNERABILITIES
		vulnerability-denial-of-service - vulnerability-type
		vulnerability-key-logger - vulnerability-type
		vulnerability-phishing - vulnerability-type
		SSL - vulnerability-type  
		vulnerability-ssl - vulnerability-type
		vulnerability-telnet-handler-remote-code-execution - vulnerability-type
		vulnerability-unauthorized-access - vulnerability-type
		vulnerability-malicious-software-installed - vulnerability-type

	;;NON-TYPED OBJECTS
		CVE-2008-3111 - unknown
		CVE-2009-2408 - unknown		
		VB-5-1 - unknown
		VB-5-6 - unknown
		VB-5-7 - unknown
		VB-5-8 - unknown
		VB-6-0 - unknown
		any-version - unknown
		SP4-2000 - unknown
		windows7 - unknown
	

)

(:predicates 
; user information
   (information-available ?aUser - User ?anAccount - account) 
   (user-knows-URL ?aUser - user ?aSite - site)
   (threat-information-available ?aComputer - device)
   (user-has-email-account ?anAccount - account)
   
; general type information
   (CVE ?num - object) ;; don't make this a type
   (email-msg ?aUser - user ?Msg - email-ID) 
   (keyboard-key ?aKey - key)

; properties
   (site-is-malicious ?aSite - site)					
   (file-is-malicious ?afile - file)					
   (has-crafted-dialog-box ?aSite - site)
   (phishing-msg ?Msg - email-ID ?aSite - site)
   (vb-script-version ?aScript - object) 


; system state information
   (current-OS ?OS-type - OS ?v - object)							
   (site-published ?asite - site)									
   (has-trojan ?afile - File)
   (installed ?S - software)
   (key-logger-trojan ?aFile - file ?aKey-logger - key-logger) 
   (logged-in ?aUser - user ?anAccount  - account)
   (msg-opened ?Msg - email-ID) 
   (opened ?aFile - file)
   (receive-email ?aUser - user ?anEmail - email-ID)
   (software-visible ?S - software) 
   (direct-message-received ?dmsg - direct-message) 
   (unread-direct-message-in-inbox ?dmsg - direct-message) 
   (insecure-http-connection-opened ?aSite - site) 
   (logged-onto-system ?U - user) 
   (anti-virus-software-selected ?av - anti-virus) ;;IJCAI16
   (anti-virus-software-downloaded ?av - anti-virus ?aComputer - device) ;;IJCAI16

;; system activity information
   (F1-dialog-box-opens ?aSite - site)
   (records ?S - software ?information - object)
   (running ?S - software)
   (virus-scan-started ?aSoftware - software) 
   (virus-infection-detected ?aComputer - device) 
   (prompt-displayed ?aSite - site)
  
; other information
   (certificate-accepted ?aSite - site ?aCertificate - certificate)
   (certificate-authorized ?aCertificate - certificate)
   (email-has-link ?anEmail - email-ID ?aSite - site) 
   (mail-attachment ?anEmai - email-ID ?aFile - file)
   (direct-message-has-link ?dmsg - direct-message ?aSite - site)

   
; vulnerability types
   (browser-compromised ?aBrowser - browser ?aVulnerability - vulnerability-type) 
   (disruption-of-service ?aUser - user)
   (information-leakage ?anAccount - account)
   (ssl-server-spoofed ?d - object) 
   (device-compromised ?D - device ?Vulnerability - vulnerability-type)

; user activity state
   (clicked-link ?anEmail - email-ID ?aSite - site) 
   (read-email ?anEmail - email-ID)
   (trigger-key-logger ?aKey - key ?aKey-logger - key-logger) 
   (use-software ?S - software)
   (user-types ?aKey - key)
   (user-visits-site ?aUser - user ?aSite - site)
   (user-submits ?Twitteracc - account ?Twitterpass - password) 
   (direct-message-opened ?dmsg - direct-message)
   (clicked-link-on-direct-message ?dmsg - direct-message ?aSite - site)

; attacker state
   (exploit-vulnerability ?aVulnerability - vulnerability-type) 
   (attacker-recieves-twitter-information ?Twitteracc - account ?Twitterpass - password)
   

; intervention state
;   (make-safe-vulnerability ?vulerability - vulnerability-type) 


)


(:action attacker-sends-email-with-malicious-link
 :parameters (?aMsg - email-ID ?auser - user ?asite - site)
 :precondition (and (= ?aMsg bad-email) (site-is-malicious ?asite) (logged-onto-system ?auser) (site-published ?asite))
 :effect (and (email-msg ?auser ?aMsg) (email-has-link ?aMsg ?asite))
)


(:action user-starts-software  
 :parameters (?U - user ?S - software)
 :precondition (and (installed ?S) (software-visible ?S) (logged-onto-system ?U))
 :effect (and (use-software ?S) (running ?S))
) 

(:action user-reads-email-through-mailer
 :parameters (?U - user ?M - mailer ?Msg - email-ID ?anAccount - account)
 :precondition (and (use-software ?M) (running ?M) (user-has-email-account ?anAccount) (= ?anAccount user1-email-account)
                    (email-msg ?U ?Msg) (logged-onto-system ?U) )
 :effect (and (msg-opened ?Msg))
) 

(:action user-reads-email-through-webmail
 :parameters (?U - user ?aBrowser - browser ?WM - webmailer ?aSite - site ?Msg - email-ID ?anAccount - account)
 :precondition (and (use-software ?aBrowser) (running ?aBrowser) (user-visits-site ?U ?aSite) (= ?aSite webmail-site)
					(user-has-email-account ?anAccount) (= ?anAccount user1-email-account)
                    (email-msg ?U ?Msg) (logged-onto-system ?U) )
 :effect (and (msg-opened ?Msg))
) 

;;IJCAI16
(:action user-visit-antivirus-download-site
 :parameters (?U - user ?aBrowser - browser ?aSite - site ?av - anti-virus)
 :precondition ( and (logged-onto-system ?U) (use-software ?aBrowser) (running ?aBrowser) (= ?aSite antivirus-software-download-site) (not (installed ?av))  (not (running ?av)) )
 :effect (and (user-visits-site ?U ?aSite))
)

;;IJCAI16
(:action user-select-antivirus-from-download-site
 :parameters (?U - user  ?aSite - site ?av - anti-virus)
 :precondition ( and (user-visits-site ?U ?aSite) (= ?aSite antivirus-software-download-site)   )
 :effect (and (anti-virus-software-selected ?av))
)

;;IJCAI16
(:action user-download-antivirus-from-download-site
 :parameters (?U - user  ?aSite - site ?av - anti-virus ?aComputer - device)
 :precondition ( and (anti-virus-software-selected ?av) (= ?aSite antivirus-software-download-site)   )
 :effect (and (anti-virus-software-downloaded ?av ?aComputer) (software-visible ?av))
)

;;IJCAI16
(:action user-install-antivirus-software
 :parameters (?av - anti-virus ?aComputer - device)
 :precondition (and  (anti-virus-software-downloaded ?av ?aComputer) (software-visible ?av)  )
 :effect (and (installed ?av))
)




(:action user-opens-attachment-through-mailer 
 :parameters (?U - user ?Msg - email-ID ?M - mailer ?F - file)
 :precondition (and   (use-software ?M) 
 	       	      (msg-opened ?Msg)
                      (mail-attachment ?Msg ?F)
		      (logged-onto-system ?U)) 
 :effect (opened ?F)) 

(:action user-opens-attachment-through-webmail 
 :parameters (?U - user ?aBrowser - browser ?WM - webmailer ?aSite - site  ?anAccount - account ?Msg - email-ID  ?F - file )
 :precondition (and  (use-software ?aBrowser) (running ?aBrowser) (user-visits-site ?U ?aSite) (= ?aSite webmail-site)(user-has-email-account ?anAccount) (= ?anAccount user1-email-account) (msg-opened ?Msg) (mail-attachment ?Msg ?F) (logged-onto-system ?U)) 
 :effect (opened ?F)) 
 
 
(:action user-visits-site 
 :parameters (?aUser - user ?aBrowser - browser ?aSite - site)
 :precondition (and (user-knows-URL ?aUser ?aSite) (running ?aBrowser) (logged-onto-system ?aUser))
 :effect (and (use-software ?aBrowser) (user-visits-site ?aUser ?aSite)))

 
 (:action user-logs-into-webmail
	:parameters (?aUser - user ?aWebMail - webmailer ?aBrowser - browser ?aSite - site ?anAccount - account ?aPassword - password)
	:precondition (and (user-visits-site ?aUser ?aSite) (= ?aSite webmail-site) (user-has-email-account ?anAccount) (= ?anAccount user1-email-account))
    :effect(and (user-submits ?anAccount ?aPassword) (logged-in ?aUser ?anAccount) (information-available ?aUser ?anAccount) (running ?aWebMail))
 )


;;IJCAI16
(:action user-clicks-link-in-email
 :parameters (?aUser - user ?aBrowser - browser ?anEmail - email-ID ?aSite - site) 
 :precondition (and (msg-opened ?anEmail) (email-has-link ?anEmail ?aSite) (and (not (= ?aSite twitter-phishing-site1)) (not (= ?aSite twitter-phishing-site2)) ) (logged-onto-system ?aUser)(installed ?aBrowser)) 
 :effect (and (clicked-link ?anEmail ?aSite) (user-knows-URL ?aUser ?aSite))
 )
 


(:action site-displays-prompt-to-press-key
 :parameters (?aSite - site ?aUser - user ?aKeylogger - key-logger)
 :precondition (and (installed ?aKeylogger) (user-visits-site ?aUser ?aSite))
 :effect (prompt-displayed ?aSite)
)


(:action user-presses-F1-at-vbscript-site 
 :parameters (?aUser - user ?aBrowser - browser ?aSite - site ?aKeyLogger - key-logger)
 :precondition (and (use-software ?aBrowser) 
                    (= ?aBrowser browser-IE) (= ?aSite vbscript-link) (keyboard-key F1)
                    (user-visits-site ?aUser vbscript-link) (installed ?aKeyLogger) (prompt-displayed ?aSite))
 :effect    (trigger-key-logger F1 ?aKeyLogger))
	

(:action browser-accepts-certificate   
 :parameters (?aUser - user ?aBrowser - browser  ?aCertificate - certificate ?aSite - site)
 :precondition (and (user-visits-site ?aUser ?aSite)
                    (certificate-authorized ?aCertificate))
 :effect (certificate-accepted ?aSite ?aCertificate))


;;IJCAI16
(:action user-login-to-twitter
    :parameters(?aUser - user ?aSite - site ?anAccount - account ?aPassword - password)
    :precondition (and (logged-onto-system ?aUser) (user-visits-site ?aUser ?aSite) (= ?aSite twitter-site) (= ?anAccount user1-twitter-account) (= ?aPassword user1-twitter-password))
    :effect(and (user-submits ?anAccount ?aPassword) (logged-in ?aUser ?anAccount) (information-available ?aUser ?anAccount))
)

 
;;IJCAI16
(:action user-opens-twitter-inbox
    :parameters(?aUser - user ?Twitteracc - account ?dmsg - direct-message) 
    :precondition(and (logged-in ?aUser ?Twitteracc) (direct-message-received ?dmsg) (= ?Twitteracc user1-twitter-account) )
    :effect (and (unread-direct-message-in-inbox ?dmsg) )
)


;;IJCAI16
(:action user-reads-direct-message
    :parameters (?aUser - user ?Twitteracc - account ?dmsg - direct-message)
    :precondition (and (unread-direct-message-in-inbox ?dmsg) (logged-in ?aUser ?Twitteracc) (= ?Twitteracc user1-twitter-account))
    :effect (and (direct-message-opened ?dmsg))

)

(:action user-starts-virus-scan
 :parameters (?av - anti-virus ?aComputer - device)
 :precondition(and (running ?av) (= ?aComputer user-computer)) ;;IJCAI16: removed (= ?av pc-doctor) 
 :effect (and (virus-scan-started ?av) )
)

(:action antivirus-detects-malicious-program
 :parameters (?av - anti-virus ?aComputer - device ?aKeylogger - key-logger)
 :precondition(and (virus-scan-started ?av) (installed ?aKeylogger)) 
 :effect (and (virus-infection-detected ?aComputer))
)


(:action antivirus-displays-threats-on-device
 :parameters (?aComputer - device )
 :precondition(and (virus-infection-detected ?aComputer)) 
 :effect (and (threat-information-available ?aComputer))
)


;;IJCAI16
(:action user-enters-information-in-phishing-site-email
 :parameters (?aUser - user  ?aSite - phishing-site ?anAccount - account)
 :precondition (and (user-visits-site ?aUser ?aSite) 
                    (exploit-vulnerability vulnerability-phishing)
                     (= ?aUser user1)
                     (or (= ?aSite email-phishing-site1)
                         (= ?aSite email-phishing-site2)
                     )
                     (= ?anAccount user1-email-account))
 :effect (information-leakage ?anAccount))
 

;;IJCAI16
(:action user-enters-information-in-phishing-site-twitter
 :parameters (?aUser - user  ?aSite - phishing-site ?anAccount - account)
 :precondition (and (user-visits-site ?aUser ?aSite) 
                    (exploit-vulnerability vulnerability-phishing)
                     (= ?aUser user1)
                     (or (= ?aSite twitter-phishing-site1)
                         (= ?aSite twitter-phishing-site2)
                     )
                     (= ?anAccount user1-twitter-account)) 
 :effect (information-leakage ?anAccount))


(:action phishing-site-opens 
 :parameters (?aUser - user  ?aSite - phishing-site ?anEmail  - email-ID) 
 :precondition (and (user-knows-URL ?aUser ?aSite)
                    (clicked-link ?anEmail ?aSite) (phishing-msg ?anEmail ?aSite)  
				) 
 :effect (user-visits-site ?aUser ?aSite)) 


(:action user-visits-crafted-site  
  :parameters (?aUser - user ?aBrowser - browser ?aSite - site ?anEmail  - email-ID)
  :precondition (and (logged-onto-system ?aUser)
                     (use-software ?aBrowser)
                     (= ?aBrowser browser-IE) 
                     (has-crafted-dialog-box ?aSite)
                     (clicked-link ?anEmail ?aSite)
                     (or (vb-script-version VB-5-1) (vb-script-version VB-5-6)
                         (vb-script-version VB-5-7) (vb-script-version VB-5-8)))
  :effect (and  (user-visits-site ?aUser ?aSite) (F1-dialog-box-opens ?aSite)
                (CVE CVE-2009-2408)))


(:action user-login-with-keylogger-activated
 :parameters (?aUser - user ?aKeyLogger - key-logger ?anAccount - account)
 :precondition (and (running ?aKeyLogger))
 :effect (and (logged-in ?aUser ?anAccount) (information-available ?aUser ?anAccount)
              (records ?aKeyLogger ?anAccount)))


(:action user-login-under-spoof 
 :parameters (?aUser - user ?aSite - site ?anAccount - account)
 :precondition (and (user-visits-site ?aUser ?aSite) (information-available ?aUser ?anAccount)
                    (ssl-server-spoofed ?aSite)
                    (exploit-vulnerability vulnerability-ssl))
 :effect (information-leakage ?anAccount))



(:action user-clicks-link-in-direct-message
    :parameters (?aUser - user ?dmsg - direct-message ?aSite - site) 
    :precondition (and (direct-message-opened ?dmsg) (direct-message-has-link ?dmsg ?aSite) (= ?aUser user1)) 
    :effect (and (clicked-link-on-direct-message ?dmsg ?aSite))
)


(:action user-visits-phishing-site-via-direct-message
    :parameters (?aUser - user ?dmsg - direct-message ?aSite - phishing-site-twitter) 
    :precondition (and (clicked-link-on-direct-message ?dmsg ?aSite) (or (= ?aSite twitter-phishing-site1) (= ?aSite twitter-phishing-site2)) )  
    :effect (and  (insecure-http-connection-opened ?aSite) (user-visits-site ?aUser ?aSite))
)

;;IJCAI16
(:action user-clicks-on-malicious-script
    :parameters (?aFile - file ?aSoftware - software)
    :precondition (and (opened ?aFile) (or (= ?aFile zip-file-with-malicious-script) (= ?aFile pdf-file-with-malicious-script)))
    :effect (and (installed malicious-script))
)

(:action user-clicks-ignore-threat-option
 :parameters (?aComputer - device)
 :precondition (and (virus-infection-detected ?aComputer) (threat-information-available ?aComputer))
 :effect (and (device-compromised ?aComputer vulnerability-malicious-software-installed))
)



(:action attacker-sends-email-with-keylogger
  :parameters (?aUser - user ?ffile - file ?aKeylogger - key-logger)
  :precondition (and (has-trojan ?ffile) (key-logger-trojan ?ffile ?aKeylogger) (= ?auser user1))
  :effect (and (email-msg ?aUser bad-email) (mail-attachment bad-email ?ffile))
)



(:action attacker-sends-email-with-crafted-link
  :parameters (?aUser - user ?aSite - site ?aKeylogger - key-logger)
  :precondition (and (has-crafted-dialog-box ?aSite) (installed ?aKeyLogger))
  :effect (and (email-msg ?aUser crafted-email) (email-has-link crafted-email ?aSite))  
)

(:action key-logger-installed 
 :parameters (?aUser - user ?ffile - file ?aKeyLogger - key-logger)
 :precondition (and (opened ?ffile) (has-trojan ?ffile) (key-logger-trojan ?ffile ?aKeyLogger))
 :effect (and (installed ?aKeyLogger)))

(:action key-logger-activated 
 :parameters (?aKeyLogger - key-logger ?Program - software)
 :precondition (and (installed ?aKeyLogger) 
      	            (trigger-key-logger F1 ?aKeyLogger) (use-software ?Program) 
                    (= ?Program browser-IE))
 :effect (running ?aKeyLogger))
	
(:action attacker-intercepts 
 :parameters (?aKeyLogger - key-logger ?anAccount - account)
 :precondition (and (records ?aKeyLogger ?anAccount)
	                   (exploit-vulnerability vulnerability-key-logger))
 :effect (information-leakage ?anAccount))


;;IJCAI16
(:action attacker-sends-phishing-email
 :parameters (?aUser - user ?aMsg - email-ID ?anAccount - account ?aSite - phishing-site)
 :precondition (and (logged-onto-system ?aUser) (user-has-email-account ?anAccount) (= ?anAccount user1-email-account) (exploit-vulnerability vulnerability-phishing) )
 :effect (and (email-msg ?aUser bad-email) (phishing-msg bad-email ?aSite)
              (email-has-link bad-email ?aSite))
)
   

(:action attacker-obtains-certificate 
  :parameters (?aBrowser - browser ?aCertificate - certificate ?aVulnerability - vulnerability-type)
  :precondition (and 
                     (browser-compromised ?aBrowser ?aVulnerability) (= ?aVulnerability SSL)
                     (= ?aCertificate malicious-certificate))
  :effect (and (certificate-authorized ?aCertificate)))


(:action attacker-spoofs-ssl-server
 :parameters (?aUser - user ?aCertificate - certificate ?aBrowser - browser ?aSite - site 
              ?aVulnerability - vulnerability-type)
 :precondition (and (user-visits-site ?aUser ?aSite) 
                    (certificate-accepted ?aSite ?aCertificate)
                    (browser-compromised ?aBrowser ?aVulnerability) (= ?aVulnerability SSL)
                    (= ?aCertificate malicious-certificate))
 :effect (ssl-server-spoofed ?aSite))


(:action attacker-sends-direct-message-with-phishing-link 
    :parameters (?aUser - user ?dmsg - direct-message ?aSite - phishing-site)
    :precondition (and  (= ?aUser user1) (= ?dmsg phishing-direct-message))
    :effect(and (direct-message-received ?dmsg) (direct-message-has-link ?dmsg ?aSite))
)


;;IJCAI16
  (:action attacker-intercepts-twitter-information
    :parameters (?Twitteracc - account ?Twitterpass - password ?aSite - phishing-site)
    :precondition (and (information-leakage ?Twitteracc) (= ?Twitteracc user1-twitter-account) (insecure-http-connection-opened ?aSite) )
    :effect(and (attacker-recieves-twitter-information ?Twitteracc ?Twitterpass))
  )


(:action attacker-sends-email-with-malicious-attachment
    :parameters (?aUser - user ?aFile - file)
    :precondition (and (file-is-malicious ?aFile))
    :effect (and (email-msg ?aUser bad-email) (mail-attachment bad-email ?aFile))
)


(:action attacker-obtains-unauthorized-access
    :parameters (?computer - device ?aSoftware - software)
    :precondition (and (running ?aSoftware) (= ?aSoftware malicious-script) (= ?computer user-computer))
    :effect (device-compromised ?computer vulnerability-unauthorized-access)
)

(:action malicious-script-activated
   :parameters (?computer - device ?aSoftware - software ?file-with-malicious-script - file)
   :precondition (and (opened ?file-with-malicious-script) (installed ?aSoftware)  (= ?aSoftware malicious-script) (= ?computer user-computer))
   :effect(and (running ?aSoftware))
)


)
