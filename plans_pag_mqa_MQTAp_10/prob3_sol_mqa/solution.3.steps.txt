(user-starts-software user1 browser-firefox)
(attacker-sends-email-with-malicious-attachment user1 zip-file-with-malicious-script)
(user-visits-site user1 browser-firefox webmail-site)
(user-reads-email-through-webmail user1 browser-firefox squirrelmail webmail-site bad-email email-account)
(user-opens-attachment-through-webmail user1 browser-firefox squirrelmail webmail-site email-account bad-email zip-file-with-malicious-script)
(user-clicks-on-malicious-script zip-file-with-malicious-script pc-doctor)
(malicious-script-activated user-computer malicious-script zip-file-with-malicious-script)
(attacker-obtains-unauthorized-access user-computer malicious-script)
; ThisSolutionCPU:0
; RunCPU:0
; Diversity:2
