
#this script assumes fast downward is a sibling directory to this repo checkout:
#  workspace
#  |-- cybersec-ijcai16-dev.git
#  |-- fast-downward.hg

fddir="../../fast-downward.hg"

domainFile="../domains/pag/ijcai16/attackgraph-typed-ijcai16-v1.0.pddl"
problemFile="../domains/pag/ijcai16/problem_1.pddl"

cp $domainFile $fddir/domain.pddl
cp $problemFile $fddir/problem.pddl
