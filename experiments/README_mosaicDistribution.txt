
Thank you for your interest in Mosaic, a planner built on the Fast Downward 2008 codebase.

For questions, please contact Mak Roberts at mark.roberts.ctr@nrl.navy.mil

To run the executables on a 64 bit linux system you need to install:
   sudo apt-get install lib32stdc++6

   #on systems older than Ubuntu 13.10, run
   sudo apt-get install ia32-libs 

   #on systems including and newer than Ubuntu 13.1, run
   sudo apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0

To run the executables, run

   mosaicPlan.sh domain.pddl problem.pddl -cconfig.xml [-ssupplemental.xml]
   mosaicPlanDebug.sh domain.pddl problem.pddl -cconfig.xml [-ssupplemental.xml]

where config.xml and supplemental.xml matches to the published results for a particular variant (or its fullname in my scripts) described in the ICAPS 2014 paper "Evaulating Diversity in Classical Planning" by Roberts and Howe.  

   Div    (div)    : -cDivAStar_w100f080_1000.xml
   RWS    (rws)    : -cRWS_1000.xml
   ITA*   (ita)    : -cItAStar_w100f080_1000.xml
   MQA_D  (mqapd)  : -cMQAp_1000.xml -sQueue_AStarFF.xml -sQueue_diversity.xml
   MQA_S  (mqaps)  : -cMQAp_1000.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml
   MQA_TD (mqtapd) : -cMQTAp_1000.xml -sQueue_AStarFF.xml -sQueue_diversity.xml
   MQA_TS (mqtaps) : -cMQTAp_1000.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml

I reported on additional variant in my dissertation but excluded it from the ICAPS paper due to its similarity with those listed above:
   Div+ITA (divita) : -c DivItAStar_w100f080_1000.xml

An example of how to run the planner is:

mosaicPlan.sh transport-ipc2011-domain.pddl transport-ipc2011-problem-p01.pddl -cMQAp_1000.xml -sQueue_AStarFF.xml -sQueue_diversity.xml
mosaicPlan.sh rover-ipc02-domain.pddl rover-ipc02-p01.pddl -cMQAp_1000.xml -sQueue_AStarFF.xml -sQueue_diversity.xml


Also included is the version of LPGd used for comparison in the study.  With help from the authors of that system, I made small changes to allow the planner to produce 1000 solutions.  The command line I used for my experiments was:

    lpgd  -f problem.pddl -o domain.pddl -cputime 360010  -incremental_max_num_solutions 1000 -d_coefficient 0.5  -out plan_d0.5 > lpg_output

