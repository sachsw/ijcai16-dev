
import os
import sys

print sys.argv
if len(sys.argv) < 2:
    print "You need to call this with the domain file and the name of the sanitized file you want to create"

domain_file=sys.argv[1]
output_file=sys.argv[2]

predicates_used=dict()

print "0. Sanitizing problem for domain {0} into {1}".format(domain_file, output_file)

found_inconsistent_state=False

## a collection of predicate names with the positions that must be unique
problem_predicates = dict()

def initialize_domain( domain ):
    if domain == "grid":
        problem_predicates["arm-empty"] = [ ]
        problem_predicates["at-robot"] = [ ]
        problem_predicates["at"] = [1]
    elif domain == "blocks":
        problem_predicates["hand-empty"] = [ ]
        problem_predicates["on"] = [1, 2]
        problem_predicates["on-table"] = [1]
        problem_predicates["holding"] = [1]
    elif domain == "logistics":
        problem_predicates["at"] = [ 1 ]
        problem_predicates["in"] = [ 1 ]
    elif domain == "navigator":
        problem_predicates["at"] = [ 1 ]
    else:
        print "Unknown domain {0}!".format(domain)
    
inside_init=False
with open(domain_file, 'r') as in_file:
    with open(output_file, 'w') as out_file:
        for line in in_file:
            if line.lower().find(":domain") >= 0:
                trimmed=line.strip()
                trimmed=trimmed.replace("(","")
                trimmed=trimmed.replace(")","")
                trimmed=trimmed.replace(":domain","")
                domain=trimmed.strip()
                initialize_domain(domain)
                print "Using problem predicates {0}".format(problem_predicates)
            if line.find(":init") >= 0:
                inside_init=True

            if line.find(":goal") >= 0:
                inside_init=False
                
            line_to_write = line
            if inside_init == True:
                trimmed=line.lower()
                trimmed=trimmed.strip()
                trimmed=trimmed.replace("(","")
                trimmed=trimmed.replace(")","")
                #print "trimmed:'{}'".format(trimmed)
                tokens=trimmed.split(" ")
                print "Examining init tokens:{0}".format(tokens)
                predicate = tokens[0]
                if predicate in problem_predicates:
                    unique_indexes = problem_predicates[predicate]
                    predicate_string = predicate
                    for index in unique_indexes:
                        value = tokens[index]
                        predicate_string += "_" + value
                    print "  Identified potential problem predicate {0}".format(predicate_string),
                    if predicate_string in predicates_used:
                        print "already used previously. commenting out!"
                        line_to_write = "; commented by sanitizer" + line
                    else:
                        print "no problemo! caching"
                        predicates_used[predicate_string] = True
            out_file.write(line_to_write)
