baseDir="../domains/rg/"

blocksDir="blocks/"
easygridDir="easygrid/"
easyipcDir="easyipc/"
idsDir="ids/"
ipcgridDir="ipcgrid/"
logisticsDir="logistics/"

domainBlocks=$baseDir$blocksDir"domain.pddl"
domainEasyGrid=$baseDir$easygridDir"domain.pddl"
domainEasyIPC=$baseDir$easyipcDir"domain.pddl"
domainIDS=$baseDir$idsDir"domain.pddl"
domainIPCGrid=$baseDir$ipcgridDir"domain.pddl"
domainLogistics=$baseDir$logisticsDir"domain.pddl"


echo ============================================
echo ==========Running Blocksworld Domain========
echo ============================================
../Metric-FF-new/ff -o $domainBlocks -f $baseDir$blocksDir"problem1.pddl"
../Metric-FF-new/ff -o $domainBlocks -f $baseDir$blocksDir"problem2.pddl"
../Metric-FF-new/ff -o $domainBlocks -f $baseDir$blocksDir"problem3.pddl"
../Metric-FF-new/ff -o $domainBlocks -f $baseDir$blocksDir"problem4.pddl"

echo ============================================
echo ==========Running Easy Grid Domain==========
echo ============================================
../Metric-FF-new/ff -o $domainEasyGrid -f $baseDir$easygridDir"problem1.pddl"
../Metric-FF-new/ff -o $domainEasyGrid -f $baseDir$easygridDir"problem2.pddl"
../Metric-FF-new/ff -o $domainEasyGrid -f $baseDir$easygridDir"problem3.pddl"
../Metric-FF-new/ff -o $domainEasyGrid -f $baseDir$easygridDir"problem4.pddl"

echo ============================================
echo ==========Running Easy IPC Domain===========
echo ============================================
../Metric-FF-new/ff -o $domainEasyIPC -f $baseDir$easyipcDir"problem1.pddl"
../Metric-FF-new/ff -o $domainEasyIPC -f $baseDir$easyipcDir"problem2.pddl"
../Metric-FF-new/ff -o $domainEasyIPC -f $baseDir$easyipcDir"problem3.pddl"
../Metric-FF-new/ff -o $domainEasyIPC -f $baseDir$easyipcDir"problem4.pddl"

echo ============================================
echo ==========Running IDS Domain================
echo ============================================
../Metric-FF-new/ff -o $domainIDS -f $baseDir$idsDir"problem1.pddl"
../Metric-FF-new/ff -o $domainIDS -f $baseDir$idsDir"problem2.pddl"
../Metric-FF-new/ff -o $domainIDS -f $baseDir$idsDir"problem3.pddl"
../Metric-FF-new/ff -o $domainIDS -f $baseDir$idsDir"problem4.pddl"

echo ============================================
echo ==========Running IPC Grid Domain===========
echo ============================================
../Metric-FF-new/ff -o $domainIPCGrid -f $baseDir$ipcgridDir"problem1.pddl"
../Metric-FF-new/ff -o $domainIPCGrid -f $baseDir$ipcgridDir"problem2.pddl"
../Metric-FF-new/ff -o $domainIPCGrid -f $baseDir$ipcgridDir"problem3.pddl"
../Metric-FF-new/ff -o $domainIPCGrid -f $baseDir$ipcgridDir"problem4.pddl"


echo ============================================
echo ==========Running Logistics Domain==========
echo ============================================
../Metric-FF-new/ff -o $domainLogistics -f $baseDir$logisticsDir"problem1.pddl"
../Metric-FF-new/ff -o $domainLogistics -f $baseDir$logisticsDir"problem2.pddl"
../Metric-FF-new/ff -o $domainLogistics -f $baseDir$logisticsDir"problem3.pddl"
../Metric-FF-new/ff -o $domainLogistics -f $baseDir$logisticsDir"problem4.pddl"
