import os
import shutil
import glob

benchmark_base_dir = "../domains/"
benchmark_to_run = "ijcai-revised"
domain_include_filter = []
#domain_include_filter = ["blocks"]
problem_include_filter = ""
#problem_include_filter = "problem1"
expected_domain_file = "domain.pddl"
expected_problem_file = "problem.pddl"


###
### Note: only one of these should be uncommented!
###
mosaic_variant = "ita-10"    #ita without preferred operators; 10 solutions
#mosaic_variant = "ita-100"    #ita without preferred operators; 100 solutions
# mosaic_variant = "mqa-ts-100"  #mqa +tabu +parsimonyQ ; 100 solutions
# mosaic_variant = "ita-p-100"  #ita +prefops; 100 solutions
# mosaic_variant = "mqa-tps-100"  #mqa +tabu +parsimonyQ ; 100 solutions

mosaic_args = dict()
mosaic_args["ita-100"] = "./mosaicPlan.sh domain.pddl problem.pddl -cITA_w100f080_100.xml" 
mosaic_args["ita-p-100"] = "./mosaicPlan.sh domain.pddl problem.pddl -cITA-p_w100f080_100.xml"
mosaic_args["mqa-ts-100"] = "./mosaicPlan.sh domain.pddl problem.pddl -cMQA-t_100.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml"
mosaic_args["mqa-tps-100"] = "./mosaicPlan.sh domain.pddl problem.pddl -cMQA-tp_100.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml"

mosaic_args["ita-10"] = "./mosaicPlan.sh domain.pddl problem.pddl -cITA_w100f080_10.xml" 
mosaic_args["ita-p-10"] = "./mosaicPlan.sh domain.pddl problem.pddl -cITA-p_w100f080_10.xml"
mosaic_args["mqa-ts-10"] = "./mosaicPlan.sh domain.pddl problem.pddl -cMQA-t_10.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml"
mosaic_args["mqa-tps-10"] = "./mosaicPlan.sh domain.pddl problem.pddl -cMQA-tp_10.xml -sQueue_AStarFF.xml -sQueue_dParsimony.xml"

#check that mosaic variant is valid


output_file = "mosaic_output"
files_to_clean = ["domain.pddl", "problem.pddl", "output", "test.groups", "all.groups", "output.sas", "solution*.txt", output_file]
files_to_capture = ["solution*.txt", output_file]

benchmark_dir = benchmark_base_dir + benchmark_to_run
domains = [ f for f in os.listdir(benchmark_dir) ]
print "Using domain_include_filter: {0}".format( domain_include_filter)
print "Using problem_include_filter: '{0}'".format(problem_include_filter)
for domain in domains:
    print "=========================================================================================================="
    print "=========================================================================================================="
    print "=========================================================================================================="
    print "=========================================================================================================="
    print "Checking domain:" + domain
    run_domain = False
    if len(domain_include_filter) > 0:
        #print "There is an include filter for domains"
        if (domain in domain_include_filter) == True:
            run_domain = True
    else:
        run_domain = True

    if run_domain == True:
        domain_path = benchmark_dir + "/" + domain
        print "Running domain {0} from path {1}".format(domain, domain_path)
        print "  (list of domains {0}".format(domains)
        dir_list = os.listdir(domain_path)
        #print dir_list
        if (problem_include_filter == ""):
            problems = [ f for f in dir_list]
        else:
            problems = [ f for f in dir_list if (f.find(problem_include_filter) >= 0)]

        #print problems
        domain_pddl_file = ""
        if expected_domain_file in problems:
            print "The domain file is in the current path"
            domain_pddl_file = domain_path + "/" + expected_domain_file
            problems.remove( expected_domain_file )
            
        for problem in problems:
            problem_path = domain_path + "/" + problem
            print "----------------------------------------------------------------------------------------------------------"
            print "----------------------------------------------------------------------------------------------------------"
            print "Working on problem path:" + problem_path

            run_mosaic = True
            if domain_pddl_file == "":
                if os.path.isdir(problem_path):
                    domain_pddl_file = problem_path + "/" + expected_domain_file
                else:
                    domain_pddl_file = domain_path + "/" + expected_domain_file

            print "  Checking domain file:" + domain_pddl_file,
            if os.path.exists(domain_pddl_file):
                print " found."
            else:
                print " missing! Mosaic cannot run."
                run_mosaic = False

            if os.path.isdir(problem_path):
                problem_pddl_file = problem_path + "/" + expected_problem_file
            else:
                problem_pddl_file = problem_path
            print "  Checking problem file:" + problem_pddl_file,
            if os.path.exists(problem_pddl_file):
                print " found."
            else:
                print " missing! Mosaic cannot run."
                run_mosaic = False

            if run_mosaic == True:
                print "  Cleaning files from previous run:",
                for file in files_to_clean:
                    if (file.find("*") > 0):
                        matching = glob.glob(file)
                        for match in matching:
                            print match,
                            os.remove(match)
                    else: 
                        if os.path.exists(file):
                            print file,
                            os.remove(file)
                print ""

                print "  Copying domain and problem pddl files"
                shutil.copyfile(domain_pddl_file, "./domain.pddl")
                shutil.copyfile(problem_pddl_file, "./problem.pddl")

                
                output_dir = problem_path.replace(benchmark_base_dir, "")
                output_dir = output_dir.replace(benchmark_to_run, benchmark_to_run.replace("/", "-") + "_" + mosaic_variant)
                
                print "  making output directory: '{0}'".format(output_dir)
                if os.path.exists(output_dir):
                    print "    first removed former output_dir '{0}'".format(output_dir)
                    shutil.rmtree(output_dir)
                os.makedirs(output_dir)

                print "  running mosaic variant: {0}".format(mosaic_variant)
                mosaic_command = mosaic_args[mosaic_variant] + " > " + output_file + " 2>&1"
                print "  running command: '" + mosaic_command + "'"
                os.system(mosaic_command)

                print "  Capturing files from run to {}".format(output_dir)
                for file in files_to_capture:
                    print "    Checking " + file + ":", 
                    if (file.find("*") > 0):
                        matching = glob.glob(file)
                        for match in matching:
                            print match,
                            shutil.copyfile(match, output_dir + "/" + match)
                        print ""
                    else: 
                        if os.path.exists(file):
                            print file
                            shutil.copyfile(file, output_dir + "/" + file)

                print "  Run completed."
