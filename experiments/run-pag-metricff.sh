
baseDir="../domains/pag/ijcai16/"
domainFile=$baseDir"attackgraph-typed-ijcai16-v1.0.pddl"

echo ============================================
echo ============================================
echo ============================================
../Metric-FF-new/ff -o $domainFile -f $baseDir"problem_1.pddl"

echo ============================================
echo ============================================
echo ============================================
../Metric-FF-new/ff -o $domainFile -f $baseDir"problem_2.pddl"

echo ============================================
echo ============================================
echo ============================================
../Metric-FF-new/ff -o $domainFile -f $baseDir"problem_3.pddl"

echo ============================================
echo ============================================
echo ============================================
../Metric-FF-new/ff -o $domainFile -f $baseDir"problem_4.pddl"
