#! /bin/sh

#adapted from the IPC2008 Lama code

# Paths to planner components
TRANSLATE="./lama/translate/translate.py"
PREPROCESS="./lama/preprocess/preprocess"
SEARCH="./MosaicEngine"

run_planner() {
    rm output
    rm test.groups
    rm all.groups
    rm output.sas
    rm solution*.txt
#     tar jxvf lama.tar.bz2
    echo "1. Running translator"
    "$TRANSLATE" "$1" "$2"
    echo "2. Running preprocessor"
    "$PREPROCESS" < output.sas
    if [ -f output ]  ; then
	echo "3. Running search"
	"$SEARCH" $3 $4 $5 $6 $7 $8 $9
    else
	echo "3. Failed - no preprocessor output file"
    fi
}

check_input_files() {
    if [ ! -e "$1" ]; then
	echo "Domain file \"$1\" does not exist."
	exit 1
    fi
    if [ ! -e "$2" ]; then
	echo "Problem file \"$2\" does not exist."
	exit 1
    fi
}

sanitize() {
    python sanitize_problem.py "$1" "$2"
}

# Make sure we have exactly 2 command line arguments
#if [ $# -ne 2 ]; then
#    echo "Usage: \"plan <domain_file> <problem_file> <result_file>\""
#    exit 1
#fi

check_input_files "$1" "$2"

sanitized_problem="$2".sanitized
sanitize "$2" "$sanitized_problem"


# Command line arguments seem to be fine, run planner
run_planner "$1" "$sanitized_problem" "$3" $4 $5 $6 $7 $8 $9 

# We do not clean up temporary files here (not necessary for IPC-2008)
