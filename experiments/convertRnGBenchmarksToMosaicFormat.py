import os
import tarfile
import shutil

rng_original_benchmark_dir = "../ramirez_geffner_dataset/benchmarks/"
rng_set_dirs = [ "aaai-10-benchmarks", "ijcai-09-benchmarks" ]
#rng_set_dirs = [ "aaai-10-benchmarks"]
new_benchmark_dir = "../domains/rng/"
set_dirs = dict()
for dir in rng_set_dirs:
    set_dirs[ dir ] =  dir.replace("-benchmarks", "")

domains = dict()
domains["aaai-10-benchmarks"] = [ "block-words", "bui-campus", "easy-ipc-grid", "kitchen", "logistics"]
#domains["aaai-10-benchmarks"] = [ "block-words" ]
domains["ijcai-09-benchmarks"] = [ "block-words", "easy-grid-navigation", "easy-ipc-grid", "intrusion-detection", "ipc-grid", "logistics"]

print "Working in directory:" + os.getcwd()

for problem_set_dir in set_dirs:
    rng_set_path = rng_original_benchmark_dir + problem_set_dir + "/"
    mosaic_set_path = new_benchmark_dir + set_dirs[problem_set_dir]
    print "Reading from RNG problem set path: " + rng_set_path
    print "  and saving to mosaic_set_path: " + mosaic_set_path
    for domain in domains[problem_set_dir]:
        mosaic_domain_path = mosaic_set_path + "/" + domain  + "/" 
        print "    gathering files for domain: " + domain
        rng_problem_files = [ ]

        rng_domain_path = rng_set_path + domain + "/" 
        rng_reading_path = rng_domain_path
        if (os.path.isdir(rng_domain_path)):
            rng_problem_files = [ f for f in os.listdir(rng_domain_path) ]
        else:
            rng_reading_path = rng_set_path
            rng_problem_files = [ f for f in os.listdir(rng_set_path) if f.startswith(domain)]
        for bzip_file in rng_problem_files:
            bzip_path = rng_reading_path + bzip_file
            output_path = mosaic_domain_path + bzip_file.replace(".tar.bz2", "")
            print "      Unpacking file: " + bzip_path
            print "        into : " + output_path
            if os.path.isdir(output_path):
                shutil.rmtree(output_path)

            with tarfile.open( bzip_path, "r" ) as tar:
                print "        Extracting...",
                # for tarinfo in tar:
                #     print tarinfo.name, "is", tarinfo.size, "bytes in size and is",
                #     if tarinfo.isreg():
                #         print "a regular file."
                #     elif tarinfo.isdir():
                #         print "a directory."
                #     else:
                #         print "something else."
                tar.extractall(output_path)
                print " done."
