(attacker-sends-direct-message-with-phishing-link user1 phishing-direct-message twitter-phishing-site)
(user-starts-software user1 browser-firefox)
(user-visits-site user1 browser-firefox twitter-site)
(user-login-to-twitter user1 twitter-site twitter-account user1-twitter-password)
(user-opens-twitter-inbox user1 twitter-account phishing-direct-message)
(user-reads-direct-message user1 twitter-account phishing-direct-message)
(user-clicks-link-in-direct-message user1 phishing-direct-message twitter-phishing-site)
(user-visits-phishing-site-via-direct-message user1 phishing-direct-message twitter-phishing-site)
(user-enters-information-in-phishing-site-twitter user1 twitter-phishing-site twitter-account)
(attacker-intercepts-twitter-information twitter-account user1-twitter-password twitter-phishing-site)
; ThisSolutionCPU:0
; RunCPU:0
; Diversity:0
