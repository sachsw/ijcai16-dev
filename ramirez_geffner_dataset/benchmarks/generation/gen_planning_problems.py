#!/usr/bin/python
import sys, os

def get_hypotheses( hyps_path ) :
	
	hyps = []
	instream = open( hyps_path )

	for line in instream :
		line = line.strip()
		atoms = [ tok.strip() for tok in line.split(',') ]
		hyps.append( atoms )

	instream.close()
	
	return hyps

def generate_planning_problems( instance_template_path, hyp_list ) :
	
	for i in range( 0, len(hyp_list) ) :

		# we will be writing the resulting problem while at the same time reading
		# the template, replacing the placeholder for goals on the fly
		instream = open( instance_template_path )
	
		# let's create the resulting planning problem file
		path_to_folder, _ = os.path.split( instance_template_path )
		print path_to_folder
		
		_, pr_prob_number, _ = path_to_folder.split( '/' )

		problem_name = "problem-%s-hyp_%s.pddl"%(pr_prob_number, i)
		new_problem_path = os.path.join( path_to_folder, problem_name )

		print >> sys.stdout, "Creating file:", new_problem_path
		outstream = open( new_problem_path, 'w' )

		for line in instream :
			line = line.strip()
			if '<HYPOTHESIS>' not in line :
				print >> outstream, line
			else :
				for atom in hyp_list[i] :
					print >> outstream, atom

		outstream.close()
		instream.close()	

def main() :
	if len(sys.argv) < 2 :
		print >> sys.stderr, "Insufficient arguments provided!"
		print >> sys.stderr, "Usage: ./gen_planning_problems.py <problem_folder>"
		sys.exit(1)

	problem_folder = sys.argv[1]
	if not os.path.exists( problem_folder ) or not os.path.isdir(problem_folder) :
		print >> sys.stderr, "Supplied problem folder:", problem_folder, "is not a folder :-/"
		sys.exit(1)
	
	print >> sys.stdout, "Problem Folder:", problem_folder

	domain_name, problem_name = os.path.split(problem_folder)

	# Check for hypotheses file
	hyps_file_path = os.path.join( problem_folder, "hyps.dat" )
	if not os.path.exists( hyps_file_path ) :
		print >> sys.stderr, "No hypothesis file 'hyps.dat' found at folder", problem_folder
		sys.exit(1)

	# Now check for planning files
	planning_files_path = os.path.join( problem_folder, "planning" )
	if not os.path.exists( planning_files_path ) :
		print >> sys.stderr, "Could not find folder", planning_files_path
	instance_template_path = os.path.join( planning_files_path, "template.pddl" )
	if not os.path.exists( instance_template_path ) :
		print >> sys.stderr, "Could not find file", instance_template_path	
	
	hyp_list = get_hypotheses( hyps_file_path )		

	generate_planning_problems( instance_template_path, hyp_list )


if __name__ == '__main__' :
	main()
