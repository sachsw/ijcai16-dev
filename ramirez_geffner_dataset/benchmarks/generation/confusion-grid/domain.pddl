;; simple Grid-navigation

(define (domain navigator)

	(:requirements :strips :typing :action-costs)
	(:types place)
	(:predicates
		(at ?p - place)
		(connected ?p1 ?p2 - place)
	)
  	(:functions
	     (distance ?l1 ?l2 - place) - number
	     (total-cost) - number
	)
	(:action MOVE
		:parameters (?src - place ?dst - place)
		:precondition (and (at ?src) (connected ?src ?dst) )
		:effect (and 
			(at ?dst) 
			(increase (total-cost) (distance ?src ?dst))
			(not (at ?src)))
	)
)		
