#!/usr/bin/python
import sys, os, math

class Grid_Node :

	def __init__( self ) :
		self.object_name = ""
		self.initial_position = False
		self.north = None
		self.south = None
		self.west = None
		self.east = None
		self.northwest = None
		self.northeast = None
		self.southwest = None
		self.southeast = None

	def generate_predicates( self ) :
		preds = []
		if self.north is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.north.object_name) )
			preds.append( '(= (distance %s %s) 1000)'%(self.object_name, self.north.object_name) )
		if self.south is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.south.object_name) )
			preds.append( '(= (distance %s %s) 1000)'%(self.object_name, self.south.object_name) )

		if self.west is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.west.object_name) )
			preds.append( '(= (distance %s %s) 1000)'%(self.object_name, self.west.object_name) )

		if self.east is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.east.object_name) )
			preds.append( '(= (distance %s %s) 1000)'%(self.object_name, self.east.object_name) )

		diag_dist = math.sqrt(2.0)
		if self.northwest is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.northwest.object_name) )
			preds.append( '(= (distance %s %s) 1414)'%(self.object_name, self.northwest.object_name) )
		if self.northeast is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.northeast.object_name) )
			preds.append( '(= (distance %s %s) 1414)'%(self.object_name, self.northeast.object_name) )
		if self.southwest is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.southwest.object_name) )
			preds.append( '(= (distance %s %s) 1414)'%(self.object_name, self.southwest.object_name) )
		if self.southeast is not None :
			preds.append( '(connected %s %s)'%(self.object_name, self.southeast.object_name) )
			preds.append( '(= (distance %s %s) 1414)'%(self.object_name, self.southeast.object_name) )

		if self.initial_position :
			preds.append( '(at %s)'%self.object_name )
		return preds	

def generate_grid( grid_lines ) :
	grid = dict()
	for y in range (0, len(grid_lines) ) :
		for x in range( 0, len(grid_lines[y]) ) :
			if grid_lines[y][x] == '#' :
				grid[ (x, y ) ] = None
			elif grid_lines[y][x] == 'I' :
				cell = Grid_Node()
				cell.initial_position = True
				cell.object_name = 'place_%d_%d'%(x,y)
				grid[ (x,y) ] = cell
			else :
				cell = Grid_Node()
				cell.object_name = 'place_%d_%d'%(x,y)
				grid[ (x,y) ] = cell

	for y in range (0, len(grid_lines) ) :
		for x in range( 0, len(grid_lines[y]) ) :
			if grid[ (x,y) ] is None : continue
			try:
				grid[ (x,y) ].west = grid[ (x-1,y) ]
			except KeyError :
				pass
			try :
				grid[ (x,y) ].east = grid[ (x+1,y) ]
			except KeyError :
				pass
			try:
				grid[ (x,y) ].northwest = grid[ (x-1,y-1) ]
			except KeyError :
				pass
			try :
				grid[ (x,y) ].northeast = grid[ (x+1,y-1) ]
			except KeyError :
				pass
			try:
				grid[ (x,y) ].southwest = grid[ (x-1,y+1) ]
			except KeyError :
				pass
			try :
				grid[ (x,y) ].southeast = grid[ (x+1,y+1) ]
			except KeyError :
				pass

			try :
				grid[ (x,y) ].north = grid[ (x,y-1) ]
			except KeyError :
				pass
			try :
				grid[ (x,y) ].south = grid[ (x,y+1) ]
			except KeyError :
				pass
	return grid
						

def parse_grid_description( grid_desc_file ) :
	instream = open( grid_desc_file )
	name = None
	grid_lines = []

	for line in instream :
		line = line.strip()
		if ";" in line : # name
			name = line[1:].strip()
		else :
			grid_lines.append( line )
	instream.close()
	return name, grid_lines

def write_pddl_instance( name, grid, grid_lines ) :
	instream = open( 'grid-template.pddl' )
	for line in instream :
		line = line.strip()
		if '<NAME>' in line :
			print >> sys.stdout, line.replace( '<NAME>', name )
		elif '<OBJECTS>' in line :
			for y in range (0, len(grid_lines) ) :
				for x in range( 0, len(grid_lines[y]) ) :
					cell = grid[(x,y)]
					if cell is None :
						continue
					print >> sys.stdout, cell.object_name,
				print >> sys.stdout
			print >> sys.stdout, '- place'
		elif '<INITIAL>' in line :
			print >> sys.stdout, "(= (total-cost) 0)"
			for _, cell in grid.iteritems() :
				if cell is None : continue
				print >> sys.stdout, " ".join( cell.generate_predicates() )
		else :
			print >> sys.stdout, line
				
	instream.close()	

def main() :
	
	if len( sys.argv ) != 2 :
		print >> sys.stderr, "Missing arguments!"
		print >> sys.stderr, "Usage: ./generate_grid.py <grid file>"
		sys.exit(1)

	desc_file = sys.argv[1]

	if not os.path.exists( desc_file ) :
		print >> sys.stderr, "Could not open grid description file", desc_file
		sys.exit(1)

	grid_name, grid_lines = parse_grid_description( desc_file )
	grid = generate_grid( grid_lines )
	write_pddl_instance( grid_name, grid, grid_lines )

if __name__ == '__main__' :
	main()

