(define (problem <NAME>)

	(:domain navigator)
	(:objects
		<OBJECTS>
	)
	(:init
		<INITIAL>
	)
	(:goal
		(and
			<HYPOTHESIS>
		)
	)
)
