#!/usr/bin/python
import sys, random, os

class Node :
	
	def __init__(self, path_idx, idx ) :
		self.name = 'place_%s_%s'%(path_idx, idx)
		self.neighbors = []

	def connect_with( self, node ) :
		self.neighbors.append( node )

	def at_pred( self ) :
		return '(at %s)'%self.name

	def conn_preds( self ) :
		pred_list = []
		for n in self.neighbors :
			pred_list.append( '(connected %s %s)'%( self.name, n.name ) )
		return pred_list

class Path :

	def __init__( self, idx, length ) :
		self.nodes = []
		for i in range(0, length ) :
			n = Node( idx, i )
			if i > 0 :
				n.connect_with( self.nodes[-1] )
				self.nodes[-1].connect_with( n )	
			self.nodes.append( n )

def generate_hyps( hyps_path, paths ) :
	
	outstream = open( hyps_path, 'w' )

	for p in paths :
		print >> outstream, p.nodes[-1].at_pred()	

	outstream.close()

def generate_template( pddl_path, paths ) :
	
	instream = open( 'grid-template.pddl' )	
	outstream = open( pddl_path, 'w' )
	for line in instream :
		line = line.strip()
		if '<NAME>' in line :
			print >> outstream, line.replace( '<NAME>', 'navigation-%s-%s'%(len(paths),len(paths[0].nodes)) )
		elif '<OBJECTS>' in line :
			for p in paths :
				for n in p.nodes :
					print >> outstream, n.name
			print >> outstream, '- place'
		elif '<INITIAL>' in line :
			print >> outstream, paths[0].nodes[0].at_pred()
			for p in paths :
				for n in p.nodes :
					print >> outstream, " ".join( n.conn_preds() )
		else :
			print >> outstream, line

	outstream.close()

def main() :
	
	if len(sys.argv) < 3 :
		print >> sys.stderr, "Missing parameters!"
		print >> sys.stderr, "Usage: random_grid.py <path len> <num paths>"	
		sys.exit(1)

	N = int(sys.argv[1])
	M = int(sys.argv[2])

	# Create directories for storing the problem 
	base_dir_name = 'p%s-%s'%(N,M)
	planning_dir_name = os.path.join( base_dir_name, 'planning' )
	if os.path.exists( base_dir_name ) :
		os.system( 'rm -rf %s'%base_dir_name )
	os.system( 'mkdir -p %s'%planning_dir_name )
	template_pddl_path = os.path.join( planning_dir_name, 'template.pddl' )
	hyps_path = os.path.join( base_dir_name, 'hyps.dat' )
	
	paths = []
	for i in range( 0, M ) :
		paths.append( Path( i, N ) )
	# connect all heads of paths
	for i in range( 0, M-1 ) :
		paths[i].nodes[0].connect_with( paths[i+1].nodes[0] )
		paths[i+1].nodes[0].connect_with( paths[i].nodes[0] )

	# now randomly connect the intermediate nodes in each path
	for i in range( 0, M-1, 2) :
		for j in range(1, N-1) :
			x = random.random()
			if x < 0.5 :
				paths[i].nodes[j].connect_with( paths[i+1].nodes[j] )
				paths[i+1].nodes[j].connect_with( paths[i].nodes[j] )
		
	generate_hyps( hyps_path, paths )
	generate_template( template_pddl_path, paths )	

if __name__ == '__main__' :
	main()
