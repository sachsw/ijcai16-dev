(define (problem navigation-5-10)

(:domain navigator)
(:objects
place_0_0
place_0_1
place_0_2
place_0_3
place_0_4
place_0_5
place_0_6
place_0_7
place_0_8
place_0_9
place_1_0
place_1_1
place_1_2
place_1_3
place_1_4
place_1_5
place_1_6
place_1_7
place_1_8
place_1_9
place_2_0
place_2_1
place_2_2
place_2_3
place_2_4
place_2_5
place_2_6
place_2_7
place_2_8
place_2_9
place_3_0
place_3_1
place_3_2
place_3_3
place_3_4
place_3_5
place_3_6
place_3_7
place_3_8
place_3_9
place_4_0
place_4_1
place_4_2
place_4_3
place_4_4
place_4_5
place_4_6
place_4_7
place_4_8
place_4_9
- place
)
(:init
(at place_0_0)
(connected place_0_0 place_0_1) (connected place_0_0 place_1_0)
(connected place_0_1 place_0_0) (connected place_0_1 place_0_2)
(connected place_0_2 place_0_1) (connected place_0_2 place_0_3)
(connected place_0_3 place_0_2) (connected place_0_3 place_0_4)
(connected place_0_4 place_0_3) (connected place_0_4 place_0_5)
(connected place_0_5 place_0_4) (connected place_0_5 place_0_6)
(connected place_0_6 place_0_5) (connected place_0_6 place_0_7) (connected place_0_6 place_1_6)
(connected place_0_7 place_0_6) (connected place_0_7 place_0_8) (connected place_0_7 place_1_7)
(connected place_0_8 place_0_7) (connected place_0_8 place_0_9)
(connected place_0_9 place_0_8)
(connected place_1_0 place_1_1) (connected place_1_0 place_0_0) (connected place_1_0 place_2_0)
(connected place_1_1 place_1_0) (connected place_1_1 place_1_2)
(connected place_1_2 place_1_1) (connected place_1_2 place_1_3)
(connected place_1_3 place_1_2) (connected place_1_3 place_1_4)
(connected place_1_4 place_1_3) (connected place_1_4 place_1_5)
(connected place_1_5 place_1_4) (connected place_1_5 place_1_6)
(connected place_1_6 place_1_5) (connected place_1_6 place_1_7) (connected place_1_6 place_0_6)
(connected place_1_7 place_1_6) (connected place_1_7 place_1_8) (connected place_1_7 place_0_7)
(connected place_1_8 place_1_7) (connected place_1_8 place_1_9)
(connected place_1_9 place_1_8)
(connected place_2_0 place_2_1) (connected place_2_0 place_1_0) (connected place_2_0 place_3_0)
(connected place_2_1 place_2_0) (connected place_2_1 place_2_2)
(connected place_2_2 place_2_1) (connected place_2_2 place_2_3)
(connected place_2_3 place_2_2) (connected place_2_3 place_2_4)
(connected place_2_4 place_2_3) (connected place_2_4 place_2_5) (connected place_2_4 place_3_4)
(connected place_2_5 place_2_4) (connected place_2_5 place_2_6) (connected place_2_5 place_3_5)
(connected place_2_6 place_2_5) (connected place_2_6 place_2_7) (connected place_2_6 place_3_6)
(connected place_2_7 place_2_6) (connected place_2_7 place_2_8)
(connected place_2_8 place_2_7) (connected place_2_8 place_2_9) (connected place_2_8 place_3_8)
(connected place_2_9 place_2_8)
(connected place_3_0 place_3_1) (connected place_3_0 place_2_0) (connected place_3_0 place_4_0)
(connected place_3_1 place_3_0) (connected place_3_1 place_3_2)
(connected place_3_2 place_3_1) (connected place_3_2 place_3_3)
(connected place_3_3 place_3_2) (connected place_3_3 place_3_4)
(connected place_3_4 place_3_3) (connected place_3_4 place_3_5) (connected place_3_4 place_2_4)
(connected place_3_5 place_3_4) (connected place_3_5 place_3_6) (connected place_3_5 place_2_5)
(connected place_3_6 place_3_5) (connected place_3_6 place_3_7) (connected place_3_6 place_2_6)
(connected place_3_7 place_3_6) (connected place_3_7 place_3_8)
(connected place_3_8 place_3_7) (connected place_3_8 place_3_9) (connected place_3_8 place_2_8)
(connected place_3_9 place_3_8)
(connected place_4_0 place_4_1) (connected place_4_0 place_3_0)
(connected place_4_1 place_4_0) (connected place_4_1 place_4_2)
(connected place_4_2 place_4_1) (connected place_4_2 place_4_3)
(connected place_4_3 place_4_2) (connected place_4_3 place_4_4)
(connected place_4_4 place_4_3) (connected place_4_4 place_4_5)
(connected place_4_5 place_4_4) (connected place_4_5 place_4_6)
(connected place_4_6 place_4_5) (connected place_4_6 place_4_7)
(connected place_4_7 place_4_6) (connected place_4_7 place_4_8)
(connected place_4_8 place_4_7) (connected place_4_8 place_4_9)
(connected place_4_9 place_4_8)
)
(:goal
(and
(at place_0_9)
)
)
)
