#!/usr/bin/python
import sys, os, math, random

def scan_for_domains() :
	domain_folders = []
	for item in os.listdir('.') :
		if os.path.isdir( item ) :
			domain_folders.append( item )
	return domain_folders

def clear_work_dir() :
	if os.path.exists( 'temp' ) :
		os.system( 'rm -rf temp' )
	os.system( 'mkdir temp' )
	return 'temp'
	
def load_plan_for_hyp( base, i ) :
	path_to_solutions = os.path.join( base, 'planning' )
	path_to_plan_for_hyp = None
	for entry in os.listdir( path_to_solutions ) :
		if 'hyp_%d.soln'%i in entry :
			path_to_plan_for_hyp = os.path.join( path_to_solutions, entry )
			break
	if path_to_plan_for_hyp is None : return
	instream = open( path_to_plan_for_hyp )
	plan = []
	for line in instream :
		line = line.strip()
		if line[0] == ';' : continue
		_, _, stuff = line.partition(':')
		op, _, _ = stuff.partition('[')
		plan.append( op.strip().upper() )	
	instream.close()
	return plan

def load_not_observable_actions( base ) :
	path_to_not_observable_actions = os.path.join( base, 'not-observable' )
	if not os.path.exists( path_to_not_observable_actions ) : return []
	action_list = []
	instream = open( path_to_not_observable_actions )
	for line in instream :
		line = line.strip()
		action_list.append( line.upper() )
	instream.close()
	return action_list

def sample_ops_from_plan( plan, pct, not_observable = [] ) :

	observable = []
	for i in range(0, len(plan)) :
		observable_action = True
		for name in not_observable :
			if name in plan[i] :
				observable_action = False
				break
		if observable_action :
			observable.append( i )

	sampled_indices = random.sample( observable, int(math.ceil((pct/100.0) * len(observable)) ))
	sampled_indices.sort()
	obs_seq = [ plan[i] for i in sampled_indices ]	
	return obs_seq

def process_domain( work_dir, domain ) :
	
	print >> sys.stdout, "Processing contents of folder", domain
	# Check that the folder has a 'domain.pddl' file in it
	path_to_domain_pddl = os.path.join( domain, 'domain.pddl' )
	if not os.path.exists( path_to_domain_pddl ) :
		print >> sys.stderr, "Did not find a 'domain.pddl' file inside", domain, "... ignoring it"
		return
	# load not observable actions
	not_obs_actions = load_not_observable_actions( domain )
	domain_work_dir = os.path.join( work_dir, domain )
	os.system( 'mkdir %s'%domain_work_dir )
	# scan (F,I,A) defined for this domain
	inst_skel_dirs = []
	for item in os.listdir( domain ) :
		if os.path.isdir( os.path.join( domain, item  ) ) :
			if os.path.exists( os.path.join( domain, os.path.join( item, 'hyps.dat' ) ) ) :	
				inst_skel_dirs.append( os.path.join( domain, item ) )
	print "Directories found with skeleton problems:", inst_skel_dirs	
	# process instance skeletons
	for dir in inst_skel_dirs :
		
		hyps_file_path = os.path.join( dir, 'hyps.dat' )
		if not os.path.exists( hyps_file_path ) : 
			print "File", hyps_file_path, "not found"
			continue
		template_file_path = os.path.join( os.path.join( dir, 'planning' ), 'template.pddl' )
		hyps = []
		instream = open( hyps_file_path )
		for line in instream :
			line = line.strip()
			hyps.append( line )
		instream.close()

		print "Valid Hypotheses and plans"
		valid_hyps_and_plans = []
		for i in range( 0, len(hyps) ) :
			plan = load_plan_for_hyp( dir, i )
			if plan is not None :
				valid_hyps_and_plans.append( (i, plan) )
				print i, plan
		
		# generate full observation PR instance
		for index, plan in valid_hyps_and_plans :
			tmp_dir = os.path.join( '/tmp', 'tarballs')		
			os.system( 'rm -rf %s'%tmp_dir )
			os.system( 'mkdir %s'%tmp_dir )
			os.system( 'cp %s %s'%( path_to_domain_pddl, tmp_dir) )	
			os.system( 'cp %s %s'%( template_file_path, tmp_dir) )
			outstream = open( os.path.join( tmp_dir, 'hyps.dat' ), 'w' )
			for i, _ in valid_hyps_and_plans :
				print >> outstream, hyps[i]
			outstream.close()
			outstream = open( os.path.join( tmp_dir, 'real_hyp.dat' ), 'w' )
			print >> outstream, hyps[index]
			outstream.close()
			outstream = open( os.path.join( tmp_dir, 'obs.dat' ), 'w' )
			for op in plan :
				observable = True
				for name in not_obs_actions :
					if name in op :
						observable = False
						break
				if observable :
					print >> outstream, op
			outstream.close()
			current = os.getcwd()
			os.chdir( tmp_dir )
			tarball_name = '%s_%s_hyp-%s_full.tar.bz2'%(domain, os.path.basename(dir), index)
			os.system( 'tar jcvf %s *'%tarball_name )
			os.chdir( current )
			os.system( 'mv %s %s'%( os.path.join( tmp_dir, tarball_name) ,domain_work_dir) )

		# generate sampled instances
		percentages = [ 70, 50, 30, 10 ]
		for index, plan in valid_hyps_and_plans :
			for pct in percentages :
				for num_obs in range(0,3) :
					tmp_dir = os.path.join( '/tmp', 'tarballs')		
					os.system( 'rm -rf %s'%tmp_dir )
					os.system( 'mkdir %s'%tmp_dir )
					os.system( 'cp %s %s'%( path_to_domain_pddl, tmp_dir) )	
					os.system( 'cp %s %s'%( template_file_path, tmp_dir) )
					outstream = open( os.path.join( tmp_dir, 'hyps.dat' ), 'w' )
					for i, _ in valid_hyps_and_plans :
						print >> outstream, hyps[i]
					outstream.close()
					outstream = open( os.path.join( tmp_dir, 'real_hyp.dat' ), 'w' )
					print >> outstream, hyps[index]
					outstream.close()
					obs_seq = sample_ops_from_plan( plan, pct, not_obs_actions )
					outstream = open( os.path.join( tmp_dir, 'obs.dat' ), 'w' )
					for op in obs_seq :
						print >> outstream, op
					outstream.close()
					current = os.getcwd()
					os.chdir( tmp_dir )
					tarball_name = '%s_%s_hyp-%s_%s_%s.tar.bz2'%(domain, os.path.basename(dir), index, pct, num_obs)
					print >> sys.stdout, "Creating tarball", tarball_name
					os.system( 'tar jcvf %s *'%tarball_name )
					os.chdir( current )
					os.system( 'mv %s %s'%( os.path.join( tmp_dir, tarball_name) ,domain_work_dir) )
		

def main() :

	work_dir = clear_work_dir()
	if len(sys.argv) == 1 : 
		domains = scan_for_domains()
		print domains
		for domain in domains :
			process_domain( work_dir, domain )
	else :
		process_domain( work_dir, sys.argv[1] )	

if __name__ == '__main__' :
	main()
