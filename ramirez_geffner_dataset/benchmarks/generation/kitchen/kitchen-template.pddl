(define (problem <NAME>)
	(:domain kitchen)
	(:objects
		<OBJECTS>
	)
	(:init
		<INITIAL>
	)
	(:goal
		(and
			<HYPOTHESIS>
		)
	)
	(:metric minimize (total-cost))
)
