(define (problem strips-grid-2)
(:domain  grid)
(:objects node_A node_B node_C node_D node_E
node_F node_G node_H node_I node_J
node_K - place
triangle square star circle - shape
key0 key1 key2 key3 - key)
(:init
(conn node_A node_C)
(conn node_C node_A)
(conn node_C node_B)
(conn node_C node_D)
(conn node_B node_C)
(conn node_D node_E)
(conn node_D node_G)
(conn node_D node_C)
(conn node_G node_D)
(conn node_G node_H)
(conn node_H node_G)
(conn node_H node_I)
(conn node_I node_H)
(conn node_E node_D)
(conn node_E node_F)
(conn node_F node_E)
(conn node_F node_J)
(conn node_J node_F)
(conn node_J node_K)
(conn node_K node_J)
(open node_A)
(locked node_B)
(lock-shape node_B square)
(open node_C)
(open node_D)
(open node_G)
(locked node_H)
(lock-shape node_H star)
(open node_I)
(locked node_E)
(lock-shape node_E triangle)
(open node_F)
(open node_J)
(locked node_K)
(lock-shape node_K circle)
(key-shape key0 triangle)
(at key0 node_C)
(key-shape key1 circle)
(at key1 node_I)
(key-shape key2 star)
(at key2 node_F)
(key-shape key3 square)
(at key3 node_K)
(at-robot node_A)
)
(:goal
(and
(at-robot node_B)
)
)
)
