#!/usr/bin/python
import sys, random

def load_blocks( fname ) :
	blocks = []
	instream = open( fname )
	for line in instream :
		line = line.strip()
		for tok in line.split() : blocks.append( tok ) 
	instream.close()

	return blocks

def write_blocks_problem( blocks, stacks ) :
	
	print >> sys.stdout, """
(define (problem blocks_words)
	(:domain blocks)
(:objects 
"""

	for b in blocks :
		print >> sys.stdout, b,
	print >> sys.stdout, "- block"
	print >> sys.stdout, ")"
	print >> sys.stdout, "(:init"
	print >> sys.stdout, "(HANDEMPTY)"
	for s in stacks :
		if len(s) == 0 : continue
		print >> sys.stdout, "(CLEAR %s)"%s[0]
		for i in range(0,len(s)-1) :
			print >> sys.stdout, "(ON %s %s)"%(s[i],s[i+1])
		print >> sys.stdout, "(ONTABLE %s)"%s[-1]

	print >> sys.stdout, ")"
	print >> sys.stdout, "(:goal (and"
	print >> sys.stdout, "<HYPOTHESIS>"
	print >> sys.stdout, "))"
	print >> sys.stdout, ")"

def generate_stacks( blocks ) :

	stacks = []
	for i in range(0, len(blocks)) :
		stacks.append( [] )

	for b in blocks :
		pos = random.randint(0, len(stacks)-1)
		stacks[pos].append( b )	

	return stacks

def main() :
	
	if len(sys.argv) < 2 :
		print >> sys.stderr, "Specify the file with list of blocks"
		sys.exit(1)

	blocks = load_blocks( sys.argv[1] )

	stacks = generate_stacks( blocks )

	write_blocks_problem( blocks, stacks )

if __name__ == '__main__' :
	main()
