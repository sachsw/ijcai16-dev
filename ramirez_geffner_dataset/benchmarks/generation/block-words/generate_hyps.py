#!/usr/bin/python
import sys

def main() :
	
	if len( sys.argv ) < 2 :
		print >> sys.stderr, "Please specify the word list"
		sys.exit(1)

	# get the words and generate hypothesis
	instream = open( sys.argv[1] )
	for line in instream :
		word = line.strip()
		atoms = [ '(CLEAR %s)'%word[0], '(ONTABLE %s)'%word[-1] ]
		for i in range( 0, len(word)-1) :
			atoms.append( '(ON %s %s)'%(word[i],word[i+1]) )
		print >> sys.stdout, ",".join( atoms )
		
	instream.close()

if __name__ == '__main__' :
	main()
