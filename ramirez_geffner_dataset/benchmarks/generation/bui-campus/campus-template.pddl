(define (problem <NAME>)
	(:domain campus)
	(:objects
		<OBJECTS>
	)
	(:init
		<INITIAL>
	)
	(:goal
		(and
			<HYPOTHESIS>
		)
	)
	(:metric minimize (total-cost))
)
