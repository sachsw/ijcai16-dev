import sys, os

def main() :
	
	domain_folders = []

	for item in os.listdir('.') :
		if os.path.isdir( item ) :
			domain_folders.append( item )

	if os.path.exists( 'temp' ) :
		os.system( 'rm -rf temp' )
	if os.path.exists( 'opt_planning_problems.zip' ) :
		os.system( 'rm opt_planning_problems.zip' )	
	os.system( 'mkdir temp' )
	work_dir = 'temp'

	for folder in domain_folders :
		print >> sys.stdout, "Processing contents of folder", folder
		# Check that the folder has a 'domain.pddl' file in it
		path_to_domain_pddl = os.path.join( folder, 'domain.pddl' )
		if not os.path.exists( path_to_domain_pddl ) :
			print >> sys.stderr, "Did not find a 'domain.pddl' file inside", folder, "... ignoring it"
			continue
		domain_name = folder
		domain_work_dir = os.path.join( work_dir, domain_name )
		os.system( 'mkdir %s'%domain_work_dir )
		os.system( 'cp %s %s'%( path_to_domain_pddl, domain_work_dir ) )
		instances_path = os.path.join( domain_work_dir, 'instances' )
		os.system( 'mkdir %s'%instances_path )
		os.system( 'find %s -name "*.pddl" > pddl.files'%folder )
		instream = open( 'pddl.files' )
		for line in instream :
			line = line.strip()
			if 'domain' in line or 'template' in line : continue
			os.system( 'cp %s %s'%(line, instances_path) )
		instream.close()
	os.chdir( 'temp' )
	os.system( 'zip -r ../opt_planning_problems.zip *' )
	os.chdir( '..' )
			

if __name__ == '__main__' :
	main()
