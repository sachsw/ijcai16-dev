(define (problem intrusion-detection-10-hosts)
(:domain intrusion-detection)
(:objects
perseus cassiopea andromeda sagittarius scorpio
virgo aries leo libra taurus - host )
(:init
(dummy)
)
(:goal
(and
(vandalized scorpio)
(data-stolen-from scorpio)
(data-stolen-from virgo)
)
)
)
