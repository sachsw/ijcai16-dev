#!/usr/bin/python
from PR.evaluation           import Test
from PR.evaluation.selection import Crawler
from PR.evaluation.selection import Dataset
from PR.evaluation.selection import Filter 
from PR.evaluation.selection import Separator
from PR.evaluation.plots     import Plot
from PR.evaluation.plots     import Obs_Acc_Plot

import getopt
import sys
import os

class Options :

	def __init__( self, args ) :
		try :
			opts, args = getopt.getopt( args,
					"hi:o:",
					["input=",
					"output="] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)
		self.directory = None
		self.output = None

		for opcode, oparg in opts :
			if opcode in ('-h', '--help' ) :
				print >> sys.stderr, "Help invoked"
				self.usage()
				sys.exit(1)
			if opcode in ('-i', '--input') :
				self.directory = oparg
			if opcode in ('-o', '--output') :
				self.output = oparg
		if self.directory is None :
			print >> sys.stderr, "No directory was specified!"
			sys.exit(1)
		if self.output is None :
			print >> sys.stderr, "No output was specified!"
			sys.exit(1)
		if not os.path.isdir( self.output ) :
			print >> sys.stderr, self.output, "is not a directory!"
		if not os.path.exists( self.output ) :
			print >> sys.stderr, self.output, "doesn't exist, creating it"
			os.mkdirs( self.output )

	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs: Plotting Accuracy for different observation levels"
		print >> sys.stderr, "-i --input       <directory>        Directory holding the data"
		print >> sys.stderr, "-o --output      <path>             Path to directory to store resulting diagrams (PDF)"
		print >> sys.stderr, "-h --help                           Shows this help message"
		
def main() :
	
	opt = Options( sys.argv[1:] )	
	
	res_crawler = Crawler( opt.directory )
	res_crawler.crawl()
	tests = []
	for path in res_crawler.test_paths :
		t = Test(0.0)
		t.load( path )
		t.compute_statistics()
		tests.append(t)

	for domain in [ 'office', 'explorer', 'kitchen', 'drawers' ] :
		datasets = {}
		for p in [ 30, 50, 70 ] :
			datasets[p] = Dataset( tests )
			datasets[p].filters.append( Filter( 'domain', domain ) )
			datasets[p].filters.append( Filter( 'obs_pct', p ) )
			datasets[p].separators.append( Separator( 'runs' ) )
			datasets[p].selected_fields = [ 'ACC' ]

		csvs = {}
		for pct, dataset in datasets.iteritems() :
			print pct
			dataset.make()
			csvs[pct] = 'tmp-obs-acc-%d.csv'%pct
			dataset.save_as_csv( csvs[pct], False )

		plot = Obs_Acc_Plot( csvs )
		plot.save_as_PDF( os.path.join(opt.output, '%s-obs-acc.pdf'%domain ) )	

if __name__ == '__main__' :
	main()
