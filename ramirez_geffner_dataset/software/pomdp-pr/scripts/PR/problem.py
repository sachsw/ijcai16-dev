import os
import sys

class Problem :

	def __init__( self, folder, task_index, goal_index ) :
		self.template = []
		self.domain = None
		self.task_index = task_index
		self.obs = None
		self.goal = None
		self.goal_index = goal_index
		self.init = None
		self.load( folder )

	def load( self, folder ) :
		fname = os.path.join( folder, 'problem.tmpl' )
		if not os.path.exists( fname ) :
			print >> sys.stderr, "Could not find problem template in", folder
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		instream = open( fname )
		for line in instream :
			self.template.append( line.strip() )
		instream.close()

	def make_name( self ) :
		#if self.obs is None :
		self.name = self.domain.name + '_G%d'%self.goal_index
		self.pddl_output = 'problem_T%d_G%d.pddl'%(self.task_index,self.goal_index)
		#else :
		#	if self.obs.negated :
		#		self.name = self.domain.name + '_not_O_G%d'%self.goal_index
		#		self.pddl_output = 'problem_T%d_not_O_G%d.pddl'%(self.task_index,self.goal_index)
		#	else :
		#		self.name = self.domain.name + '_O_G%d'%self.goal_index
		#		self.pddl_output = 'problem_T%d_O_G%d.pddl'%(self.task_index,self.goal_index)				

	def prepare( self ) :
		self.compile_pddl()
	
	def compile_pddl( self ) :
		self.pddl = []
		#self.pddl += self.domain.compile_obs( self.obs )
		self.pddl += self.compile_obs()	

	def compile_obs( self ) :
		self.result = self.domain.compile_obs( None, self.goal )
		for line in self.template :
			if '@' in line :
				if 'ProblemName' in line :
					self.result.append( self.name )
				elif 'DomainName' in line :
					self.result.append( self.domain.name )
				elif 'InitialSituation' in line :
					for p in self.init.set_preds :
						self.result.append( p )
				elif 'ObservationsInitialization' in line :
					continue
					#if self.obs is None : continue
					#self.result += self.obs.state_initializations()
				elif 'GoalSituation' in line :
					for p in self.goal.preds :
						self.result.append( p )
				elif 'ObsGoal' in line:
					if self.obs is None : continue		
					self.result.append( self.obs.declare_goal() )
			else :
				self.result.append( line )

	def write_pddl( self, fname = None ) :
		if fname is not None :
			self.pddl_output = fname 
		outstream = open( self.pddl_output, 'w' )
		for l in self.result :
			print >> outstream, l
		outstream.close()
