import sys
import os
import copy

class TaskDescription :

	def __init__( self, task_idx, goal_idx, probs, o, gen_not_O = True ) :
		self.task_idx = task_idx
		self.true_goal = goal_idx
		self.problems = probs
		self.obs = o
		print self.obs.vars
		print self.obs.obs_actions
		self.computations = []
		self.generate_not_O = gen_not_O

	def make_and_pack( self, task_id, target_dir ) :
		target_path = os.path.abspath( target_dir )
		current_dir = os.getcwd()	
		#if os.path.exists( 'work-generate' ) :
			#	os.system( 'rm -rf work-generate' )
		#	os.chdir( 'work-generate' )
		#	os.system( 'rm -rf *.log *.pddl *.output *.core tmp.core' )
		#	os.chdir( current_dir )
		#else :
		#	os.mkdir( 'work-generate' )
		os.chdir( 'work-generate' )
		for i in range(0,len(self.problems)) :
			p = copy.deepcopy(self.problems[i])
			p.obs = copy.deepcopy(self.obs)
			p.obs.negated = False
			p.make_name()
			p.compile_obs()
			p.write_pddl()
			self.computations.append( (i, p.goal.prior, True, p.pddl_output ) )
			if self.generate_not_O :
				p = copy.deepcopy(self.problems[i])
				p.obs = copy.deepcopy(self.obs)
				p.obs.negated = True
				p.make_name()
				p.compile_obs()
				p.write_pddl()
				self.computations.append( (i, False, p.pddl_output ) )
		self.write_descriptor()
		tarball_name = 'pr-%s-%d.tar.bz2'%(self.problems[0].domain.name, task_id)
		os.system( 'tar jcvf %s *.pddl *.core task.descriptor'%tarball_name )
		os.system( 'mv %s %s'%( tarball_name, target_dir ) )
		os.chdir( current_dir )

	def write_descriptor( self ) :
		outstream = open( 'task.descriptor', 'w' )
		print >> outstream, 'domain=%s'%self.problems[0].domain.name
		print >> outstream, 'task=%s'%self.task_idx
		print >> outstream, 'true_goal=%s'%self.true_goal
		print >> outstream, 'obs_pct=%s'%self.obs.level
		print >> outstream, 'obs_len=%s'%len(self.obs.obs_actions)
		print >> outstream, 'obs_seq=%s'%self.obs.obs_actions
		policy_name = 'greedy'
		if self.use_soft_max : policy_name = 'soft-max'
		print >> outstream, 'policy=%s'%policy_name
		print >> outstream, 'beta=%s'%self.beta
		for comp in self.computations :
			print >> outstream, 'computation=%s,%s,%s,%s'%comp
		outstream.close()
