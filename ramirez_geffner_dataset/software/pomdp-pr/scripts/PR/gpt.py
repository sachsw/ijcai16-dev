import os
import sys
import re
import benchmark
import glob

class Script :

	def __init__(self, name ) :
		self.name = name
		self.commands = []
	
	def emit( self, fname = None ) :
		if fname is not None :
			self.name = fname
		outstream = open( self.name, 'w' )
		for cmd in self.commands :
			print >> outstream, cmd
		outstream.close()

	def set( self, var, value ) :
		self.commands.append( 'set %s %s'%(var, value) )
	
	def parse( self, name, pddl ) :
		self.commands.append( 'parse %s %s'%(name, pddl) )
	
	def compile( self, dom_name, prob_name ) :
		self.commands.append( 'compile %s_%s'%(dom_name,prob_name) )

	def solve( self, dom_name, prob_name ) :
		self.commands.append( 'solve %s_%s'%(dom_name,prob_name) )

	def evaluate( self, dom_name, prob_name ) :
		self.commands.append( 'evaluate verbose %s_%s'%(dom_name,prob_name) )

	def clean( self, dom_name, prob_name ) :
		self.commands.append( 'shell make -f Makefile.%s_%s deep-clean'%(dom_name,prob_name) )

	def generate_core( self, core_name ) :
		self.commands.append( 'generate core %s'%core_name )

	def load_core( self, core_name ) :
		self.commands.append( 'load core %s'%core_name )

class Trial :

	def __init__( self ) :
		self.cost = None
		self.length = None
		self.actions = None
		self.state = None

	def describe( self ) :
		print >> sys.stdout, "Trial"
		print >> sys.stdout, "c(\\pi)", self.cost
		print >> sys.stdout, "|\\pi|=", self.length
		pritn >> sys.stdout, "\\pi=", ",".join( self.actions )

class PolicySimulation :

	def __init__( self ) :
		self.trials = []
		self.b0_value_pattern = re.compile( r'\<v=(?P<b0_value>\d+\.\d+)\>' )
		self.cost_pattern = re.compile( r'\<c=(?P<cost>\d+\.\d+)\>' )
		self.len_pattern = re.compile( r'\<d=(?P<length>\d+\.\d+)\>' )
		self.actseq_pattern = re.compile( r'\<p=(?P<actions>.+)\>' )
		self.act_pattern = re.compile( r'\<(?P<action>[\w\d_]+\(\)),' )
		self.final_state_pattern = re.compile( r'\<T={(?P<fluents>.+)}\>' )
		self.fluent_pattern = re.compile( r'_(?P<fluent_name>[\w\d_]+)=(?P<value>\d),' )

	def describe( self ) :
		print >> sys.stdout, "V(b0)=", self.b0_value
		

	def load( self, prob_name, dom_name ) :
		fname = '%s_%s.evaluation.output'%(prob_name, dom_name)
		instream = open( fname )
		for line in instream :	
			if line[0] == '%' : continue
			t = Trial()
			line = line.strip()
			fields = line.split(':')
			for f in fields :
				m = self.b0_value_pattern.search(f)
				if m is not None :
					self.b0_value = float(m.group('b0_value'))
					continue
				m = self.cost_pattern.search(f)
				if m is not None :
					t.cost = float(m.group('cost'))
					continue
				m = self.len_pattern.search(f)
				if m is not None :
					t.length = float(m.group('length'))	
					continue
				m = self.actseq_pattern.search(f)
				if m is not None :
					raw_seq = m.group('actions')
					if raw_seq == 'null' :
						t.actions = []
						continue
					seq = []
					for m in self.act_pattern.finditer(raw_seq) :
						seq.append( m.group('action') )
					t.actions = seq[0:-1]
				m = self.final_state_pattern.search(f)
				if m is not None :
					raw_state = m.group('fluents')
					t.state = {}
					if raw_state == 'null' :
						continue
					for m in self.fluent_pattern.finditer(raw_state) :
						f = m.group('fluent_name')
						v = int(m.group('value'))
						t.state[f] = v
					#print >> sys.stdout, "State:"
					#for f, v in t.state.iteritems() :
					#	print >> sys.stdout, f, "=", v
			#print >> sys.stdout, "Trial: cost:", t.cost, "len:", t.length
			if t.actions is not None :
				self.trials.append(t)

		instream.close()

	def compute_prob_O( self, obs_seq ) :
		self.num_sat_O = 0
		self.num_unsat_O = 0
		self.avg_sat_O_cost = 0.0
		self.avg_unsat_O_cost = 0.0
		
		unsat_trials = []
		for i in range(0, len(self.trials)) :
			#print self.satisfies_obs( self.trials[i], obs_len)
			if self.satisfies_obs( self.trials[i], obs_seq) :
				self.num_sat_O += 1	
				self.avg_sat_O_cost += float(self.trials[i].cost)
				#print >> sys.stdout, "Satisfies Obs"	
			else :
				self.num_unsat_O += 1
				self.avg_unsat_O_cost += float(self.trials[i].cost)
				unsat_trials.append(self.trials[i])

		print >> sys.stdout, "Unsat Trials"
		for t in unsat_trials :
			print >> sys.stdout, t.actions	

		if self.num_sat_O == 0 :
			self.avg_sat_O_cost = 'n/a'
		else :	
			self.avg_sat_O_cost /= float(self.num_sat_O)
		if self.num_unsat_O == 0 :
			self.avg_unsat_O_cost = 'n/a'
		else :
			self.avg_unsat_O_cost /= float(self.num_unsat_O)

		return float(self.num_sat_O) / float(len(self.trials))
	
	def satisfies_obs( self, trial, obs_seq ) :
		i = 0
		j = 0
		for i in range(0,len(trial.actions)) :
			if trial.actions[i] == obs_seq[j] :
				j+=1
			if j == len(obs_seq) : break
			i += 1
		return j == len(obs_seq)
		#num_sat = 0
		#for f, v in trial.state.iteritems() :
		#	if 'accounted_for' in f  :
		#		if v == 1 :
		#			num_sat += 1
		#return num_sat == int(obs_len)

	def compute_statistics( self ) :
		self.avg_cost = 0.0
		self.avg_len = 0.0
		for i in range(0,len(self.trials)) :
			self.avg_cost += self.trials[i].cost
			self.avg_len += self.trials[i].length
		self.avg_cost = self.avg_cost / float(len(self.trials))
		self.avg_len = self.avg_len / float(len(self.trials))

class GPT :
	
	def __init__( self, home ) :
		self.home = home
		self.gpt_bin_path = os.path.join( self.home, os.path.join( 'bin', 'gpt') )
		self.cmd_head = 'GPTHOME="%s" %s'%(self.home, self.gpt_bin_path) 
	
	def solve_and_simulate2( self, pname, dname, pddl, nruns = 100, beta = 1.0 ) :
		
		script = Script( pddl.replace('pddl','gpt') )
		cname = pddl.replace('_O', '' )
		cname = cname.replace('pddl','core')
		script.set( 'output-level', 1 )
		script.parse( pname, pddl )
		script.compile( pname, dname )
		script.set( 'runs', 1 )
		#script.set( 'cutoff', 1000 )
		#script.set( 'epsilon', 0.001 )
		#script.set( 'stoprule', 0.001 )
		script.set( 'discretization-levels', 15 )
		#script.solve( pname, dname )
		#script.set( 'incremental-mode', 'on' )
		script.load_core( cname )
		script.set( 'runs', nruns )
		if beta is not None :
			script.set( 'beta', beta )
			script.set( 'soft-max-policy', 1 )
		script.evaluate( pname, dname ) 
		script.clean( pname, dname )

		script.emit()
		log = benchmark.Log( '%s.log'%pname )
		cmd = '%s < %s'%( self.cmd_head, script.name )
		rv, time = benchmark.run( cmd, 1800, 1024, log )
		
		if rv != 0 : 
			print >> sys.stderr, "POMDP problem could not be solved!"
			print >> sys.stderr, "Got return code:", res
			return None
		
		sim = PolicySimulation()
		sim.load( pname, dname )
		
		return sim, time

	def solve_and_simulate( self, problem, use_soft_max, beta, work_dir ) :
		problem.compile_obs()
		cname =  problem.pddl_output.replace('pddl','core')	
	
		script = Script( problem.pddl_output.replace('pddl','gpt') )
		script.set( 'output-level', 1 )
		script.parse( problem.name, problem.pddl_output )
		script.compile( problem.name, problem.domain.name )
		#script.set( 'runs', 1000 )
		script.set( 'cutoff', 1000 )
		script.set( 'epsilon', 0.001 )
		#script.set( 'stoprule', 'off' )
		script.set( 'stoprule', 0.001 )
		script.set( 'discretization-levels', 15 )
		script.solve( problem.name, problem.domain.name )
		script.set( 'runs', 100 )
		script.set( 'beta', beta )
		if use_soft_max :
			script.set( 'soft-max-policy', 1 )
		script.evaluate( problem.name, problem.domain.name ) 
		script.generate_core( cname )
		script.clean( problem.name, problem.domain.name )

		current_dir = os.getcwd()
		if os.path.exists( work_dir ) :
			os.chdir( work_dir )
			files = glob.glob( '*' )
			for f in files :
				if 'core' or 'gpt' in files : continue
				os.system( 'rm -rf %s'%f )
			os.chdir(current_dir)
		else :
			os.mkdir( work_dir )
		os.chdir( work_dir )

		problem.write_pddl()
		script.emit()
		log = benchmark.Log( '%s.log'%problem.name )
		cmd = '%s < %s'%( self.cmd_head, script.name )
		rv, time = benchmark.run( cmd, 1800, 1024, log )
		
		if rv != 0 : 
			print >> sys.stderr, "POMDP problem could not be solved!"
			print >> sys.stderr, "Got return code:", res
			return None
		
		sim = PolicySimulation()
		sim.load( problem.name, problem.domain.name )

		os.chdir( current_dir )

		return sim 
