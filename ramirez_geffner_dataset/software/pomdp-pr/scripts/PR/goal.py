import os
import sys
import glob

def load_goals( folder ) :
	goals = []
	goal_templates = glob.glob( os.path.join( folder, '*.goal' ) )
	goal_templates.sort()
	prior_values = glob.glob( os.path.join( folder, '*.prior' ) )
	prior_values.sort()
	for i in range(0, len(goal_templates)) :
		goals.append(Goal(goal_templates[i], prior_values[i]))
	return goals

class Goal :

	def __init__( self, goal_desc_file, prior_file ) :
		self.preds = []
		self.prior = 0.0
		self.load( goal_desc_file )
		self.retrieve_prior( prior_file )

	def load( self, goal_fname ) :
		instream = open( goal_fname )
		for line in instream :
			self.preds.append( line.strip() )
		instream.close()	

	def retrieve_prior( self, prior_fname ) :
		instream = open(prior_fname)
		text = instream.read()
		instream.close()
		self.prior = float(text)

	def describe( self ) :
		for p in self.preds :
			print >> sys.stdout, p
		print >> sys.stdout, "with probability", self.prior
	
	def make_true_statements( self ) :
		var_value = []
		for p in self.preds :
			p2 = p.replace('(','')
			p2 = p2.replace(')','')
			_, var, value = p2.split(' ')
			var_value += [ (var, value) ]
		return [ '(:set %s %s)'%t for t in var_value ]
		

