import tarfile
import sys
import os
import math

class Computation :
	
	def __init__( self, domain, goal_index, prior ) :
		self.domain = domain
		self.goal_index = goal_index 
		self.prior = float(prior)
		self.O_pddl = None

	def solve( self, solver, obs_seq, nruns, beta ) :
		self.O_problem_name = '%s_G%d'%(self.domain,self.goal_index)
		self.O_sim, self.O_time = solver.solve_and_simulate2( self.O_problem_name, self.domain, self.O_pddl, nruns, beta )
		self.compute_probabilities(obs_seq)
	
	def compute_probabilities( self, obs_seq ) :
		self.O_sim.compute_statistics()
		self.prob_O = self.O_sim.compute_prob_O(obs_seq)
		self.num_sat_O = self.O_sim.num_sat_O
		self.num_unsat_O = self.O_sim.num_unsat_O
		self.O_cost = self.O_sim.avg_sat_O_cost
		self.not_O_cost = self.O_sim.avg_unsat_O_cost 
		self.b0_value = self.O_sim.b0_value

class Task :

	def __init__( self, tarball, solver ) :
		self.solver = solver
		self.tarball_path = os.path.abspath(tarball)
		self.tarball = os.path.basename(tarball)
		self.setup_task()

	def setup_task( self ) :
		if os.path.exists( 'work' ) :
			os.system( 'rm -rf work' )
		os.mkdir( 'work' )	
		tar = tarfile.open( self.tarball_path, 'r' )
		# check that there is a task.description file
		if not 'task.descriptor' in tar.getnames() :
			print >> sys.stderr, "Tarball", self.tarball, "does not contain a task.descriptor file"
			print >> sys.stderr, "Aborting"
			tar.close()
			sys.exit(1)
		current_dir = os.path.abspath(os.getcwd())
		os.chdir( 'work' )
		# extract all files
		tar.extractall()
		self.process_descriptor()
		os.chdir( current_dir )

	def process_descriptor( self ) :
		if not os.path.exists('task.descriptor') :
			print >> sys.stderr, "Task descriptor not found in current directory:", os.getcwd()
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		with open( 'task.descriptor' ) as instream :
			self.computations = {}
			for line in instream :
				line = line.strip()
				key, value = line.split('=')
				if 'obs_seq' in key :
					value = value.replace("[","")
					value = value.replace("]","")
					value = value.replace("'","")
					self.obs_seq = [ s.strip() for s in value.split(',') ]
				elif 'computation' in key :
					g, p, O, pddl = value.split(',')
					g = int(g)
					try :
						self.computations[g].O_pddl = pddl
					except KeyError :
						self.computations[g] = Computation(self.domain, g, p)
						self.computations[g].O_pddl = pddl
				else :
					setattr( self, key, value )
		print >> sys.stdout, "Computations to perform"
		print >> sys.stdout, "domain:", self.domain
		print >> sys.stdout, "task:", self.task
		print >> sys.stdout, "%Obs:", self.obs_pct
		print >> sys.stdout, "|O|:", self.obs_len
		for _, c in self.computations.iteritems() :
			print >> sys.stdout, "Goal index:", c.goal_index, "G+O:", c.O_pddl
						
	def solve( self, nruns, beta ) :
		self.nruns = nruns
		self.beta = beta
		if not os.path.exists( 'work' ) :
			print >> sys.stderr, "'work' folder not found!"
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		current_dir = os.path.abspath( os.getcwd() )
		os.chdir( 'work' )

		self.time = 0.0	
		for _, c in self.computations.iteritems() :
			c.solve(self.solver, self.obs_seq,  nruns, beta )
			self.time = c.O_time 
			
		os.chdir( current_dir )
		self.compute_probabilities()
	
	def compute_probabilities( self ) :
		self.prob_dist = [ (g, c.prob_O, c.prior) for g, c in self.computations.iteritems() ]
		print self.prob_dist	
		prob_sum = 0.0
		for _, p, prior in self.prob_dist :
			prob_sum += p*prior
		if prob_sum == 0.0 :
			self.prob_dist = [ (g, 0.0) for g, p, prior in self.prob_dist ]
		else :
			self.prob_dist = [ (g, ((p*prior)/prob_sum)) for g, p, prior in self.prob_dist ]
	
	def write_result( self, dir ) :
		self.result = self.tarball.replace('tar.bz2', 'result' )
		self.result = os.path.join( dir, self.result )
		with open( self.result, 'w' ) as outstream :
			print >> outstream, "domain=%s"%self.domain
			print >> outstream, "task=%s"%self.task
			print >> outstream, "true_goal=%s"%self.true_goal
			print >> outstream, "obs_pct=%s"%self.obs_pct
			print >> outstream, "obs_len=%s"%self.obs_len
			print >> outstream, "time=%f"%self.time
			print >> outstream, "runs=%s"%self.nruns	
			print >> outstream, "beta=%s"%self.beta
			for t in self.prob_dist :
				print >> outstream, "goal_prob=%s,%s"%t
			for _, c in self.computations.iteritems() :
				data = []
				data += [str(c.goal_index)]
				data += [str(c.b0_value)]
				data += [str(c.O_time)]
				data += [str(c.prob_O)]
				data += [str(c.num_sat_O)]
				data += [str(c.num_unsat_O)]
				data += [str(c.O_cost)]
				data += [str(c.not_O_cost)]
				print >> outstream, "computation=%s"%",".join(data)	
