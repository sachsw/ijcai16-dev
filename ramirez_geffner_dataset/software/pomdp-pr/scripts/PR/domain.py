import os
import sys
import re

def load_non_observable_actions( folder ) :
	acts = []

	act_list_fname = os.path.join( folder, 'non-observable.list' )	
	if os.path.exists( act_list_fname ) :
		with open( act_list_fname ) as instream :
			for line in instream :
				line = line.strip()
				acts.append( line )	

	return acts

class Domain :

	def __init__( self, folder ) :
		self.template = []
		self.action_locs = {}
		self.load( folder )

	def load( self, folder ) :
		input_name = os.path.join( folder, 'domain.tmpl' )
		if not os.path.exists( input_name ) :
			print >> sys.stderr, "Could not find file 'domain.tmpl' in folder", folder
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		instream = open( input_name )
		for line in instream :
			self.template.append(line.strip())
		instream.close()
		print >> sys.stdout, "'domain.tmpl' loaded"
		self.locate_actions()
		self.get_domain_name()

	def get_domain_name( self ) :
		for i in range(0, len(self.template) ) :
			line = self.template[i]
			if 'define' in line and 'domain' in line :  
				break
		# now we have the line
		pattern = re.compile(r'\(domain\s+(?P<name>[\w_]+)\)')
		match = pattern.search(line)
		if match is None :
			print >> sys.stderr, "Revise the regex, it is not detecting the domain name"
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		self.name = match.group('name')

	def locate_actions( self ) :
		for i in range (0,len(self.template)) :
			line = self.template[i]
			if ':action' in line :
				tokens = line.split()
				self.action_locs[tokens[1]] = i
		#print >> sys.stdout, "Actions located"
		#for action_name in self.action_locs.keys() :
		#	print >> sys.stdout, action_name
			
	def compile_obs( self, obs, goal ) :
		result = []
		current_obs = None
		line = 0
		for line in self.template :
			if '@' in line :
				if obs is None : continue
				if 'ObsVariables' in line :
					result += obs.declare_vars()
				elif 'ObsEffect' in line :
					if current_obs is None : continue
					for o in current_obs :
						result.append( obs.declare_effect(o))
				elif 'AbandonObsSequence' in line :
					if obs.negated : continue
					result += obs.declare_abandon_op(goal)
				elif 'AbandonNotObsSequence' in line :
					if not obs.negated : continue	
					result += obs.declare_abandon_op(goal)
			elif ':action' in line :
				if obs is not None : 
					_, name = line.split()
					current_obs = obs.match(name)
				result.append( line )
			else :
				result.append( line )
		return result	
