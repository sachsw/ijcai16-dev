import os
import sys

class Initial :
	
	def __init__( self, folder ) :
		self.set_preds = []
		self.load( folder )

	def load( self, folder ) :
		fname = os.path.join( folder, 'I1.init' )
		if not os.path.exists( fname ) :
			print >> sys.stderr, "Could not find initial state description", fname
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		instream = open( fname )
		for line in instream :
			line = line.strip()
			self.set_preds.append(line)
		instream.close()
