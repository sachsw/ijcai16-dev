import tarfile
import sys
import os
import math

class Computation :
	
	def __init__( self, domain, goal_index ) :
		self.domain = domain
		self.goal_index = goal_index 
		self.O_pddl = None
		self.not_O_pddl = None

	def solve( self, solver ) :
		self.O_problem_name = '%s_O_G%d'%(self.domain,self.goal_index)
		self.not_O_problem_name ='%s_not_O_G%d'%(self.domain,self.goal_index)
		self.O_sim, self.O_time = solver.solve_and_simulate2( self.O_problem_name, self.domain, self.O_pddl )
		self.not_O_sim, self.not_O_time = solver.solve_and_simulate2( self.not_O_problem_name, self.domain, self.not_O_pddl )
		self.compute_probabilities()
	
	def compute_probabilities( self ) :
		self.O_sim.compute_statistics()
		self.not_O_sim.compute_statistics()
		self.delta = self.not_O_sim.avg_cost - self.O_sim.avg_cost
		self.prob_O = 1.0 / ( 1.0 + math.exp( -1.0*self.delta ) ) 
		self.prob_not_O = 1.0 - self.prob_O

class Task :

	def __init__( self, tarball, solver ) :
		self.solver = solver
		self.tarball_path = os.path.abspath(tarball)
		self.tarball = os.path.basename(tarball)
		self.setup_task()

	def setup_task( self ) :
		if os.path.exists( 'work' ) :
			os.system( 'rm -rf work' )
		os.mkdir( 'work' )	
		tar = tarfile.open( self.tarball_path, 'r' )
		# check that there is a task.description file
		if not 'task.descriptor' in tar.getnames() :
			print >> sys.stderr, "Tarball", self.tarball, "does not contain a task.descriptor file"
			print >> sys.stderr, "Aborting"
			tar.close()
			sys.exit(1)
		current_dir = os.path.abspath(os.getcwd())
		os.chdir( 'work' )
		# extract all files
		tar.extractall()
		self.process_descriptor()
		os.chdir( current_dir )

	def process_descriptor( self ) :
		if not os.path.exists('task.descriptor') :
			print >> sys.stderr, "Task descriptor not found in current directory:", os.getcwd()
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		with open( 'task.descriptor' ) as instream :
			self.computations = {}
			for line in instream :
				line = line.strip()
				key, value = line.split('=')
				if not 'computation' in key :
					setattr( self, key, value )
				else :
					g, O, pddl = value.split(',')
					g = int(g)
					if O == 'True' : O = True
					else : O = False
					try :
						if O :
							self.computations[g].O_pddl = pddl
						else :
							self.computations[g].not_O_pddl = pddl
					except KeyError :
						self.computations[g] = Computation(self.domain, g)
						if O :
							self.computations[g].O_pddl = pddl
						else :
							self.computations[g].not_O_pddl = pddl
		#print >> sys.stdout, "Computations to perform"
		#print >> sys.stdout, "domain:", self.domain
		#print >> sys.stdout, "task:", self.task
		#print >> sys.stdout, "%Obs:", self.obs_pct
		#print >> sys.stdout, "|O|:", self.obs_len
		#for _, c in self.computations.iteritems() :
		#	print >> sys.stdout, "Goal index:", c.goal_index, "G+O:", c.O_pddl, "G+\overline{O}:", c.not_O_pddl
						
	def solve( self ) :
		if not os.path.exists( 'work' ) :
			print >> sys.stderr, "'work' folder not found!"
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		current_dir = os.path.abspath( os.getcwd() )
		os.chdir( 'work' )

		self.time = 0.0	
		for _, c in self.computations.iteritems() :
			c.solve(self.solver)
			self.time = c.O_time + c.not_O_time
			
		os.chdir( current_dir )
		self.compute_probabilities()
	
	def compute_probabilities( self ) :
		self.prob_dist = [ (g, c.prob_O) for g, c in self.computations.iteritems() ]	
		prob_sum = 0.0
		for _, p in self.prob_dist :
			prob_sum += p
		self.prob_dist = [ (g, p/prob_sum) for g, p in self.prob_dist ]
	
	def write_result( self, dir ) :
		self.result = self.tarball.replace('tar.bz2', 'result' )
		self.result = os.path.join( dir, self.result )
		with open( self.result, 'w' ) as outstream :
			print >> outstream, "domain=%s"%self.domain
			print >> outstream, "task=%s"%self.task
			print >> outstream, "true_goal=%s"%self.true_goal
			print >> outstream, "obs_pct=%s"%self.obs_pct
			print >> outstream, "obs_len=%s"%self.obs_len
			print >> outstream, "time=%f"%self.time
			for t in self.prob_dist :
				print >> outstream, "goal_prob=%s,%s"%t
			for _, c in self.computations.iteritems() :
				data = []
				data += [str(c.goal_index)]
				data += [str(c.delta)]
				data += [str(c.O_time)]
				data += [str(c.prob_O)]
				data += [str(c.not_O_time)]
				data += [str(c.prob_not_O)]
				print >> outstream, "computation=%s"%",".join(data)	
