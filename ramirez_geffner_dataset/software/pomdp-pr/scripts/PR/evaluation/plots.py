import math
import os
import sys
import re
import tempfile
import csv

class PyxPlot :

	def __init__( self ) :
		self.xlabel = 'X'
		self.ylabel = 'Y'
		self.title = 'Untitled'
		self.data_file = ''
		self.data_title = ''
		self.output = ''

	def write( self, template, fname ) :
		script_lines = []
		placeholder_pattern = re.compile( r'@(?P<field>[\w\d_]+)@' )
		with open( template ) as instream :
			for line in instream :
				line = line.strip()
				for m in placeholder_pattern.finditer(line) :
					line = line.replace( '@%s@'%m.group('field'), 
								getattr(self, m.group('field') ) )
				script_lines.append( line )
		with open( fname, 'w' ) as outstream :
			for l in script_lines :
				print >> outstream, l
class Plot :

	def __init__(self, template) :
		self.template = template 
		self.script= None
		self.aux_files = []

	def make_script( self, output ) :
		self.output = output
		self.set_values()
		self.generate_file()

	def set_values( self ) :
		pass		

	def generate_file( self ) :
		script_lines = []
		placeholder_pattern = re.compile( r'@(?P<field>[\w\d_]+)@' )
		with open( self.template ) as instream :
			for line in instream :
				line = line.strip()
				for m in placeholder_pattern.finditer(line) :
					line = line.replace( '@%s@'%m.group('field'), 
								str(getattr(self, m.group('field') )) )
				script_lines.append( line )
		self.script = 'script.tmp'
		with open( self.script, 'w' ) as output :
			for l in script_lines :
				print >> output, l
				print >> sys.stdout, l
	
	def save_as_PDF( self, pdf_fname ) :
		eps_fname = pdf_fname.replace('pdf','eps')
		self.make_script(eps_fname)
		os.system( 'pyxplot %s'%self.script )
		os.system( 'ps2pdf -dEPSCrop -dEPSFitPage %s %s'%(eps_fname, pdf_fname) )
		os.system( 'rm %s'%self.script )
		os.system( 'rm %s'%eps_fname )
		for f in self.aux_files : os.system( 'rm %s'%f )

class Probability_Curve(Plot) :

	def __init__(self, group) :
		Plot.__init__(self, 'templates/prob_curves.tmpl.ppl' )
		self.group = group
		self.N_values = []
		self.beta_values = []
		self.goals = []
		self.N_probs = dict()
		self.N_stddevs = dict()
		self.N_counts = dict()
		self.beta_probs = dict()
		self.beta_stddevs = dict()
		self.beta_counts = dict()
		self.get_params()
		print self.N_values
		print self.beta_values
		print self.goals
		for N in self.N_values :
			self.N_probs[N] = [0 for i in self.goals]
			self.N_stddevs[N] = [ 0 for i in self.goals ]
			self.N_counts[N] = 0
		for beta in self.beta_values :
			self.beta_probs[beta] = [0 for i in self.goals]
			self.beta_stddevs[beta] = [ 0 for i in self.goals ]
			self.beta_counts[beta] = 0
		self.get_probs_and_stats()

	def get_prob_and_stats( self ) :
		pass

	def get_params( self ) :
		self.N_values = []
		self.beta_values = []
		self.goals = []
		for t in self.group :
			beta_t = t.beta
			N_t = int(t.runs)
			Gset_t = t.computations.keys() 
				
			if beta_t not in self.beta_values : self.beta_values += [beta_t]
			if N_t not in self.N_values : self.N_values += [N_t]
			for g in Gset_t :
				if g not in self.goals :
					self.goals.append(g)
		self.N_values.sort()
		self.beta_values.sort()
		self.goals.sort()

class Obs_Prob_Curve(Probability_Curve) :
	
	def __init__( self, group ) :
		Probability_Curve.__init__(self,group)

	def set_values( self ) :
		self.title_top = "$P(O|G_i)$ v. $N$"
		self.ylabel_top = "$P(O|G_i)$"
		self.xmin_top = str(self.N_values[0])
		self.xmax_top = str(self.N_values[-1])
		self.title_bottom = "$P(O|G_i)$ v. $\\beta$"
		self.ylabel_bottom = "$P(O|G_i)$"
		self.xmin_bottom = str(self.beta_values[0])
		self.xmax_bottom = str(self.beta_values[-1])
		# plot command for left
		goal_csvs_N = [ 'data-N-%d.csv'%i for i in self.goals ]
		goal_csvs_beta = [ 'data-beta-%d.csv'%i for i in self.goals ]
		self.aux_files += goal_csvs_N + goal_csvs_beta
		for i in self.goals :
			with open( goal_csvs_N[i], 'w' ) as output :
				for N in self.N_values :
					u = self.N_probs[N][i]
					o = self.N_stddevs[N][i]
					print >> output, "%(N)d,%(u)f,%(o)f"%locals()
		for i in self.goals :
			with open( goal_csvs_beta[i], 'w' ) as output :
				for beta in self.beta_values :
					u = self.beta_probs[beta][i]
					o = self.beta_stddevs[beta][i]
					print >> output, "%(beta)f,%(u)f,%(o)f"%locals()
		self.plot_command_top = "plot "
		for i in range(0,len(self.goals)) :
			fragment = "'%s' title '$P(O|G_%d)$' using 1:2 with lines, "%( goal_csvs_N[self.goals[i]], self.goals[i] )
			fragment += "'%s' title '' with errorbars"%goal_csvs_N[self.goals[i]]
			self.plot_command_top += fragment
			if i < len(self.goals)-1 :
				self.plot_command_top += ", "
		self.plot_command_bottom = "plot "
		for i in range(0,len(self.goals)) :
			fragment = "'%s' title '$P(O|G_%d)$' using 1:2 with lines, "%( goal_csvs_beta[self.goals[i]], self.goals[i] )
			fragment += "'%s'title '' with errorbars"%goal_csvs_beta[self.goals[i]]
			self.plot_command_bottom += fragment
			if i < len(self.goals)-1 :
				self.plot_command_bottom += ", "
			
			
	def get_probs_and_stats( self ) :
		for t in self.group :
			goals = t.computations.keys()
			goals.sort()
			self.N_counts[t.runs] += 1
			self.beta_counts[t.beta] += 1
			for i in range(0,len(goals)) :
				self.N_probs[t.runs][goals[i]] += t.computations[goals[i]].prob_O
				self.beta_probs[t.beta][goals[i]] += t.computations[goals[i]].prob_O
		for k in self.N_probs.keys() :
			u = self.N_probs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.N_counts[k] )
		for k in self.beta_probs.keys() :
			u = self.beta_probs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.beta_counts[k] )
		for t in self.group :
			goals = t.computations.keys()
			goals.sort()
			for i in range(0,len(goals)) :
				v = t.computations[goals[i]].prob_O - self.N_probs[t.runs][goals[i]]
				v *= v
				self.N_stddevs[t.runs][goals[i]] += v
				v = t.computations[goals[i]].prob_O - self.beta_probs[t.beta][goals[i]]
				v *= v
				self.beta_stddevs[t.beta][goals[i]] += v
		for k in self.N_stddevs.keys() :
			u = self.N_stddevs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.N_counts[k] - 1.0 )
				u[i] = math.sqrt( u[i] )
		for k in self.beta_stddevs.keys() :
			u = self.beta_stddevs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.beta_counts[k] - 1.0 )
				u[i] = math.sqrt( u[i] )
		
			
class Goal_Prob_Curve(Probability_Curve) :
	
	def __init__( self, group ) :
		Probability_Curve.__init__(self,group)

	def set_values( self ) :
		self.title_top = "$P(G_i|O)$ v. $N$"
		self.ylabel_top = "$P(G_i|O)$"
		self.xmin_top = str(self.N_values[0])
		self.xmax_top = str(self.N_values[-1])
		self.title_bottom = "$P(G_i|O)$ v. $\\beta$"
		self.ylabel_bottom = "$P(G_i|O)$"
		self.xmin_bottom = str(self.beta_values[0])
		self.xmax_bottom = str(self.beta_values[-1])
		# plot command for left
		goal_csvs_N = [ 'data-N-%d.csv'%i for i in self.goals ]
		goal_csvs_beta = [ 'data-beta-%d.csv'%i for i in self.goals ]
		self.aux_files += goal_csvs_N + goal_csvs_beta
		for i in self.goals :
			with open( goal_csvs_N[i], 'w' ) as output :
				for N in self.N_values :
					u = self.N_probs[N][i]
					o = self.N_stddevs[N][i]
					print >> output, "%(N)d,%(u)f,%(o)f"%locals()
		for i in self.goals :
			with open( goal_csvs_beta[i], 'w' ) as output :
				for beta in self.beta_values :
					u = self.beta_probs[beta][i]
					o = self.beta_stddevs[beta][i]
					print >> output, "%(beta)f,%(u)f,%(o)f"%locals()
		self.plot_command_top = "plot "
		for i in range(0,len(self.goals)) :
			fragment = "'%s' title '$P(G_%d|O)$' using 1:2 with lines, "%( goal_csvs_N[self.goals[i]], self.goals[i] )
			fragment += "'%s' title '' with errorbars"%goal_csvs_N[self.goals[i]]
			self.plot_command_top += fragment
			if i < len(self.goals)-1 :
				self.plot_command_top += ", "
		self.plot_command_bottom = "plot "
		for i in range(0,len(self.goals)) :
			fragment = "'%s' title '$P(G_%d|O)$' using 1:2 with lines, "%( goal_csvs_beta[self.goals[i]], self.goals[i] )
			fragment += "'%s'title '' with errorbars"%goal_csvs_beta[self.goals[i]]
			self.plot_command_bottom += fragment
			if i < len(self.goals)-1 :
				self.plot_command_bottom += ", "
			
			
	def get_probs_and_stats( self ) :
		for t in self.group :
			goals = t.computations.keys()
			goals.sort()
			self.N_counts[t.runs] += 1
			self.beta_counts[t.beta] += 1
			for i in range(0,len(goals)) :
				self.N_probs[t.runs][goals[i]] += t.goal_probs[goals[i]]
				self.beta_probs[t.beta][goals[i]] += t.goal_probs[goals[i]]
		for k in self.N_probs.keys() :
			u = self.N_probs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.N_counts[k] )
		for k in self.beta_probs.keys() :
			u = self.beta_probs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.beta_counts[k] )
		for t in self.group :
			goals = t.computations.keys()
			goals.sort()
			for i in range(0,len(goals)) :
				v = t.goal_probs[goals[i]] - self.N_probs[t.runs][goals[i]]
				v *= v
				self.N_stddevs[t.runs][goals[i]] += v
				v = t.goal_probs[goals[i]] - self.beta_probs[t.beta][goals[i]]
				v *= v
				self.beta_stddevs[t.beta][goals[i]] += v
		for k in self.N_stddevs.keys() :
			u = self.N_stddevs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.N_counts[k] - 1.0 )
				u[i] = math.sqrt( u[i] )
		for k in self.beta_stddevs.keys() :
			u = self.beta_stddevs[k]
			for i in range(0,len(u)) :
				u[i] /= float( self.beta_counts[k] - 1.0 )
				u[i] = math.sqrt( u[i] )

class Obs_Acc_Plot( Plot ) :

	def __init__( self, obs_data ) :
		Plot.__init__( self, 'templates/simple-log-x.tmpl.ppl' )
		self.obs_data = obs_data
		for _, fname in self.obs_data.iteritems() :
			self.aux_files.append(fname)
	
	def set_values( self ) :
		self.xlabel = "$m$"
		self.ylabel = "ACC"
		self.ymin = 0.5
		self.ymax = 1
		self.xmin = 1
		self.xmax = 10000	
		functions = []
		for pct, fname in self.obs_data.iteritems() :
			cmd = "'%s' title '%d' with lines linetype 5"%(fname,pct)
			functions.append( cmd )
		self.plot_command = "plot %s"%", ".join(functions)

class ROC_Plot( Plot ) :

	def __init__( self, points, m_values, beta_values ) :
		Plot.__init__( self, 'templates/roc.tmpl.ppl' )
		self.points = points
		#for _, fname in self.points.iteritems() :
		#	self.aux_files.append(fname)
		self.point_styles = {}
		i = 4
		for m in m_values :
			for b in beta_values :
				self.point_styles[(m,b)] = i
			i+= 1
				
	
	def set_values( self ) :
		self.xlabel = "FPR"
		self.ylabel = "TPR"
		functions = []
		#pstyle = 4
		keys = self.points.keys()
		keys.sort()
		for k  in keys :
			m, b = k
			fname = self.points[k]
			pstyle = self.point_styles[k]
			cmd = "'%(fname)s'  title '\\textsc{GR}($ m = %(m)d, \\beta = %(b).1f$)' with points pt %(pstyle)d"%locals()
			#cmd = "'%(fname)s' title '\\textsc{GR}($ m = %(m)d, \\beta = %(b).1f$)' with points pt 4"%locals()
			functions.append( cmd )
			#pstyle += 1
		self.plot_command = ", ".join(functions)

class Curve_Plot( Plot ) :

	def __init__( self, curves, curve_names ) :
		Plot.__init__( self, 'templates/simple-log-x.tmpl.ppl' )
		self.curve_names = curve_names
		self.curves = curves
	
	def set_values( self ) :
		self.xlabel = "$m$"
		self.ylabel = "ACC"
		functions = []

		line_type = 1
		keys = self.curves.keys()
		keys.sort()
		for k  in keys :
			fname = self.curves[k]
			title = self.curve_names[k]
			cmd = "'%(fname)s'  title '%(title)s' with linespoints lw 1 lt %(line_type)d"%locals()
			functions.append( cmd )
			line_type += 1
		self.plot_command = ", ".join(functions)

