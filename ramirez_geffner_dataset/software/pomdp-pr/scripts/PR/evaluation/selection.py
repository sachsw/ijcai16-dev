from PR.evaluation.plots import PyxPlot
import os
import csv
import sys

class Group :
	
	def __init__(self, field, value ) :
		self.children = []
		self.items = []
		self.field = field
		self.value = value

	def __cmp__( self, other ) :
		return self.value.__cmp__(other.value)

	def add( self, item ) :
		self.items.append(item)

	def get_fields( self, names ) :
		fields = []
		for n in names :
			fields += [ getattr(self, n) ]
		return fields
	
	def get_rows( self, row_head, fields ) :
		if len(self.children) == 0 :
			print len(self.items)
			row = row_head + [self.value] + self.get_fields(fields)
			return [ row ]
		new_row_head = row_head + [self.value]
		rows = []
		for c in self.children :
			rows += c.get_rows( new_row_head, fields )
		return rows

	def compute_statistics( self ) :
		self.H = 0.0
		self.L = 0.0
		self.T = 0.0
		self.TPR = 0.0
		self.FPR = 0.0
		self.ACC = 0.0
		self.PPV = 0.0
		self.SST = 0.0
		TP = 0
		TN = 0
		FP = 0
		FN = 0
		P = 0
		N = 0
		Pp = 0
		Np = 0
		
		for item in self.items :
			self.H += len(item.goal_probs)
			self.L += item.obs_len
			self.T += item.time
			TP += item.TP
			TN += item.TN
			FP += item.FP
			FN += item.FN
			P += item.P
			N += item.N
			Pp += item.Pp
			Np += item.Np

		self.H /= float(len(self.items))
		self.L /= float(len(self.items))
		self.T /= float(len(self.items))
		self.TPR = float(TP)/float(TP + FN)
		self.FPR = float(FP)/float(FP + TN)
		self.ACC = float(TP+TN)/float(P+N)
		self.PPV = float(TP)/float(TP+FP)
		self.SST = float(TP)/float(P)
	
		for c in self.children :
			c.compute_statistics()	

class Separator :

	def __init__(self,  field ) :
		self.field = field

	def do( self, father ) :
		groups = {}
		for t in father.items :
			try :
				groups[getattr(t,self.field)].add(t)
			except KeyError :
				groups[getattr(t,self.field)] = Group(self.field,getattr(t,self.field))
				groups[getattr(t,self.field)].add(t)
		
		for value, g in groups.iteritems () :
			if self.field == 'pct_obs' : print value
			father.children.append( g )

class Filter :
	
	def __init__( self, field, value ) :
		self.field = field
		self.value = value
		self.result = []

	def do( self, tests ) :
		for t in tests :
			y = str(self.value)
			x = getattr(t, self.field)
			x = str(x)
			if y == x :
				self.result.append(t)
		print >> sys.stdout, "%d tests passed %s = %s"%(len(self.result), self.field, self.value)

class Crawler :

	def __init__(self, root_dir ) :
		self.root_dir = root_dir
		self.test_paths = []

		if not os.path.exists( self.root_dir ) :
			raise RuntimeError, "Directory %s does not exist"%self.root_dir
		if not os.path.isdir( self.root_dir ) :
			raise RuntimeError, "%s is not a directory"%self.root_dir

	def crawl( self ) :
		pattern = '.result'
		for dirpath, _, files in os.walk( self.root_dir ) :
			if 'svn' in dirpath : continue
			for fname in files :
				if pattern in fname :
					self.test_paths.append( os.path.join( dirpath, fname ) )
				

class Dataset :

	def __init__( self, tests ) :
		self.tests = tests
		self.filters = []
		self.separators = []
		self.groups = []
		self.selected_fields = []

	def load_spec( self, fname ) :
		with open(fname) as instream :
			line_idx = 1
			for line in instream :
				line = line.strip()
				key,values = line.split('=')
				values = values.split(',')
				if key == 'group_by' :
					if len(values) == 0 :
						raise RuntimeError, "No field specified for group_by command at line %d"%line_idx
					elif len(values) > 1 :
						raise RuntimeError, "Too many parameters specified for group_by command at line %d"%line_idx
					p = Separator(values[0])
					self.separators.append(p)
				elif key == 'filter' :
					if len(values) == 0 :
						raise RuntimeError, "No field and value specified for filter command at line %d"%line_idx
					elif len(values) == 1 :
						raise RuntimeError, "Missing parameters specified for filter command at line %d"%line_idx
					field = values[0]
					value = values[1]
					if field in ['beta'] :
						value = float(value)
					elif field in ['obs_len','true_goal','runs'] :
						value = int(value)
					p = Filter(field,value)
					self.filters.append(p)
				elif key == 'select' :
					self.selected_fields = values
				else :
					raise RuntimeError, "Unknown criterion %s specified at line %d"%(key,line_idx)
			line_idx += 1

	def make( self ) :
		self.apply_processes()
		self.root.compute_statistics()
	
	def apply_processes( self ) :
		for proc in self.filters :
			proc.do( self.tests )
			self.tests = proc.result
		if len(self.tests) == 0 :
			raise RuntimeError, "All data was filtered out"	

		# Root group
		self.root = Group(None,None)
		for t in self.tests :
			self.root.add(t)

		if len(self.separators) == 0 : return

	
		sep = self.separators[0]
		sep.do( self.root )
		for grp in self.root.children :
			self.apply_separators( grp, self.separators[1:] )

	def apply_separators( self, grp, seps ) :
		if len(seps) == 0 : return
		sep = seps[0]
		sep.do( grp )
		for child in grp.children :
			self.apply_separators( child, seps[1:] )
					
	def get_header( self ) :
		
		header = [ sep.field for sep in self.separators ]
		header += self.selected_fields
		return header

	def retrieve_rows( self ) :
		rows = []
		if len(self.root.children) == 0 :
			rows.append( self.root.get_fields( self.selected_fields ) )
			print rows
			return rows
		self.root.children.sort()
		print len(self.root.children)	
		for g in self.root.children :
			rows += g.get_rows([], self.selected_fields)
		return rows 

	def save_as_csv( self, fname, write_header = True ) :

		header = self.get_header()
		rows = self.retrieve_rows()

		spreadsheet = csv.writer( open( fname, 'w' ) )
		if write_header :
			spreadsheet.writerow( header )
		for r in rows :
			print r
			spreadsheet.writerow( r )

	def save_as_latex( self ) :
		pass


class Review :

	def __init__( self, exp_file_list, prefix, tolerance ) :
		self.exp_files = exp_file_list
		self.prefix = prefix
		self.tests = []
		self.tolerance = tolerance
		self.process()


	def process( self ) :
		for f in self.exp_files :
			t = Test(self.tolerance)
			t.load(f)
			t.compute_statistics()
			self.tests.append(t)
		self.summarize_by_domain_and_obs(  )
		#self.summarize_by_domain_goal_obs_samples()

	def summarize_by_domain_goal_obs_samples( self ) :
		
		self.groups = {}
		for t in self.tests :
			try :
				dom_grps = self.groups[t.domain]
			except KeyError :
				self.groups[t.domain] = {}
				dom_grps = self.groups[t.domain]
			try :
				goal_grps = dom_grps[t.true_goal]
			except KeyError :
				dom_grps[t.true_goal] = {}
				goal_grps = dom_grps[t.true_goal]
			try :
				obs_grps = goal_grps[t.obs_pct]
			except KeyError :
				goal_grps[t.obs_pct] = {}
				obs_grps = goal_grps[t.obs_pct]
			try :
				obs_grps[t.runs].add(t)
			except KeyError :
				obs_grps[t.runs] = Group()
				obs_grps[t.runs].add(t)
		for domain, dgrp in self.groups.iteritems() :
			for goal, ggrp in dgrp.iteritems() :
				for obs, ogrp in ggrp.iteritems() :
					for runs, grp in ogrp.iteritems() :
						grp.compute_statistics()
		

	def summarize_by_domain_and_obs( self ) :
		self.groups = {}
		for t in self.tests :
			try :
				dom_grps = self.groups[t.domain]
			except KeyError :
				self.groups[t.domain] = {}
				dom_grps = self.groups[t.domain]
			try :
				obs_grps = dom_grps[t.obs_pct]
			except KeyError :
				dom_grps[t.obs_pct] = {}
				obs_grps = dom_grps[t.obs_pct]
			try :
				obs_grps[t.runs].add(t)
			except KeyError :
				obs_grps[t.runs] = Group()
				obs_grps[t.runs].add(t)
		for domain, dgrp in self.groups.iteritems() :
			for obs, ogrp in dgrp.iteritems() :
				for runs, grp in ogrp.iteritems() :
					grp.compute_statistics()

	def write_confusion_matrix( self ) :
		for a, b in self.domains.iteritems () :
			with open( '%s_confusion_matrix.csv'%a, 'w' ) as outstream :
				fields = [ 'Domain', '%Obs', 'True Goal' ]
				print >> outstream, ",".join(fields)
				for c, d in b.iteritems() :
					for g, error_set in d.conf_matrix.iteritems() :
						count = {}
						for i in range(0, len(d.conf_matrix.keys())) :
							count[i] = 0
						for gi in error_set :
							count[gi] += 1
						values = [ x for x in count.iteritems() ]
						values.sort()
						fields = [a, c, g ] + [ gi_count for gi, gi_count in values ]
						fields = [ str(x) for x in fields ]
						print >> outstream, ",".join(fields) 
		

	def write_spreadsheets( self ) :
		with open( '%s-summary.csv'%self.prefix, 'w' ) as outstream :
			header = [ 'Domain', '%Obs', 'N', 'L', 'T', 'TPR', 'FPR', 'ACC', 'PPV', 'SST' ]
			print >> outstream, ",".join(header)
			for domain, dgrp in self.groups.iteritems() :
				for obs, ogrp in dgrp.iteritems() :
					for runs, grp in ogrp.iteritems() :
						fields = [ domain, obs, runs ]
						fields += grp.get_fields(header[3:])
						fields = [ str(x) for x in fields ]
						print >> outstream, ",".join(fields)
		with open( '%s-roc.csv'%self.prefix, 'w' ) as outstream :
			header = ['FPR', 'TPR']
			groups = {}
			for domain, dgrp in self.groups.iteritems() :
				for obs, ogrp in dgrp.iteritems() :
					for runs, grp in ogrp.iteritems() :
						fields = grp.get_fields( header ) + [ domain ]
						fields = [ str(x) for x in fields ]
						try :
							groups[obs].append( fields )
						except KeyError :
							groups[obs] = [fields]
			for obs, lines in groups.iteritems() :
				for fields in lines :
					print >> outstream, ",".join(fields)
				print >> outstream
				print >> outstream

		# self.write_confusion_matrix()

	def write_ROC_curve_script( self ) :
		plot = PyxPlot()
		plot.xlabel = 'FPR'
		plot.ylabel = 'TPR'
		plot.title = 'ROC curve (%s)'%self.prefix
		plot.data_file = '%s-roc.csv'%self.prefix
		plot.output = '%s-roc.eps'%self.prefix
		plot.write( 'roc-curve.script.tmpl', 'roc.ppl' )
		os.system( 'pyxplot roc.ppl' )
		os.system( 'ps2pdf -dEPSCrop -dEPSFitPage %s %s'%( plot.output, plot.output.replace('eps','pdf') ) )


