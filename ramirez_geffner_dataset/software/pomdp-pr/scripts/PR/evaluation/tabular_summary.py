import math
import csv

class Table_Summary :

	def __init__(self) :
		self.header = []
		self.rows = []

	def save_as_csv( self, fname ) :
		spreadsheet = csv.writer( open( fname, 'w' ), delimiter = ',' )
		spreadsheet.writerow( self.header )
		for row in self.rows : spreadsheet.writerow( row )

class Table_Composer :

	def __init__( self, group ) :
		self.table = Table_Summary()
		self.group = group

	def compose( self ) :
		pass

class Instance_Summary( Table_Composer ) :
	
	def __init__( self, group ) :
		Table_Composer.__init__(self,group)

	def compose( self ) :
		self.table.header = [ 'fname', 'domain', 'runs', 'beta', 'pct_obs', 'obs_len', 'time', 'true_goal', 'P(G_t)', 'TP' ]
		for obj in self.group :
			row = obj.get_fields( self.table.header )
			self.table.rows.append( row )
