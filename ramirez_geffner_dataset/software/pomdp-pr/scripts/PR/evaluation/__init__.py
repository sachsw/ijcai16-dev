import sys
import os
import math
import re
from PR.evaluation.selection import Group

class Computation_Result :

	def __init__( self ) :
		self.time = 0.0 
		self.prob_O = 0.0
		self.V_b0 = 0.0
		self.num_sat_O = 0
		self.num_unsat_O = 0
		self.O_cost = 0.0
		self.not_O_cost = 0.0

	

class Test :
	
	def __init__( self, tolerance ) :
		self.domain = None
		self.pct_obs = None
		self.obs_len = None
		self.true_goal = None
		self.time = None
		self.runs = None
		self.beta = None
		self.S_set = []
		self.goal_probs = []
		self.computations = dict()
		self.tolerance = tolerance
		# True Positive
		self.TP = 0
		# True Negative
		self.TN = 0
		# False Positive
		self.FP = 0
		# False Negative
		self.FN = 0
		# Actual Positive
		self.P = 0
		# Actual Negative
		self.N = 0
		# Predicted Positive
		self.Pp = 0
		# Predicted Negative
		self.Np = 0

	def load( self, fname ) :
		self.fname = fname
		self.time = 0
		goal_prob_dist = []
		with open( fname ) as instream :
			for line in instream :
				line = line.strip()
				key, value = line.split('=')
				if 'goal_prob' in key :
					tok = value.split(',')
					goal_prob_dist.append( (int(tok[0]), float(tok[1]) ) )
				elif 'computation' in key :
					tok = value.split(',')
					goal_idx, V_b0, t, prob_O, n_sat_O, n_unsat_O, O_cost, not_O_cost = tok
					goal_idx = int(goal_idx)
					self.computations[goal_idx] = Computation_Result()
					self.computations[goal_idx].time = float(t)
					self.computations[goal_idx].prob_O = float(prob_O)
					self.computations[goal_idx].V_b0 = float(V_b0)
					self.computations[goal_idx].num_sat_O = int(n_sat_O)
					self.computations[goal_idx].num_unsat_O = int(n_unsat_O)
					try :
						self.computations[goal_idx].O_cost = float(O_cost)
					except ValueError :
						self.computations[goal_idx].O_cost = 'n/a'
					try :
						self.computations[goal_idx].not_O_cost = float(not_O_cost)
					except ValueError :
						self.computations[goal_idx].not_O_cost = 'n/a'
					
					self.time += float(t)
					continue
				elif key in ['obs_len','obs_pct', 'true_goal'] :
					setattr( self, key, int(value))
				elif 'time' in key :
					continue
				else :
					setattr( self, key, value )
		try :
			self.beta = float(self.beta)
		except ValueError :
			self.beta = 0.0
		self.runs = int(self.runs)
		goal_prob_dist.sort()
		for _, p in goal_prob_dist :
			self.goal_probs.append(p)

	def get_fields( self, flist ) :
		values = []
		for name in flist :
			if name == 'P(G_t)' :
				values.append( self.goal_probs[self.true_goal] )	
			else :
				values.append( getattr( self, name ) )
		return values

	def	compute_statistics( self ) :
		max_prob = max(self.goal_probs)
		threshold = max_prob * (1. - float(self.tolerance)/100.) 
		for i in range(0,len(self.goal_probs)) :
			#if math.fabs( max_prob - self.goal_probs[i] ) < 1e-7 :
			if self.goal_probs[i] >= threshold :
				self.S_set.append(i) 
		if self.true_goal in self.S_set :
			self.TP = 1
			self.FP = len(self.S_set) - 1
			self.TN = len(self.goal_probs) - len(self.S_set)
		else :
			self.FN = 1
			self.FP = len(self.S_set)
			self.TN = len(self.goal_probs) - len(self.S_set) - 1
		self.P = 1
		self.N = len(self.goal_probs) - 1
		self.Pp = len(self.S_set)
		self.Np = len(self.goal_probs) - len(self.S_set)
		print self.fname, self.TP, self.FN, self.FP, self.FN

