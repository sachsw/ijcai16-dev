import sys
import random
import math

def make_obs_from_file( obs_file ) :
	obs = []
	with open( obs_file ) as instream :
		for line in instream :
			line = line.strip()
			obs.append( line )
	o = ObservationSequence( obs )
	o.level = 0
	return o

def sample_actions( level, sim, non_obs ) :
	obs_actions = []
	print non_obs
	while len(obs_actions) == 0 :
		# 1. choose one trial at random
		st = random.choice(sim.trials)
		print len(sim.trials)	
		# 2. select actions
		observable = [ a for a in st.actions if a not in non_obs ]
		print st.actions
		print observable
		N = len(observable)
		indices = [ i for i in range(0,N) ]
		pop_size = int(math.ceil(len(observable)*(float(level)/100.)))
		if pop_size == 0 : pop_size = 2
		sampled_indices = random.sample( indices, pop_size )
		sampled_indices.sort()
		obs_actions = [ observable[i] for i in sampled_indices ]
	return obs_actions

def make_obs_from_sim( sim, level, non_obs ) :
	obs = sample_actions( level, sim, non_obs )
	o = ObservationSequence( obs )
	o.level = level
	return o

class ObservationSequence :

	def __init__(self, obs ) :
		self.obs_actions = obs
		self.vars = [ 'obs_%d_accounted_for'%i for i in range(0,len(self.obs_actions)) ]

	def declare_vars( self ) :
		return ['%s - :boolean'%( x ) for x in self.vars ]

	def match( self, act_name ) :
		obs = []
		for i in range(0,len(self.obs_actions)) :
			if act_name in self.obs_actions[i] :
				obs.append(i)
		return obs

	def declare_effect( self, index ) :
		if index == 0 :
			return '(:set %s true)'%self.vars[index]
		a = self.vars[index]
		b = self.vars[index-1]
		return '(:when (= %s true) (:set %s true) )'%(b,a)

	
	def declare_abandon_op( self, goal ) :
		op_decl = []
		op_decl += [ '(:action abandon_O' ]
		op_decl += [ ':effect' ]
		op_decl += goal.make_true_statements()
		print self.vars
		if self.negated :
			op_decl += [ '(:set %s false)'%self.vars[-1] ]
		else :
			op_decl += [ '(:set %s true)'%self.vars[-1] ]
		op_decl += [ ':cost 100' ]
		op_decl += [ ')' ]
		return op_decl	
		
	def state_initializations( self ) :
		return [ '(:set %s false)'%x for x in self.vars ]	
	
	def declare_goal( self ) :
		if self.negated :
			return '(= %s false)'%self.vars[-1]
		return '(= %s true)'%self.vars[-1]
