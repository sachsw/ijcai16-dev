#!/usr/bin/python
import os
import sys
from datetime import datetime

def main() :

	#folders = [ 'office-v2-greedy-policy', 'explorer-greedy-policy', 'drawers-2-greedy-policy', 'small-kitchen-greedy-policy' ]
	folders = [ 'office-v2-greedy-policy', 'drawers-2-greedy-policy', 'small-kitchen-greedy-policy' ]
	m_values = [ 10, 100, 1000, 10000, 100000 ]
	#m_values = [ 10000 ]
	#beta_values = [ 1.0, 2.0, 4.0, None ]
	#beta_values = [ 1.0, 10.0, 40.0 ]
	beta_values = [ 1.0, 10.0, 20.0, 40.0, None ]

	input_basename = 'tasks-180211'
	output_basename = 'results-180211'

	if os.path.exists( 'progress.log' ) :
		os.system( 'rm progress.log' )

	progress = open( 'progress.log', 'a' )

	for m in m_values :
		print >> sys.stdout, "m=%d"%m
		m_suffix = '-m-%d'%m
		for b in beta_values :
			if b is None :
				b_suffix = '-greedy-policy'
				print >> sys.stdout, "greedy policy"
			else :
				print >> sys.stdout, "beta=%f"%b
				b_suffix = '-b-%d'%(int(b))
			for domain in folders :
				input_dir = os.path.join( input_basename, domain )
				output_dir = os.path.join( output_basename, domain) + m_suffix + b_suffix	
				if b is None :
					beta = ''
				else :
					beta = '-b %f'%b
				cmd = './solve-pr-task.py -r %(m)d %(beta)s -i %(input_dir)s -o %(output_dir)s'%locals()
				print >> progress, "Executed", cmd
				print >> progress, "At", datetime.now()

				os.system( cmd )
				print >> progress, "Finished", datetime.now()

	progress.close()

if __name__ == '__main__' :
	main()
