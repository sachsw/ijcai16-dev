#!/usr/bin/python
from PR.evaluation           import Test
from PR.evaluation.selection import Crawler
from PR.evaluation.selection import Dataset
from PR.evaluation.selection import Filter 
from PR.evaluation.selection import Separator
from PR.evaluation.plots     import Plot
from PR.evaluation.plots     import ROC_Plot

import getopt
import sys
import os

class Options :

	def __init__( self, args ) :
		try :
			opts, args = getopt.getopt( args,
					"hi:o:",
					["input=",
					"output="] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)
		self.directory = None
		self.output = None

		for opcode, oparg in opts :
			if opcode in ('-h', '--help' ) :
				print >> sys.stderr, "Help invoked"
				self.usage()
				sys.exit(1)
			if opcode in ('-i', '--input') :
				self.directory = oparg
			if opcode in ('-o', '--output') :
				self.output = oparg
		if self.directory is None :
			print >> sys.stderr, "No directory was specified!"
			sys.exit(1)
		if self.output is None :
			print >> sys.stderr, "No output was specified!"
			sys.exit(1)

	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs: Plotting Accuracy for different observation levels"
		print >> sys.stderr, "-i --input       <directory>        Directory holding the data"
		print >> sys.stderr, "-o --output      <path>             Path of resulting diagram (PDF)"
		print >> sys.stderr, "-h --help                           Shows this help message"
		
def main() :
	
	opt = Options( sys.argv[1:] )	
	
	res_crawler = Crawler( opt.directory )
	res_crawler.crawl()
	tests = []
	for path in res_crawler.test_paths :
		t = Test(0.0)
		t.load( path )
		t.compute_statistics()
		tests.append(t)
	datasets = {}
	for m in [ 10, 100, 1000 ] :
		for b in [ 1.0, 10.0, 40.0 ] :
			k = (m,b)
			datasets[k] = Dataset( tests )
			datasets[k].filters.append( Filter( 'beta', b ) )
			datasets[k].filters.append( Filter( 'runs', m ) )
			datasets[k].selected_fields = [ 'FPR', 'TPR' ]

	csvs = {}
	for key, dataset in datasets.iteritems() :
		print key
		dataset.make()
		csvs[key] = 'tmp-roc-m-%d-b-%d.csv'%key
		dataset.save_as_csv( csvs[key], False )

	plot = ROC_Plot( csvs, [10, 100, 1000], [1.0, 10.0, 40.0] )
	plot.save_as_PDF( opt.output )	

if __name__ == '__main__' :
	main()
