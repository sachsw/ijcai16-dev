#!/bin/bash
./solve-pr-task.py -r 1000 -b 1.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b1-1000
./solve-pr-task.py -r 1000 -b 1.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b1-1000

./solve-pr-task.py -r 10000 -b 4.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b4-10000
./solve-pr-task.py -r 10000 -b 4.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b4-10000
./solve-pr-task.py -r 10000 -b 4.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b4-10000
./solve-pr-task.py -r 10000 -b 4.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b4-10000

./solve-pr-task.py -r 10000 -b 2.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b2-10000
./solve-pr-task.py -r 10000 -b 2.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b2-10000
./solve-pr-task.py -r 10000 -b 2.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b2-10000
./solve-pr-task.py -r 10000 -b 2.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b2-10000

./solve-pr-task.py -r 10000 -b 1.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b1-10000
./solve-pr-task.py -r 10000 -b 1.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b1-10000
./solve-pr-task.py -r 10000 -b 1.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b1-10000
./solve-pr-task.py -r 10000 -b 1.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b1-10000

./solve-pr-task.py -r 100000 -b 4.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b4-100000
./solve-pr-task.py -r 100000 -b 4.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b4-100000
./solve-pr-task.py -r 100000 -b 4.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b4-100000
./solve-pr-task.py -r 100000 -b 4.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b4-100000

./solve-pr-task.py -r 100000 -b 2.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b2-100000
./solve-pr-task.py -r 100000 -b 2.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b2-100000
./solve-pr-task.py -r 100000 -b 2.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b2-100000
./solve-pr-task.py -r 100000 -b 2.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b2-100000

./solve-pr-task.py -r 100000 -b 1.0 -i pr-paper-tasks/optimized/office-greedy-policy -o paper-results/office-greedy-policy-b1-100000
./solve-pr-task.py -r 100000 -b 1.0 -i pr-paper-tasks/optimized/explorer-greedy-policy -o paper-results/explorer-greedy-policy-b1-100000
./solve-pr-task.py -r 100000 -b 1.0 -i pr-paper-tasks/optimized/drawers-greedy-policy -o paper-results/drawers-greedy-policy-b1-100000
./solve-pr-task.py -r 100000 -b 1.0 -i pr-paper-tasks/optimized/kitchen-greedy-policy -o paper-results/kitchen-greedy-policy-b1-100000
