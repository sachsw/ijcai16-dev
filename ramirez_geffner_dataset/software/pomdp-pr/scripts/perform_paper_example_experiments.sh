#!/bin/bash
./solve-pr-task.py -r 10 -b 4.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10-samples-beta-4
./solve-pr-task.py -r 100 -b 4.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/100-samples-beta-4
./solve-pr-task.py -r 1000 -b 4.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/1000-samples-beta-4
./solve-pr-task.py -r 10000 -b 4.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10000-samples-beta-4

./solve-pr-task.py -r 10 -b 3.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10-samples-beta-3
./solve-pr-task.py -r 100 -b 3.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/100-samples-beta-3
./solve-pr-task.py -r 1000 -b 3.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/1000-samples-beta-3
./solve-pr-task.py -r 10000 -b 3.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10000-samples-beta-3

./solve-pr-task.py -r 10 -b 2.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10-samples-beta-2
./solve-pr-task.py -r 100 -b 2.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/100-samples-beta-2
./solve-pr-task.py -r 1000 -b 2.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/1000-samples-beta-2
./solve-pr-task.py -r 10000 -b 2.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10000-samples-beta-2

./solve-pr-task.py -r 10 -b 1.5 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10-samples-beta-1.5
./solve-pr-task.py -r 100 -b 1.5 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/100-samples-beta-1.5
./solve-pr-task.py -r 1000 -b 1.5 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/1000-samples-beta-1.5
./solve-pr-task.py -r 10000 -b 1.5 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10000-samples-beta-1.5

./solve-pr-task.py -r 10 -b 1.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10-samples-beta-1
./solve-pr-task.py -r 100 -b 1.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/100-samples-beta-1
./solve-pr-task.py -r 1000 -b 1.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/1000-samples-beta-1
./solve-pr-task.py -r 10000 -b 1.0 -i paper-example/pr-drawers-2.tar.bz2 -o paper-example/results-3/10000-samples-beta-1

