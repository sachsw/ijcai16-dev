#!/usr/bin/python
import sys
import options
import glob
from PR.domain 		import Domain, load_non_observable_actions
from PR.problem 	import Problem
from PR.goal 		import *
from PR.init 		import Initial
from PR.task 		import TaskDescription
from PR.obs  		import ObservationSequence, make_obs_from_file, make_obs_from_sim
from PR.gpt 		import GPT

def main() :
	opt = options.GenerateOptions(sys.argv[1:])
	dom = Domain( opt.domain_dir )
	
	# get problems in domain
	tasks = glob.glob( os.path.join( opt.domain_dir, 'task-*' ) )
	tasks.sort()	

	solver = GPT(opt.gpt_home)
	pr_tasks = []	
	num_goals = 0
	non_obs = load_non_observable_actions(opt.domain_dir)

	for j in range(0,len(tasks)) :
		if not os.path.isdir( tasks[j] ) : continue 
		problem_set = []
		goals = load_goals( tasks[j] )
		num_goals = len(goals)
		init = Initial( tasks[j] )
		for i in range(0,len(goals)) :	
			problem = Problem( opt.domain_dir, j, i )
			problem.init = init
			problem.goal = goals[i]
			problem.domain = dom
			problem.make_name()
			problem_set.append( problem )
		for i in range(0,len(problem_set)) :
			problem_set[i].compile_obs()
			problem_set[i].write_pddl()
		sims = []
		for i in range(0,len(goals)) :
			sim = solver.solve_and_simulate( problem_set[i], opt.use_soft_max, opt.beta, 'work-generate' )
			for t in sim.trials :
				if len(t.actions) == 0 :
					print >> sys.stdout, "Check domain, empty trials retrieved from sim!"
					t.describe()
					sys.exit(1)
			sims.append( sim )

		if opt.use_obs_from_file is None :	
			obs_sequences = [ [] for i in range(0,len(goals)) ]
			for level in [10,30,50,70,100] :
				for i in range(0,opt.num_sequences) :
					for k in range(0,len(goals)) :
						#o = ObservationSequence(sims[k], level, non_observable_actions )
						o = make_obs_from_sim( sims[k], level, non_obs )
						obs_sequences[k].append(o)
			for i in range(0,len(goals)) :
				for k in range(0,len(obs_sequences[i])) :	
					pr_task = TaskDescription( j, i, problem_set, obs_sequences[i][k], False )
					pr_task.beta = opt.beta
					pr_task.use_soft_max = opt.use_soft_max
					pr_tasks.append( pr_task )

		else :
			#obs_seq =  ObservationSequence( opt.use_obs_from_file )
			obs_seq = make_obs_from_file( opt.use_obs_from_file )
			pr_task = TaskDescription( j, 0, problem_set, obs_seq, False )
			pr_task.beta = opt.beta
			pr_task.use_soft_max = opt.use_soft_max
			pr_tasks.append( pr_task )


	
	for i in range(0,len(pr_tasks)) :
		pr_tasks[i].make_and_pack(i, opt.output_dir)

if __name__ == '__main__' :
	main()
