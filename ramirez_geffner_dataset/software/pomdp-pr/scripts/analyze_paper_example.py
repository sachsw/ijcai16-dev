#!/usr/bin/python
import sys, os
from PR.evaluation import Test
from PR.evaluation.tabular_summary import *
from PR.evaluation.plots import Obs_Prob_Curve, Goal_Prob_Curve

def main() :
	res_folder = 'paper-example/results-2'
	res_filename = 'pr-drawers-1.result'	
	folders = [ os.path.join(res_folder, f) for f in os.listdir(res_folder) if 'svn' not in f ]
	#for f in folders : print f
	experiments = [ os.path.join( f, res_filename ) for f in folders ]
	#for path in experiments : print path
	tests = []
	beta_values = []
	N_values = []
	for result_file in experiments :
		if not os.path.exists(result_file) :
			print >> sys.stderr, "Warning! result file", result_file, "not found!"
			continue
		t = Test( 0.0 )
		t.load( result_file )
		t.compute_statistics()
		beta_t = float(t.beta)
		N_t = int(t.runs)
		if beta_t not in beta_values : beta_values += [beta_t]
		if N_t not in N_values : N_values += [N_t]
		tests.append( t )
	beta_values.sort()
	N_values.sort()
	#print >> sys.stdout, "Beta values:"
	#for b in beta_values :
	#	print >> sys.stdout, "\\beta=%f"%b
	#print >> sys.stdout, "N values:"
	#for N in N_values :
	#	print >> sys.stdout, "N=%d"%N
	summary = Instance_Summary( tests )
	summary.compose()
	summary.table.save_as_csv( os.path.join( res_folder, 'summary.csv' ) )
	plot_obs_probs = Obs_Prob_Curve( tests )
	plot_obs_probs.save_as_PDF( os.path.join( res_folder, 'obs_probs.pdf' ) )
	plot_obs_probs = Goal_Prob_Curve( tests )
	plot_obs_probs.save_as_PDF( os.path.join( res_folder, 'goal_probs.pdf' ) )


if __name__ == '__main__' :
	main()
