#!/bin/bash
./generate-pr-tasks.py -d optimized/office-v2 -o pr-paper-tasks/optimized/office-greedy-policy -n 10 
./generate-pr-tasks.py -d optimized/explorer -o pr-paper-tasks/optimized/explorer-greedy-policy -n 10 
./generate-pr-tasks.py -d optimized/drawers -o pr-paper-tasks/optimized/drawers-greedy-policy -n 10 
./generate-pr-tasks.py -d optimized/small-kitchen -o pr-paper-tasks/optimized/kitchen-greedy-policy -n 10 
./generate-pr-tasks.py -d optimized/office-v2 -o pr-paper-tasks/optimized/office-beta-4 -n 10 --beta 4.0 --use-soft-max-policy
./generate-pr-tasks.py -d optimized/explorer -o pr-paper-tasks/optimized/explorer-beta-4 -n 10 --beta 4.0 --use-soft-max-policy
./generate-pr-tasks.py -d optimized/drawers -o pr-paper-tasks/optimized/drawers-beta-4 -n 10 --beta 4.0 --use-soft-max-policy
./generate-pr-tasks.py -d optimized/small-kitchen -o pr-paper-tasks/optimized/kitchen-beta-4 -n 10 --beta 4.0 --use-soft-max-policy
