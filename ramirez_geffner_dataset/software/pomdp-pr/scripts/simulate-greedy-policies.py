#!/usr/bin/python
import sys
import options
import glob
import csv
from PR.domain 		import Domain, load_non_observable_actions
from PR.problem 	import Problem
from PR.goal 		import *
from PR.init 		import Initial
from PR.task 		import TaskDescription
from PR.obs  		import ObservationSequence, make_obs_from_file, make_obs_from_sim
from PR.gpt 		import GPT

def main() :
	opt = options.GenerateOptions(sys.argv[1:])
	dom = Domain( opt.domain_dir )
	
	# get problems in domain
	tasks = glob.glob( os.path.join( opt.domain_dir, 'task-*' ) )
	tasks.sort()	

	solver = GPT(opt.gpt_home)
	pr_tasks = []	
	num_goals = 0
	non_obs = load_non_observable_actions(opt.domain_dir)

	for j in range(0,len(tasks)) :
		if not os.path.isdir( tasks[j] ) : continue 
		problem_set = []
		goals = load_goals( tasks[j] )
		num_goals = len(goals)
		init = Initial( tasks[j] )
		for i in range(0,len(goals)) :	
			problem = Problem( opt.domain_dir, j, i )
			problem.init = init
			problem.goal = goals[i]
			problem.domain = dom
			problem.make_name()
			problem_set.append( problem )
		for i in range(0,len(problem_set)) :
			problem_set[i].compile_obs()
			problem_set[i].write_pddl()
		sims = []
		for i in range(0,len(goals)) :
			sim = solver.solve_and_simulate( problem_set[i], opt.use_soft_max, opt.beta, 'work-generate' )
			for t in sim.trials :
				if len(t.actions) == 0 :
					print >> sys.stdout, "Check domain, empty trials retrieved from sim!"
					sys.exit(1)
			sims.append( sim )

		sim_stats = csv.writer( open( os.path.join(opt.output_dir,'%s-greedy-policy.csv'%dom.name), 'w' ) ) 
		sim_stats.writerow( [ 'Goal', 'N', 'Avg', 'Min', 'Max', 'Min Index', 'Max Index' ] )
		for k in range(0, len(goals)) :
			histogram_csv = csv.writer( open(os.path.join(opt.output_dir,'%s-G%d-histogram.csv'%(dom.name,k)), 'w' ) )
			histogram = {}
			goal = k
			N = len(sims[k].trials)
			avg = 0.0
			minlen = 100000
			maxlen = 0
			minidx = 0
			maxidx = 0
			for i in range(0, len(sims[k].trials)) :
				sim_file = os.path.join( opt.output_dir, problem_set[k].name + '-%d.sim'%i )
				steps = len(sims[k].trials[i].actions)
				try :
					histogram[steps].append( i )
				except KeyError :
					histogram[steps] = [ i ]
				avg += float(steps)
				if minlen > steps :
					minlen = steps
					minidx = i
				if maxlen < steps :
					maxlen = steps
					maxidx = i
				with open( sim_file, 'w' ) as outstream :
					for a in sims[k].trials[i].actions :
						print >> outstream, a
			values = histogram.keys()
			values.sort()
			for l in values :
				row = [ l, len(histogram[l]) ] + histogram[l]
				histogram_csv.writerow( row )		
	
			avg /= float(N)
			sim_stats.writerow( [ goal, N, avg, minlen, maxlen, minidx, maxidx ] )
				

if __name__ == '__main__' :
	main()
