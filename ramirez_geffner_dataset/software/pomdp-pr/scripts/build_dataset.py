#!/usr/bin/python
from PR.evaluation           import Test
from PR.evaluation.selection import Crawler
from PR.evaluation.selection import Dataset
from options                 import Dataset_Options

import sys

def main() :
	opt = Dataset_Options( sys.argv[1:] )

	results_crawler = Crawler( opt.directory )
	results_crawler.crawl()
	print >> sys.stdout, len(results_crawler.test_paths), "tests found" 

	print >> sys.stdout, "Loading tests..."
	tests = []
	for path in results_crawler.test_paths :
		t = Test(opt.tolerance)
		t.load( path )
		t.compute_statistics()
		tests.append(t)

	the_dataset = Dataset(  tests )
	the_dataset.load_spec( opt.spec_file)
	the_dataset.make()
	csv_name = opt.name + '.csv'
	the_dataset.save_as_csv( csv_name )

if __name__ == '__main__' :
	main()
