#!/usr/bin/python
import sys
from options 	import	SolveOptions
from PR.gpt	import	GPT
from PR.solve2	import	Task

def main() :
	opt = SolveOptions( sys.argv[1:] )	

	solver = GPT(opt.gpt_home)

	for pr_task in opt.input :
		print >> sys.stdout, "Processing", pr_task
		task = Task( pr_task, solver )
		if int(task.obs_pct) in [ 10,  100 ] :
			continue
		task.solve(opt.nruns, opt.beta)
		task.write_result(opt.output_directory)

if __name__ == '__main__' :
	main()
