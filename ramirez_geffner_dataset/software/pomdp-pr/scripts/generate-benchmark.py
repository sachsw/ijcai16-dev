#!/usr/bin/python
import os

def main() :

	folders = [ 'office-v2', 'explorer', 'drawers-2', 'small-kitchen' ]
	num_obs = 10
	
	for name in folders :
		cmd = "./generate-pr-tasks.py -d optimized/%(name)s -o tasks-180211/%(name)s-greedy-policy -n %(num_obs)d"%locals()
		os.system( cmd )

if __name__ == '__main__' :
	main()
