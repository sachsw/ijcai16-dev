#!/usr/bin/python
from PR.evaluation	     	import Test
from PR.evaluation.selection 	import Crawler
from PR.evaluation.selection 	import Dataset
from PR.evaluation.selection 	import Filter 
from PR.evaluation.selection 	import Separator

import getopt
import os
import sys
import csv

class Options :

	def __init__( self, args ) :
		try :
			opts, args = getopt.getopt( args,
					"hi:o:",
					["input=",
					"output="] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)
		self.directory = None
		self.output = None

		for opcode, oparg in opts :
			if opcode in ('-h', '--help' ) :
				print >> sys.stderr, "Help invoked"
				self.usage()
				sys.exit(1)
			if opcode in ('-i', '--input') :
				self.directory = oparg
			if opcode in ('-o', '--output') :
				self.output = oparg
		if self.directory is None :
			print >> sys.stderr, "No directory was specified!"
			sys.exit(1)
		if self.output is None :
			print >> sys.stderr, "No output was specified!"
			sys.exit(1)

	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs: Plotting Accuracy for different observation levels"
		print >> sys.stderr, "-i --input       <directory>        Directory holding the data"
		print >> sys.stderr, "-o --output      <path>             Path of resulting confusion matrices"
		print >> sys.stderr, "-h --help                           Shows this help message"


class Confusion_Matrix :

	def __init__( self, tests ) :
		self.tests = tests
		self.goals = set()
		self.counts = {}
		self.get_counts()

	def get_counts( self ) :
		for t in self.tests :
			gt = t.true_goal
			self.goals.add(gt)
			errors = False
			for gi in t.S_set :
				entry = ( gi, gt ) 
				if gi != gt :
					errors = True
				try :
					self.counts[entry] += 1
				except KeyError :
					self.counts[entry] = 1
			if errors :
				print >> sys.stdout, "Experiment", t.fname, "has classification errors"

	def write_as_csv( self, fname ) :
		spreadsheet = csv.writer( open( fname, 'w' ) )
		for gi in self.goals :
			row = [ gi ]
			for gj in self.goals :
				try :
					row.append( self.counts[(gi,gj)] )
				except KeyError :
					row.append( 0 )
			spreadsheet.writerow( row )

def main() :
	
	opt = Options( sys.argv[1:] )

	res_crawler = Crawler( opt.directory )
	res_crawler.crawl()
	tests = []
	for path in res_crawler.test_paths :
		t = Test(0.0)
		t.load( path )
		t.compute_statistics()
		tests.append(t)
	d = Dataset( tests )
	d.separators.append( Separator( 'domain' ) )
	d.separators.append( Separator( 'obs_pct' ) )
	d.make()

	for domain_grp in d.root.children : 
		for obs_grp in domain_grp.children :
			csv_name = 'confmtx-%s-%s.csv'%(domain_grp.value, obs_grp.value )
			c = Confusion_Matrix( obs_grp.items )
			c.write_as_csv( csv_name )	

if __name__ == '__main__' :
	main()
