import getopt, os, sys, glob
import tarfile
import zipfile

class Dataset_Options :
	
	def __init__(self, args ) :
		try :
			opts, args = getopt.getopt( args,
					"hi:s:t:n:",
					["help",
					"input=",
					"use-spec-file="
					"tolerance="
					"name="] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)
		self.directory = None
		self.spec_file = None
		self.tolerance = 0.0
		self.name = 'dataset'
		
		for opcode, oparg in opts :
			if opcode in ('-h', '--help' ) :
				print >> sys.stderr, "Help invoked"
				self.usage()
				sys.exit(1)
			if opcode in ('-i', '--input' ) :
				self.directory = oparg
			if opcode in ('-s', '--use-spec-file' ) :
				self.spec_file = oparg
			if opcode in ('-n', '--name' ) :
				self.name = oparg
			if opcode in ('-t', '--tolerance' ) :
				self.tolerance = oparg
		if self.directory is None :
			print >> sys.stderr, "No directory was specified!"
			sys.exit(1)
		if self.spec_file is None :
			print >> sys.stderr, "No specification were given for the dataset!"
			sys.exit(1)


	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs Dataset Builder Help"
		print >> sys.stderr, "-i --input         <directory>       Generate dataset with result files in <directory>"
		print >> sys.stderr, "-n --name          <name>            Dataset name (must be a valid file name)"
		print >> sys.stderr, "-t --tolerance     <percentage>      Tolerance threshold (in \%) to accept a goal into the MLG set"
		print >> sys.stderr, "-s --use-spec-file <spec file>       Generate dataset according to specs in <spec file>"
		print >> sys.stderr, "-h                                   Shows this help message"

class EvaluateOptions :

	def __init__(self, args) :
		try :
			opts, args = getopt.getopt( args,
					"hi:b:p:t:",
					["help",
					"input=",
					"prefix=",
					"tolerance="] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)

		self.experiments = []
		self.prefix = ''
		self.tolerance = 10
		for opcode, oparg in opts :
			if opcode in ( '-h', '--help' ) :
				print >> sys.stderr, "Help invoked!"
				self.usage()
				sys.exit(1)
			if opcode in ( '-i', '--input' ) :
				if os.path.isdir( oparg ) : # we have directory
					pattern = os.path.join( oparg, '*.result' )
					self.experiments = [ os.path.abspath(f) for f in glob.glob( pattern ) ]
				else :
					tmp_dir = '/tmp/results'
					if not os.path.exists( tmp_dir ) :
						print >> sys.stdout, "Creating temp dir"
						os.makedirs( tmp_dir )
					else :
						print >> sys.stdout, tmp_dir, "already exists, cleaning up"
						os.system( 'rm %s/*'%tmp_dir )
					if zipfile.is_zipfile( oparg ) :
						handle_compressed_file( zipfile.open(oparg), tmp_dir )
					elif tarfile.is_tarfile( oparg ) :
						handle_compressed_file( tarfile.open(oparg), tmp_dir )
					else :
						print >> sys.stderr, oparg, "is neither a ZIP nor a TAR file!"
						print >> sys.stderr, "Aborting"
						sys.exit(1)
			if opcode in ( '-p', '--prefix' ) :
				self.prefix = oparg
			if opcode in ( '-t', '--tolerance' ) :
				self.tolerance = int(oparg)
		if len(self.experiments) == 0 :
			print >> sys.stderr, "No experiments available to review!"
			print >> sys.stderr, "Aborting"
			sys.exit(1)
					
	def handle_compressed_file( self, f, path ) :
		f.extractall( path )
		f.close()
		pattern = os.path.join( path, '*.result' )
		self.experiments = [ os.path.abspath(f) for f in glob.glob( pattern ) ]
	
	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs Evaluator Help"
		print >> sys.stderr, "-i --input     <directory|tarball>  Evaluate results in directory or compressed file"
		print >> sys.stderr, "-p --prefix    <prefix>             Prefix to append to all files created"
		print >> sys.stderr, "-t --tolerance <percentage>         Tolerance threshold (in \%) to accept a goal into the MLG set"
		print >> sys.stderr, "-h                                Shows this help message"
		 

class SolveOptions :

	def __init__(self, args) :
		try :
			opts, args = getopt.getopt( args,
						"hi:G:o:r:b:",
						["help",
						"input=",
						"gpt-home=",
						"output-directory=",
						"runs=",
						"beta="])

		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)

		self.input = None
		self.gpt_home = None
		self.output_directory = './'
		self.nruns = 100
		self.beta = None

		for opcode, oparg in opts :
			if opcode in ( '-h', '--help' ) :
				print >> sys.stderr, "Help requested"
				self.usage()
				sys.exit(0)
			elif opcode in ( '-i', '--input' ) :
				if not os.path.exists( oparg ) :
					print >> sys.stderr, "Specified input file", oparg, "cannot be found"
					print >> sys.stderr, "Aborting"
					self.usage()
					sys.exit(1)
				if not os.path.isdir( oparg ) :
					if not tarfile.is_tarfile(oparg) :
						print >> sys.stderr, "Specified input file", oparg, "is not a valid tar file"
						print >> sys.stderr, "Aborting"
						self.usage()
						sys.exit(1)
					else :
						self.input = [oparg]
				else :
					pattern = os.path.join( oparg, '*.tar.bz2' )
					self.input = glob.glob( pattern )	
			elif opcode in ( '-G', '--gpt-home' ) :
				if not os.path.exists( oparg ) :
					print >> sys.stderr, "Could not find GPT home folder!"
					print >> sys.stderr, "Aborting"
					self.usage()
					sys.exit(1)
				self.gpt_home = oparg
			elif opcode in ( '-o', '--output-directory' ) :
				if not os.path.exists( oparg ) :
					print >> sys.stdout, "Creating directory", oparg, "to store results"
					os.makedirs( oparg ) 
				self.output_directory = os.path.abspath( oparg )
			elif opcode in ( '-r', '--runs' ) :
				self.nruns = oparg
			elif opcode in ( '-b', '--beta' ) :
				self.beta = oparg	

		if self.gpt_home is None :
			self.gpt_home = os.path.abspath(os.path.join(os.getcwd(),'../gpt'))
			if not os.path.exists( os.path.join( self.gpt_home, 'bin') ) :
				print >> sys.stderr, "Could not find default GPT folder! (%s)"%self.gpt_home
				self.usage()
				sys.exit(1)
	
		if self.input is None :
			print >> sys.stderr, "No PR task was specified"
			print >> sys.stderr, "Aborting"
			self.usage()
			sys.exit(1)

	def usage( self ) :
		print >> sys.stderr, "PR over POMDPs Solver Help"
		print >> sys.stderr, "-i --instance   <tarball>    Solve the PR task in 'tarball'"
		print >> sys.stderr, "-o --output-dir <directory>  Path to directory where PR tasks are to be stored (creates it if it doesn' exist)"
		print >> sys.stderr, "-G --gpt-home   <directory>  Path to GPT binaries"
		print >> sys.stderr, "-r --runs       <number>     Number of simulation runs done to estimate P(O|G) [defaults to 100]"
		print >> sys.stderr, "-b --beta       <real>       Value of the beta parameter for the Boltzmann policy [defaults to greedy policy]"
		print >> sys.stderr, "-h                           Shows this help message"
		

class GenerateOptions :
	
	def __init__(self, args) :
		try :
			opts, args = getopt.getopt( args,
					"d:hG:n:o:b:sO:",
					["domain=", 
					"help",
					"gpt-home=",
					"num-sequences=",
					"output-dir=",
					"beta=",
					"use-soft-max-policy",
					"use-obs-from-file=" ] )
		except getopt.GetoptError :
			print >> sys.stderr, "Missing or incorrect parameters specified!"
			self.usage()
			sys.exit(1)

		self.domain_dir = None
		self.gpt_home = None
		self.num_sequences = 10
		self.output_dir = None
		self.beta = 1.0
		self.use_soft_max = False
		self.use_obs_from_file = None

		for opcode, oparg in opts :
			if opcode in ( '-h', '--help' ) :
				print >> sys.stderr, "Help requested"
				self.usage()
				sys.exit(0)
			elif opcode in ( '-d', '--domain' ) :
				if not os.path.exists( oparg ) :
					print >> sys.stderr, "Domain", oparg, "directory not found!"
					print >> sys.stderr, "Aborting"
					self.usage()
					sys.exit(1)
				self.domain_dir = oparg	
			elif opcode in ( '-G', '--gpt-home' ) :
				if not os.path.exists( oparg ) :
					print >> sys.stderr, "Could not find GPT home folder!"
					print >> sys.stderr, "Aborting"
					self.usage()
					sys.exit(1)
				self.gpt_home = oparg
			elif opcode in ( '-n', '--num-sequences' ) :
				try :
					self.num_sequences = int(oparg)
				except ValueError :
					print >> sys.stderr, oparg, "is not an integer number"
					print >> sys.stderr, "Aborting"
					self.usage()
					sys.exit(1)
			elif opcode in ( '-o', '--output-dir' ) :
				self.output_dir = os.path.abspath(oparg)
				if not os.path.exists( self.output_dir ) :
					os.system( 'mkdir -p %s'%self.output_dir)
			if opcode in ( '-s', '--use-soft-max-policy' ) :
				self.use_soft_max = True
			if opcode in ( '-b', '--beta' ) :
				try :
					self.beta = float(oparg)	
				except ValueError :
					print >> sys.stderr, oparg, "is not a valid value for the beta parameter"
			if opcode in ( '-O', '--use-obs-from-file' ) :
				self.use_obs_from_file = oparg
				if not os.path.exists( self.use_obs_from_file ) :
					print >> sys.stderr, "Specified obs-file", self.use_obs, "doesn't exist"	

		if self.domain_dir is None :
			print >> sys.stderr, "No domain was specified!"
			self.usage()
			print >> sys.stderr, "Aborting"
			sys.exit(1)

		if self.gpt_home is None :
			self.gpt_home = os.path.abspath(os.path.join(os.getcwd(),'..'))
			if not os.path.exists( os.path.join( self.gpt_home, 'bin') ) :
				print >> sys.stderr, "Could not find default GPT folder!"
				self.usage()
				sys.exit(1)

		if self.output_dir is None :
			print >> sys.stderr, "No output directory was specified!"
			self.usage()
			print >> sys.stderr, "Aborting"
			sys.exit(1)
		print >> sys.stdout, "Setting GPT_HOME to", self.gpt_home

	def usage( self ) :
		print >> sys.stderr, "Parameters:"
		print >> sys.stderr, "-d  --domain         <directory>  PR task domain folder"
		print >> sys.stderr, "-n  --num-sequences  <number>     Number of observation sequences to generate for each level"
		print >> sys.stderr, "-o  --output-dir     <directory>  Path to directory where PR tasks are to be stored (creates it if it doesn' exist)"
		print >> sys.stderr, "-G  --gpt-home       <directory>  Path to GPT binaries"
		print >> sys.stderr, "-h  --help                        Show parameters description"
