//  Theseus
//  theResult.cc -- Result Implementation
//
//  Blai Bonet, Hector Geffner
//  Universidad Simon Bolivar, 1998, 1999, 2000, 2001, 2002

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

#include <theseus/theResult.h>
#include <theseus/theUtils.h>
#include <theseus/theModel.h>
#include <theseus/theBelief.h>

///////////////////////////////////////////////////////////////////////////////
//
// Result Class
//

resultClass::~resultClass()
{
}

void
resultClass::startTimer()
{
  gettimeofday(&startTime,NULL);
}

void
resultClass::stopTimer()
{
  gettimeofday(&stopTime,NULL);
}

unsigned long
resultClass::getElapsedSeconds()
{
  if( secs == (unsigned long)-1 ) diffTime(secs,usecs,startTime,stopTime);
  return(secs);
}

unsigned long
resultClass::getElapsedUSeconds()
{
  if( usecs == (unsigned long)-1 ) diffTime(secs,usecs,startTime,stopTime);
  return(usecs);
}


///////////////////////////////////////////////////////////////////////////////
//
// Standard Result Class
//

void
standardResultClass::print( ostream& os, int outputLevel, problemHandleClass *pHandle )
{
	stepClass firstStep = stepList.front();
	stepClass lastStep = stepList.back();
	list<stepClass>::const_iterator it;
	
	os << "<e=" << (float)getElapsedSeconds() + ((float)getElapsedUSeconds()) / 1000000.0 << ">:";
	os << "<r=" << run << ">:";
	os << "<t=" << (runType == 1 ? 'l' : 'c') << ">:";
	os << "<v=" << initialValue << ">:";
	os << "<l=" << (solved ? 's' : 'u') << ">:";
	os << "<i=" << firstStep.state << ">:";
	os << "<f=" << lastStep.state << ">:";
	
	switch( outputLevel ) 
	{
	case 0:
		os << "<s=" << numMoves << ">:<c=" << costToGoal << ">:<d=" << discountedCostToGoal << ">";
		break;
	case 1:
		os << "<s=" << numMoves << ">:<c=" << costToGoal << ">:<d=" << discountedCostToGoal << ">:";
		if( numMoves != -1 )
		{
			os << "<p=";
			os.flush();
			for( it = stepList.begin(); it != stepList.end(); ) 
			{
				if( pHandle != NULL ) 
				{
					os << '<';
					if ( (*it).action == -1 ) 
						os << "GOAL_REACHED()";
					else if ( (*it).action == -2 )
						os << "DEAD_END_REACHED()";
					else if ( (*it).action == -3 )
						os << "SOLVED_STATE_REACHED()" << std::endl;
					else
						(*pHandle->printAction)(os,(*it).action);
					os << ",obs" << (*it).observation;
				}
				else 
				{
					os << "<act" << (*it).action << ",obs" << (*it).action;
				}
				++it;
				os << (it != stepList.end() ? ">," : ">");
			}
			os << ">";
		}
		else
			os << "<p=null>";
		// Terminal belief
		std::stringstream sstr;
		os << ":<T={";
		if ( finalBelief == NULL ) os << "null";
		else
		{
			finalBelief->getModel()->printState( lastStep.state, sstr, 0 );
			std::string str = sstr.str();
			std::string printable;
			for ( unsigned k = 0; k < str.size(); k++ ) 
			{
				if ( str[k] == ' ' ) continue;
				if ( str[k] == '\n' )
				{
					printable.push_back(',');
					continue;
				}
				printable.push_back(str[k]);
			}
			os << printable;	
		}
		os << "}>";
		break;
	}
	os << endl;
}


///////////////////////////////////////////////////////////////////////////////
//
// Search Result Class
//

void
searchResultClass::print( ostream& os, int outputLevel, problemHandleClass *pHandle )
{
  list<stepClass>::const_iterator it;

  os << "<e=" << (float)getElapsedSeconds() + ((float)getElapsedUSeconds()) / 1000000.0 << ">:";
  os << "<r=" << run << ">:";
  os << "<t=" << (runType == 1 ? 'l' : 'c') << ">:";
  os << "<v=" << initialValue << ">:";

  switch( outputLevel ) {
    case 0:
      os << "<s=" << numMoves << ">:<c=" << costToGoal << ">:<d=" << discountedCostToGoal << ">";
      break;
    case 1:
      os << "<s=" << numMoves << ">:<c=" << costToGoal << ">:<d=" << discountedCostToGoal << ">:";
      if( numMoves != -1 ) {
        os << "<p=<";
        assert( pHandle != NULL );
        for( it = stepList.begin(); it != stepList.end(); ) {
          (*pHandle->printAction)(os,(*it).action);
          ++it;
          os << (it != stepList.end() ? "," : "");
        }
        os << ">>";
      }
      else
        os << "<p=null>";
      break;
  }
  os << endl;

  // print sequence of actions plan-like
  os << "%plan-begin" << endl;
  for( it = stepList.begin(); it != stepList.end(); ++it ) {
    os << "%step ";
    (*pHandle->printAction)(os,(*it).action);
    os << endl;
  }
  os << "%plan-end" << endl;
}

