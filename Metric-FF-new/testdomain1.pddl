(define (domain test1)
;(:requirements :equality :disjunctive-preconditions) ; :strips
(:functions
   (version ?software)
)
(:predicates 
   (user ?User) 
   (email-msg ?User ?Msg) 
   (phishing-msg ?Msg ?Site)
   (mailer ?email-program)
   (use-software ?software)
   (msg-opened ?Msg) 
   (successful)
   (clicked-link ?msg ?Site)  ;; OK
   (user-visits-site ?User ?Site)
   (exploit-vulnerability ?Vulnerability)
   (information-leakage ?Account)
   (account ?Acct)
   (has-link ?Email ?Site) ;; OK
   (software ?program)
   (browser ?name)
   (site ?site)
   (user-types ?key)
   (running ?software)
   (file ?file)
   (mail-attachment ?msg ?file)
   (opened ?file)
   (has-trojan ?File)
   (key-logger-trojan ?File ?Keylogger)
   (key-logger ?kl)
   (installed ?file)
   (has-crafted-dialog-box ?Site)
   (F1-dialog-box-opens ?Site)
   (vb-script-version ?num)
   (information-available ?User ?Account)
   (logged-in ?User ?Account)
   (records ?KeyLogger ?Account)
 (browser-ssl-compromised ?Browser)
 (certificate-authorized ?Certificate)
)

(:action user-starts-email  :parameters (?User ?Mailer)
 :precondition (and (user ?User) (mailer ?Mailer))
 :effect (and (use-software ?Mailer) (running ?Mailer))
)

(:action user-reads-email  :parameters (?User ?Mailer ?Msg)
 :precondition (and (user ?User) (mailer ?Mailer) (use-software ?Mailer) (email-msg ?User ?Msg))
 :effect (and (msg-opened ?Msg))
)

(:action user-visits-site :parameters (?User ?Browser  ?Site)
 :precondition (and (user ?User) (software ?Browser) (browser ?Browser) (site ?Site))
 :effect (and (use-software ?Browser) (user-visits-site ?User ?Site)))

(:action user-clicks-link-in-email
 :parameters (?User ?Email ?Site ?Browser) 
 :precondition (and (user ?User) (browser ?Browser)
                    (msg-opened ?Email) (has-link ?Email ?Site)) 
 :effect (and (clicked-link ?Email ?Site) (use-software ?Browser)))

(:action user-login :parameters (?User ?Account)  ;; changed
 :precondition (and  (user ?User) (account ?Account))
 :effect (and (logged-in ?User ?Account) (information-available ?User ?Account))
)

;; test
(:action user-presses-F1-at-vbscript-site   ;; changed
 :parameters (?User ?Browser ?Site)
 :precondition (and (user ?User) (use-software ?Browser) (browser ?Browser)
   (= ?Browser browser-IE) (= ?Site vbscript-link)
   (user-visits-site ?User vbscript-link))
 :effect (user-types F1))

(:action user-opens-attachment :parameters (?User ?Msg ?File ?Mailer)
 :precondition (and (user ?User) (use-software ?Mailer) (mailer ?Mailer)
 	       	      (msg-opened ?Msg)
                      (mail-attachment ?Msg ?File) (file ?File))
 :effect (opened ?File))

(:action key-logger-installed :parameters (?User ?File ?KeyLogger)
 :precondition (and (user ?User) (opened ?File) (file ?File)
    (has-trojan ?File) (key-logger-trojan ?File ?KeyLogger))
 :effect (and (key-logger ?KeyLogger) (installed ?KeyLogger)))

(:action key-logger-activated :parameters (?KeyLogger ?Program)
 :precondition (and (key-logger ?KeyLogger) (installed ?KeyLogger) 
      	            (user-types F1) (use-software ?Program) 
                    (= ?Program browser-IE))
 :effect (running ?KeyLogger))

(:action attacker-intercepts 
 :parameters (?KeyLogger ?Account)
 :precondition (and (records ?KeyLogger ?Account)
	            (exploit-vulnerability vulnerability-key-logger))
 :effect (information-leakage ?Account))


(:action user-login-with-keylogger-activated
 :parameters (?User ?Account ?Keylogger)
 :precondition (and  (user ?User) (account ?Account) (key-logger ?Keylogger)
               (running ?Keylogger))
 :effect (and (logged-in ?User ?Account) (information-available ?User ?Account)
              (records ?Keylogger ?Account)))

;; new
(:action attacker-sends-email-with-keylogger
  :parameters (?User ?File ?Keylogger)
  :precondition (and (user ?User)  (file ?File) 
               (has-trojan ?File) (key-logger-trojan ?File ?Keylogger))
  :effect (and (email-msg ?User bad-email) (mail-attachment bad-email ?File))
)
(:action attacker-sends-email-with-crafted-link
  :parameters (?User ?Site)
  :precondition (and (user ?User) (site ?Site) (has-crafted-dialog-box ?Site))
  :effect (and (email-msg ?User crafted-email) (has-link crafted-email ?Site))  
)

(:action user-visits-crafted-site  ;-keylogger-installed
  :parameters (?User ?Browser ?Site ?Email)
  :precondition (and (user ?User) (use-software ?Browser)
                     (browser ?Browser) (= ?Browser browser-IE) (site ?Site) (has-crafted-dialog-box ?Site)
                     (clicked-link ?Email ?Site)
                     (or (vb-script-version VB-5-1) (vb-script-version VB-5-6)
                         (vb-script-version VB-5-7) (vb-script-version VB-5-8)))
  :effect (and  (user-visits-site ?User ?Site) (F1-dialog-box-opens ?Site)))

;; SSL
(:action attacker-obtains-certificate 
  :parameters (?Certificate)
  :precondition (and (= ?Certificate malicious-certificate))
  :effect (certificate-authorized ?Certificate))

(:action browser-contains-ssl-vulnerability
 :parameters (?Browser)
 :precondition (or (and (software ?Browser) (browser ?Browser)
                        (= ?Browser browser-firefox) (< (version ?Browser) 3))
                   (and (software ?Browser) (browser ?Browser) 
                        (= ?Browser browser-seamonkey)(< (version ?Browser) 1))
                   (and (software ?Browser) (browser ?Browser) 
                        (= ?Browser browser-mozilla) (= (version ?Browser) 3)))
 :effect (browser-ssl-compromised ?Browser))

; (:action browser-contains-ssl-vulnerability
;    :parameters (?Browser ?Software)
;    :precondition (and (software ?Browser) (browser ?Browser)
;                       (= ?Browser firefox) (< (version ?Browser) 3))
;    :effect (browser-ssl-compromised ?Browser))  
)