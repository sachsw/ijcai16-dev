(define (problem test-prob1)
    (:domain test1)
    (:objects
        user1
        bad-email
        gmail
        phishing-site
        account-bank
        vulnerability-phishing
        compromised-site
        file-with-trojan
        key-logger1
        crafted-email
        vbscript-link
        VB-5-1
        browser-IE
	vulnerability-key-logger
	browser-firefox
	malicious-certificate
	VB-5-6
	VB-5-1
	VB-5-7
	VB-5-8
	F1
	browser-seamonkey
	browser-mozilla
    )
    (:init 
        (user user1)
        (mailer gmail)
;        (use-software gmail)
        (exploit-vulnerability vulnerability-key-logger)    ;;;THIS DETERMINES THE ATTACK TYPE
        (file file-with-trojan)
        (has-trojan file-with-trojan)
;        (mail-attachment file-without-trojan)
;        (file file-without-trojan)
        (key-logger key-logger1)
        (key-logger-trojan file-with-trojan key-logger1)
        (site vbscript-link) 
        (has-crafted-dialog-box vbscript-link) 
        (vb-script-version VB-5-1)
        (software browser-IE)
        (browser browser-IE)
        (software browser-firefox)
	(= (version browser-firefox) 2)
        (browser browser-firefox)
        (information-available user1 account-bank)
        (account account-bank)
     )
;    (:goal (email-msg user1 crafted-email))
;    (:goal (user-types F1))
;    (:goal (and (opened file-with-trojan)))
;    (:goal (and (installed key-logger1) (has-link crafted-email vbscript-link)))
;    (:goal (and (clicked-link crafted-email vbscript-link) (use-software browser-IE) (F1-dialog-box-opens vbscript-link)))
;    (:goal (running key-logger1))
;    (:goal (and (logged-in user1 account-bank) (records key-logger1 account-bank)))
;    (:goal (and (information-leakage account-bank)))
;    (:goal (use-software browser-IE))
 ;;subgoals along the way to the ssl vulnerability 
;      (:goal (and (user-visits-site user1 bank-site)))
      (:goal (and (browser-ssl-compromised browser-firefox)))
;       (:goal (and (certificate-authorized malicious-certificate)))
;      (:goal (and (certificate-accepted bank-site malicious-certificate)))
;     (:goal (and (ssl-server-spoofed bank-site)))
;    (:goal (and (information-leakage account-bank)))
)

